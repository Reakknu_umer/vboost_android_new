package com.vboost.pro;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;

import com.bigkoo.alertview.AlertView;
import com.bigkoo.alertview.OnDismissListener;
import com.bigkoo.alertview.OnItemClickListener;
import com.google.gson.Gson;
import com.vboost.pro.Adapter.ManageContactsAdapter;
import com.vboost.pro.DBClasses.Campaign;
import com.vboost.pro.DBClasses.Company;
import com.vboost.pro.DBClasses.Contact;
import com.vboost.pro.Util.Constant;
import com.vboost.pro.Util.SharedPreferencesHelper;
import com.vboost.pro.model.Campaigns;
import com.vboost.pro.model.ManageContacts;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


/**
 * Created by Nabeel Hafeez on 7/27/2016.
 */
public class ManageContatcsAvtivity extends AppCompatActivity implements OnItemClickListener, OnDismissListener {

    ListView listViewContacts;
    Button addNewContact;
    Button contactNextButton;
    Context context;
    String[] contactsName;
    private ProgressDialog progress;
    ArrayList<ManageContacts> manageContactsArray = new ArrayList<ManageContacts>();
    ManageContactsAdapter adapter;

    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String Token = "tokenKey";
    public static final String Id = "idKey";
    public static final String Key = "companyKey";
    public static final String Name = "nameKey";
    public static final String Logo = "logoKey";
    public static final String Status = "statusKey";
    public static final String Terms = "termsKey";
    SharedPreferences sharedpreferences;

    String userCredentials;

    List<Contact> contactList;
    List<Campaign> campaigns;

    ArrayList<Campaigns> campaignsArray = new ArrayList<Campaigns>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_manage_contacts);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        context = this;
        addNewContact = (Button) findViewById(R.id.add_new_contact_button);
        contactNextButton = (Button) findViewById(R.id.manage_contacts_next_button);
        addNewContact.setAllCaps(false);
        contactNextButton.setAllCaps(false);
        addNewContact.getBackground().setColorFilter(new LightingColorFilter(Color.parseColor("#8ec63e"), 0x00000000));
        contactNextButton.getBackground().setColorFilter(new LightingColorFilter(Color.parseColor("#FBA919"), 0x00000000));
        listViewContacts = (ListView) findViewById(R.id.manage_contacts_list_view);

        userCredentials = sharedpreferences.getString(Token, null);
        peformOperation();

    }

    private void peformOperation() {
        manageContactsArray = new ArrayList<ManageContacts>();
        if(isOnline(ManageContatcsAvtivity.this)) {
            Contact.destroy();
            Campaign.destroy();
            new GetCampaigns(this).execute();
            new GetCompany(this).execute();

        }else{
            contactList  = Contact.getAll();
            for(int i = 0; i<contactList.size(); i++) {
                Contact contact = contactList.get(i);
                ManageContacts manageContacts = new ManageContacts(Integer.parseInt(contact.getCompany()), contact.getContact_id(), contact.getEmail(), contact.getIs_active(), contact.getName(), contact.getPhone(), contact.getPhoto(), contact.getTitle(), contact.getType());
                manageContactsArray.add(manageContacts);
            }
            extractNameArray(manageContactsArray);
            adapter = new ManageContactsAdapter(ManageContatcsAvtivity.this, manageContactsArray, userCredentials, contactsName, false);
            listViewContacts.setAdapter(adapter);

            campaigns  = Campaign.getAll();
            for(int i = 0; i<campaigns.size(); i++) {
                Campaign campaign = campaigns.get(i);
                Campaigns cam = new Campaigns(campaign.getCampaign_Color(), campaign.getCampaign_Details(), campaign.getCampaignID(),campaign.getCompany(), campaign.getDefault_count(), campaign.getKey(), campaign.getLogo(), campaign.getMax_count(), campaign.getMin_count(), campaign.getName());
                campaignsArray.add(cam);
            }

            Collections.sort(campaignsArray, new Comparator<Campaigns>() {
                public int compare(Campaigns c1, Campaigns c2) {
                    return c1.getName().compareTo(c1.getName());
                }
            });
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK && requestCode == 10)
            peformOperation();
    }

    public void customerTypeButton(View View) {

        if(campaignsArray.size() == 1){

           Intent campaignsDataIntent = new Intent(context,CampaignsDataActivity.class);
            campaignsDataIntent.putExtra("contactsName",contactsName);
            campaignsDataIntent.putExtra("campaignrow",campaignsArray.get(0));
            campaignsDataIntent.putExtra("contacts",manageContactsArray);
            campaignsDataIntent.putExtra("campaignArray", campaignsArray);
            startActivity(campaignsDataIntent);

        }else {
            Intent contactIntent = new Intent(ManageContatcsAvtivity.this, CampaignsActivity.class);
            contactIntent.putExtra("contactsName", contactsName);
            contactIntent.putExtra("contacts", manageContactsArray);
            contactIntent.putExtra("campaignArray", campaignsArray);
            startActivity(contactIntent);
            finish();
        }
    }

    public void addNewContatct(View view){

        Intent addContatctIntent = new Intent(ManageContatcsAvtivity.this, AddNewContact.class);
        addContatctIntent.putExtra("contactsName", contactsName);
        addContatctIntent.putExtra("contacts", manageContactsArray);
        startActivityForResult(addContatctIntent, 10);

    }

    public void logoutActivity(View view){

        Intent logoutIntent = new Intent(ManageContatcsAvtivity.this, LogOutActivity.class);
        startActivity(logoutIntent);
    }


    private class GetContacts extends AsyncTask<String, Void, Void> {

        private final Context context;

        public GetContacts(Context c) {
            this.context = c;
        }

        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(String... params) {
            try {

                URL url = new URL(Constant.SERVER_URL+"contacts/");

                HttpURLConnection connection = (HttpURLConnection) url.openConnection();

               // userCredentials = "96ea502e088bc46dd3e863f1c82db3e160a4a56a";
                byte[] encodedBytes = Base64.encode(userCredentials.getBytes(), 0);
                String tokenAuth = "Token " + userCredentials;/*new String(encodedBytes);*/
                connection.setRequestProperty("Authorization", tokenAuth);
                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                connection.setRequestProperty("Content-Language", "en-US");
                connection.setUseCaches(false);
                connection.setDoInput(true);

                connection.setRequestMethod("GET");
                connection.setRequestProperty("USER-AGENT", "Mozilla/5.0");


                int responseCode = connection.getResponseCode();

                System.out.println("\nSending 'POST' request to URL : " + url);
                System.out.println("Post parameters : " + tokenAuth);
                System.out.println("Response Code : " + responseCode);

                final StringBuilder output = new StringBuilder("Request URL " + url);
                //output.append(System.getProperty("line.separator") + "Request Parameters " + urlParameters);
                output.append(System.getProperty("line.separator") + "Response Code " + responseCode);
                output.append(System.getProperty("line.separator") + "Type " + "GET");
                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                String line = "";

                StringBuilder responseOutput = new StringBuilder();
                System.out.println("output===============" + br);
                while ((line = br.readLine()) != null) {
                    //responseOutput.append(line);

                    JSONArray jsonArray = new JSONArray(line);
                    System.out.println(line);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                        ManageContacts manageContacts = null;
                        try {
                            manageContacts = new ManageContacts(jsonObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        manageContactsArray.add(manageContacts);
                        responseOutput.append(jsonObject.getString("name")).append("  /  ");
                    }

                }
                br.close();

                output.append(System.getProperty("line.separator") + "Response " + System.getProperty("line.separator") + System.getProperty("line.separator") + responseOutput.toString());

                ManageContatcsAvtivity.this.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {

                        for(int i=0; i<manageContactsArray.size(); i++){

                                ManageContacts mc = manageContactsArray.get(i);
                                Contact contact = new Contact(String.valueOf(mc.getCompany()),mc.getId(),mc.getEmail(),mc.getIs_active(),mc.getName(),mc.getPhone(),mc.getPhoto(),null,mc.getTitle(),mc.getType(),null);
                                contact.saveToDb();
                        }
                        extractNameArray(manageContactsArray);
                        adapter = new ManageContactsAdapter(ManageContatcsAvtivity.this, manageContactsArray, userCredentials, contactsName, true);
                        listViewContacts.setAdapter(adapter);

                        progress.dismiss();

                    }
                });


            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute() {
            //progress.dismiss();
        }

    }

    private class GetCampaigns extends AsyncTask<String, Void, Void> {

        private final Context context;

        public GetCampaigns(Context c){
            this.context = c;
        }

        protected void onPreExecute(){
            progress = new ProgressDialog(this.context);
            progress.setMessage("Loading");
            progress.show();
        }

        @Override
        protected Void doInBackground(String... params) {
            try {

                URL url = new URL(Constant.SERVER_URL+"campaigns");

                HttpURLConnection connection = (HttpURLConnection)url.openConnection();

                //String userCredentials = "96ea502e088bc46dd3e863f1c82db3e160a4a56a";
                byte[] encodedBytes = Base64.encode(userCredentials.getBytes(), 0);
                String tokenAuth = "Token " + userCredentials;/*new String(encodedBytes);*/
                connection.setRequestProperty ("Authorization", tokenAuth);
                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                connection.setRequestProperty("Content-Language", "en-US");
                connection.setUseCaches(false);
                connection.setDoInput(true);

                connection.setRequestMethod("GET");
                connection.setRequestProperty("USER-AGENT", "Mozilla/5.0");


                int responseCode = connection.getResponseCode();

                System.out.println("\nSending 'POST' request to URL : " + url);
                System.out.println("Post parameters : " + tokenAuth);
                System.out.println("Response Code : " + responseCode);

                final StringBuilder output = new StringBuilder("Request URL " + url);
                //output.append(System.getProperty("line.separator") + "Request Parameters " + urlParameters);
                output.append(System.getProperty("line.separator")  + "Response Code " + responseCode);
                output.append(System.getProperty("line.separator")  + "Type " + "GET");
                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                String line = "";

                StringBuilder responseOutput = new StringBuilder();
                System.out.println("output===============" + br);
                while((line = br.readLine()) != null ) {
                    //responseOutput.append(line);

                    JSONArray jsonArray = new JSONArray(line);
                    System.out.println(line);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                        Campaigns campaigns = null;
                        try {
                            campaigns = new Campaigns(jsonObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        campaignsArray.add(campaigns);
                        if (campaignsArray.size() > 0) {
                            Collections.sort(campaignsArray, new Comparator<Campaigns>() {
                                @Override
                                public int compare(final Campaigns object1, final Campaigns object2) {
                                    return object1.getName().compareTo(object2.getName());
                                }
                            });
                        }

                        if(campaignsArray.size() == 1){
                            for(int c=0; c<campaignsArray.size(); c++) {
                                Campaigns singleRow = campaignsArray.get(c);
                                Campaign campaign = new Campaign(singleRow.getColor(), singleRow.getDetails(), singleRow.getId(), singleRow.getCompany(), singleRow.getImage().getDefault_count(), singleRow.getKey(), singleRow.getImage(), singleRow.getLogo(), singleRow.getImage().getMax_count(), singleRow.getImage().getMin_count(), singleRow.getName(), null, null, null);
                                campaign.saveToDb();
                            }
                        }
                        responseOutput.append(jsonObject.getString("name")).append("  /  ");
                    }
                    SharedPreferencesHelper.getInstance(ManageContatcsAvtivity.this).setString("data",new Gson().toJson(campaignsArray));
                }
                br.close();

                output.append(System.getProperty("line.separator") + "Response " + System.getProperty("line.separator") + System.getProperty("line.separator") + responseOutput.toString());
                new GetContacts(ManageContatcsAvtivity.this).execute();

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute() {
            //progress.dismiss();
        }

    }

    private class GetCompany extends AsyncTask<String, Void, Void> {

        private final Context context;

        public GetCompany(Context c){
            this.context = c;
        }

        protected void onPreExecute(){

        }

        @Override
        protected Void doInBackground(String... params) {
            try {

                URL url = new URL(Constant.SERVER_URL+"company/ ");

                HttpURLConnection connection = (HttpURLConnection)url.openConnection();

                //String userCredentials = "96ea502e088bc46dd3e863f1c82db3e160a4a56a";
                byte[] encodedBytes = Base64.encode(userCredentials.getBytes(), 0);
                String tokenAuth = "Token " + userCredentials;/*new String(encodedBytes);*/
                connection.setRequestProperty ("Authorization", tokenAuth);
                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                connection.setRequestProperty("Content-Language", "en-US");
                connection.setUseCaches(false);
                connection.setDoInput(true);

                connection.setRequestMethod("GET");
                connection.setRequestProperty("USER-AGENT", "Mozilla/5.0");


                int responseCode = connection.getResponseCode();

                System.out.println("\nSending 'POST' request to URL : " + url);
                System.out.println("Post parameters : " + tokenAuth);
                System.out.println("Response Code : " + responseCode);

                BufferedReader in = new BufferedReader(
                        new InputStreamReader(connection.getInputStream()));
                String inputLine;
                final StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                //print result
                System.out.println("Login Response:  "+response.toString());


                            try {
                                String responseString = response.toString();
                                System.out.println(response.toString());
                                /**
                                 * Parse JSON response to Gson library
                                 */
                              JSONObject  jsonObject = new JSONObject(responseString);
                                int id = jsonObject.getInt("id");
                                String key = jsonObject.getString("key");
                                String name = jsonObject.getString("name");
                                String logo = jsonObject.getString("logo");
                                String status = jsonObject.getString("status");
                                String terms = jsonObject.getString("terms");
                                Log.e("Company Id", ""+id);
                                Log.e("Company Key", key);
                                Log.e("Company Name", name);
                                Log.e("Company Logo", logo);
                                Log.e("Company Status", status);
                                Log.e("Company Term", terms);

                                /*SharedPreferences.Editor editor = sharedpreferences.edit();
                                editor.putInt(Id, id);
                                editor.putString(Key, key);
                                editor.putString(Name, name);
                                editor.putString(Logo, logo);
                                editor.putString(Status, status);
                                editor.putString(Terms, terms);
                                editor.commit();*/

                                Company company = new Company(id, null, key, logo, name, status, terms, null);
                                company.saveToDb();

                            } catch (final JSONException e) {
                                e.printStackTrace();
                            }

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute() {
            //progress.dismiss();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    public String[] extractNameArray(ArrayList<ManageContacts> contactsArray)
    {
        int i = contactsArray.size();
        int n = ++i;
        contactsName = new String[n];
        for(int c=0;c<contactsArray.size();c++)
        {
            contactsName[c] = contactsArray.get(c).getName();
        }
        return contactsName;
    }

    public void alertMenuContact(View view) {
        new AlertView(null, null, null, null,
                new String[]{"Change Customer Type",
                        "Switch Companies",
                        "Get Help",
                },
                this, AlertView.Style.Alert, this).setCancelable(true).setOnDismissListener(this).show();
    }

    @Override
    public void onItemClick(Object o, int position) {

        if (position == 0) {

            if(campaignsArray.size() == 1){

                Intent campaignsDataIntent = new Intent(context,CampaignsDataActivity.class);
                campaignsDataIntent.putExtra("contactsName",contactsName);
                campaignsDataIntent.putExtra("campaignrow",campaignsArray.get(0));
                campaignsDataIntent.putExtra("contacts",manageContactsArray);
                campaignsDataIntent.putExtra("campaignArray", campaignsArray);
                startActivity(campaignsDataIntent);

            }else {
                Intent contactIntent = new Intent(ManageContatcsAvtivity.this, CampaignsActivity.class);
                contactIntent.putExtra("contactsName", contactsName);
                contactIntent.putExtra("contacts", manageContactsArray);
                contactIntent.putExtra("campaignArray", campaignsArray);
                startActivity(contactIntent);
                finish();
            }

           /* Intent contactIntent = new Intent(ManageContatcsAvtivity.this, CampaignsActivity.class);
            contactIntent.putExtra("contactsName", contactsName);
            contactIntent.putExtra("contacts", manageContactsArray);
            startActivity(contactIntent);*/

        } else if (position == 1) {

            Intent logoutIntent = new Intent(ManageContatcsAvtivity.this, LogOutActivity.class);
            startActivity(logoutIntent);

        } else if (position == 2) {

            Intent helpIntent = new Intent(ManageContatcsAvtivity.this, GetHelpActivity.class);
            startActivity(helpIntent);
        }


    }

    @Override
    public void onDismiss(Object o) {


    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
//            if(mAlertView!=null && mAlertView.isShowing()){
//                mAlertView.dismiss();
//                return false;
//            }
        }

        return super.onKeyDown(keyCode, event);

    }


    public boolean isOnline(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        //should check null because in airplane mode it will be null
        return (netInfo != null && netInfo.isConnected());

    }

}
