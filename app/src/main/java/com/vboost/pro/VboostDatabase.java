package com.vboost.pro;

import android.content.ContentValues;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.vboost.pro.DbHelper.DatabaseHelper;
import com.vboost.pro.service.MyApplication;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;

/**
 * Created by saqib on 9/8/2015.
 */
public class VboostDatabase  {

    private static String DB_PATH = "/data/data/com.vboost.pro/databases/";

    private static String DB_NAME = "vboost.sqlite";

    private SQLiteDatabase myDataBase;

    private static final String LOG = "DatabaseHelper";

    // Database Version
    private static final int DATABASE_VERSION = 2;

    // Database Name
    private static final String DATABASE_NAME = "vboost.sqlite";
    protected static SQLiteOpenHelper sqLiteOpenHelper;


    public static Cursor selectDataForQuery(String query) {

        if(sqLiteOpenHelper == null) {
            sqLiteOpenHelper = new SQLiteOpenHelper(MyApplication.context, DB_NAME, null, 1) {
                @Override
                public void onCreate(SQLiteDatabase db) {

                }

                @Override
                public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

                }
            };
        }
        SQLiteDatabase db = sqLiteOpenHelper.getReadableDatabase();


        Log.e(LOG, query);

        Cursor c = db.rawQuery(query, null);

        int count = c.getCount();
        if (c != null)
            c.moveToFirst();


        return c;


    }

    public static String getString(Cursor cursor, int columnId) {
        try {
            return cursor.getString(15);
        } catch (Exception e) {
            return "";
        }
    }

    public static long insertDataForQuery(String table, ContentValues values) {

        SQLiteDatabase db = sqLiteOpenHelper.getWritableDatabase();

        // insert row
        long tag_id = db.insert(table, null, values);

        return tag_id;
    }

    public static long updateDataForQuery(String table, ContentValues values, String whereClause) {


        SQLiteDatabase db = sqLiteOpenHelper.getWritableDatabase();

        // updating row
        return db.update(table, values, whereClause, null);

    }

    public static boolean CheckIsDataAlreadyInDBorNot(String TableName, String dbfield, String fieldValue) {
        SQLiteDatabase sqldb = sqLiteOpenHelper.getReadableDatabase();
        String Query = "Select * from " + TableName + " where " + dbfield + " = " + fieldValue;
        Cursor cursor = sqldb.rawQuery(Query, null);
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }


    public void createDataBase(Context context) throws IOException {
        sqLiteOpenHelper = new SQLiteOpenHelper(context, DB_NAME, null, 1) {
            @Override
            public void onCreate(SQLiteDatabase db) {

            }

            @Override
            public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

            }
        };

        boolean dbExist = checkDataBase();

        if (dbExist) {

            //do nothing - database already exist

        } else {

            //By calling this method and empty database will be created into the default system path
            //of your application so we are gonna be able to overwrite that database with our database.
            sqLiteOpenHelper.getReadableDatabase();

            try {

                copyDataBase(context);

            } catch (IOException e) {

                throw new Error("Error copying database");

            }
        }

    }

    /**
     * Check if the database already exist to avoid re-copying the file each time you open the application.
     *
     * @return true if it exists, false if it doesn't
     */
    private boolean checkDataBase() {

        SQLiteDatabase checkDB = null;

        try {
            String myPath = DB_PATH + DB_NAME;
            File file = new File(myPath);
            if (file.exists() && !file.isDirectory()) {
                checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
            } else {
                System.out.println("Database Can not open");
            }

        } catch (SQLiteException e) {

            //database does't exist yet.

        }

        if (checkDB != null) {

            checkDB.close();

        }

        return checkDB != null ? true : false;
    }

    /**
     * Copies your database from your local assets-folder to the just created empty database in the
     * system folder, from where it can be accessed and handled.
     * This is done by transfering bytestream.
     */
    private void copyDataBase(Context context) throws IOException {

        //Open your local db as the input stream
        InputStream myInput = context.getAssets().open(DB_NAME);

        // Path to the just created empty db
        String outFileName = DB_PATH + DB_NAME;

        //Open the empty db as the output stream
        OutputStream myOutput = new FileOutputStream(outFileName);

        //transfer bytes from the inputfile to the outputfile
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);

        }

        Log.e("Data Copy", "Suucess");

        //Close the streams
        myOutput.flush();
        myOutput.close();
        myInput.close();
    }

    public void openDataBase() throws SQLException {

        //Open the database
        String myPath = DB_PATH + DB_NAME;
        myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);


    }
}
