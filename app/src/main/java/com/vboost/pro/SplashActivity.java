package com.vboost.pro;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.SQLException;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import com.vboost.pro.DbHelper.DatabaseHelper;
import com.vboost.pro.Util.Constant;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class SplashActivity extends AppCompatActivity {

    Button getStarted;

    private SharedPreferences sharedpreferences;
    private SharedPreferences.Editor editor;
    boolean login;

    private static final int PERMISSION_REQUEST_CODE_STORAGE = 1;
    private static final int PERMISSION_REQUEST_CODE_CAMERA = 2;
    private static final int PERMISSION_REQUEST_CODE_CACHE = 3;
    private static final int REQUEST_APP_SETTINGS = 168;

    private static final String[] requiredPermissions = new String[]{
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
            /* ETC.. */
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash);

        sharedpreferences = getSharedPreferences(Constant.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = getSharedPreferences(Constant.MyPREFERENCES, MODE_PRIVATE).edit();

        login = sharedpreferences.getBoolean(Constant.Login, false);
        getStarted = (Button) findViewById(R.id.splash_button_get_started);
        getStarted.setAllCaps(false);
        getStarted.getBackground().setColorFilter(new LightingColorFilter(Color.parseColor("#8ec63e"), 0x00000000));

        if (Build.VERSION.SDK_INT >= 23) {
            if (checkPermissionStorage()) {
                Log.e("value", "Permission already Granted, Now you can get image.");
                VboostDatabase dbHelper = new VboostDatabase();
                try {
                    dbHelper.createDataBase(this);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                checkForLogin();

            } else {
                requestPermissionStorage();
            }
        } else {

            VboostDatabase dbHelper = new VboostDatabase();
            try {
                dbHelper.createDataBase(this);
            } catch (IOException e) {
                e.printStackTrace();
            }
            checkForLogin();
            Log.e("value", "Not required for requesting runtime permission");
        }


        //databaseExport();
    }

    private void checkForLogin() {
        nextActivity(null);
    }

    public void nextActivity(View view) {

        if (login) {
            Intent splashIntent = new Intent(SplashActivity.this, LoginActivity.class);
            startActivity(splashIntent);
            finish();
        } else {
            Intent splashIntent = new Intent(SplashActivity.this, LoginActivity.class);
            startActivity(splashIntent);
            finish();
        }
    }
    
     /*Runtime Permission Code Start Here*/

    private boolean checkPermissionStorage() {
        int result = ContextCompat.checkSelfPermission(SplashActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkPermissionCamera() {
        int result = ContextCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkPermissionCache() {
        int result = ContextCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.CLEAR_APP_CACHE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermissionStorage() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(SplashActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(SplashActivity.this, "Write External Storage permission allows us to do store images. Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(SplashActivity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE_STORAGE);
        }
    }

    private void requestPermissionCamera() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(SplashActivity.this, Manifest.permission.CAMERA)) {
            Toast.makeText(SplashActivity.this, "Write camera permission allows us to do capture images. Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(SplashActivity.this, new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST_CODE_CAMERA);
        }
    }

    private void requestPermissionCache() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(SplashActivity.this, Manifest.permission.CLEAR_APP_CACHE)) {
            Toast.makeText(SplashActivity.this, "Write camera permission allows us to do clear cache. Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(SplashActivity.this, new String[]{Manifest.permission.CLEAR_APP_CACHE}, PERMISSION_REQUEST_CODE_CACHE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    VboostDatabase dbHelper = new VboostDatabase();
                    try {
                        dbHelper.createDataBase(this);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if (checkPermissionCamera()) {

                        Log.e("value", "Permission already Granted, Now you can capture image.");
                    } else {
                        requestPermissionCamera();
                    }

                    Log.e("value", "Permission Granted, Now you can save image .");
                } else {

                    new AlertDialog.Builder(this)
                            .setTitle("Reguired Permission")
                            .setMessage("Please allow storage and camera permissions for go to next screen.")
                            .setPositiveButton("Allow Permission", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    if (Build.VERSION.SDK_INT > 22 && !hasPermissions(requiredPermissions)) {
                                        goToSettings();
                                    }
                                }
                            })
                            .setNegativeButton("Close App", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();

                }
                break;

            case PERMISSION_REQUEST_CODE_CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("value", "Permission Granted, Now you can capture image .");

                    if (checkPermissionCache()) {

                        Log.e("value", "Permission already Granted, Now you can clear Cache.");
                    } else {
                        requestPermissionCache();
                    }

                } else {
                    new AlertDialog.Builder(this)
                            .setTitle("Reguired Permission")
                            .setMessage("Please allow storage and camera permissions for go to next screen.")
                            .setPositiveButton("Allow Permission", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    if (Build.VERSION.SDK_INT > 22 && !hasPermissions(requiredPermissions)) {
                                        goToSettings();
                                    }
                                }
                            })
                            .setNegativeButton("Close App", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                    ;
                }
                break;
            case PERMISSION_REQUEST_CODE_CACHE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("value", "Permission Granted, Now you can clear Cache");

                } else {
                    Log.e("value", "Permission Denied, You cannot clear cache.");
                }
                break;

        }
    }

    public void databaseExport() {
        File f = new File("/data/data/com.saqibdb.vboost.pro/databases/vboost.sqlite");
        FileInputStream fis = null;
        FileOutputStream fos = null;

        try {
            fis = new FileInputStream(f);
            fos = new FileOutputStream("/storage/emulated/0" + "/vboost.sqlite");
            while (true) {
                int i = fis.read();
                if (i != -1) {
                    fos.write(i);
                } else {
                    break;
                }
            }
            fos.flush();
            Toast.makeText(this, "DB dump OK", Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Error from database", e.toString());
            Toast.makeText(this, e.toString() + "DB dump ERROR", Toast.LENGTH_LONG).show();
        } finally {
            try {
                fos.close();
                fis.close();
            } catch (Exception ioe) {
            }
        }
    }

    public void openDataBase() {

        DatabaseHelper myDbHelper = new DatabaseHelper(SplashActivity.this);
        try {

            myDbHelper.createDataBase();

        } catch (IOException ioe) {

            throw new Error("Unable to create database");

        }

        try {

            myDbHelper.openDataBase();

        } catch (SQLException sqle) {

            throw sqle;

        }
        Toast.makeText(SplashActivity.this, "Success", Toast.LENGTH_SHORT).show();

    }

    private void goToSettings() {
        Intent myAppSettings = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + getPackageName()));
        myAppSettings.addCategory(Intent.CATEGORY_DEFAULT);
        myAppSettings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivityForResult(myAppSettings, REQUEST_APP_SETTINGS);
    }

    public boolean hasPermissions(String[] permissions) {
        for (String permission : permissions)
            if (PackageManager.PERMISSION_GRANTED != checkSelfPermission(permission))
                return false;
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_APP_SETTINGS) {
            if (hasPermissions(requiredPermissions)) {
                VboostDatabase dbHelper = new VboostDatabase();
                try {
                    dbHelper.createDataBase(this);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                // Toast.makeText(this, "Permissions not granted.", Toast.LENGTH_LONG).show();
                finish();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
