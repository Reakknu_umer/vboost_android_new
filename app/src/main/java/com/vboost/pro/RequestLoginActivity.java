package com.vboost.pro;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;

import com.bigkoo.alertview.AlertView;
import com.bigkoo.alertview.OnItemClickListener;
import com.vboost.pro.DBClasses.NewUser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Nabeel Hafeez on 8/15/2016.
 */
public class RequestLoginActivity extends AppCompatActivity implements OnItemClickListener {

    Button submitButton;

    EditText companyNameEditText;
    EditText flNameEditText;
    EditText emailEditText;
    EditText phoneEditText;

    String CompanyName;
    String fullName;
    String email;
    String phone;

    private final String USER_AGENT = "Mozilla/5.0";
    private ProgressDialog progress;

    String emailPattern ;
    int keyDel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_login);

        submitButton = (Button) findViewById(R.id.request_login_submit_button);
        submitButton.setAllCaps(false);
        submitButton.getBackground().setColorFilter(new LightingColorFilter(Color.parseColor("#8ec63e"), 0x00000000));

        companyNameEditText = (EditText) findViewById(R.id.request_login_company_name_edit_text);
        flNameEditText = (EditText) findViewById(R.id.request_login_name_edit_text);
        emailEditText = (EditText) findViewById(R.id.request_login_email_edit_text);
        phoneEditText = (EditText) findViewById(R.id.request_login_phone_edit_text);

        companyNameEditText.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        companyNameEditText.setSingleLine(true);

        flNameEditText.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        flNameEditText.setSingleLine(true);

        emailEditText.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        emailEditText.setSingleLine(true);

        phoneEditText.setImeOptions(EditorInfo.IME_ACTION_DONE);
        phoneEditText.setSingleLine(true);

        emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        phoneEditText.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {

                boolean flag = true;
                String eachBlock[] = phoneEditText.getText().toString().split("-");
                for (int i = 0; i < eachBlock.length; i++)
                {
                    if (eachBlock[i].length() > 3)
                    {
                        Log.v("11111111111111111111","cc"+flag + eachBlock[i].length());
                    }
                }
                if (flag) {
                    phoneEditText.setOnKeyListener(new View.OnKeyListener() {

                        public boolean onKey(View v, int keyCode, KeyEvent event) {

                            if (keyCode == KeyEvent.KEYCODE_DEL)
                                keyDel = 1;
                            return false;
                        }
                    });

                    if (keyDel == 0) {

                        if (((phoneEditText.getText().length() + 1) % 4) == 0)
                        {
                            if (phoneEditText.getText().toString().split("-").length <= 2)
                            {
                                phoneEditText.setText(phoneEditText.getText() + "-");
                                phoneEditText.setSelection(phoneEditText.getText().length());
                            }
                        }
                        phone = phoneEditText.getText().toString();
                    }
                    else
                    {
                        phone = phoneEditText.getText().toString();
                        keyDel = 0;
                    }

                } else {
                    phoneEditText.setText(phone);
                }

            }

            public void beforeTextChanged(CharSequence s, int start, int count,int after)
            {


            }

            public void afterTextChanged(Editable s) {


            }


        });

    }

    public void submitRequestButton(View view){

        if (companyNameEditText != null && flNameEditText != null && emailEditText != null && phoneEditText != null) {

            CompanyName = companyNameEditText.getText().toString();
            fullName = flNameEditText.getText().toString();
            email = emailEditText.getText().toString();
            phone = phoneEditText.getText().toString();


            if (email.matches(emailPattern)) {

                if (phone.length() < 12) {
                    new AlertView(null,"Please Enter 10 digit phone number!",null,null,new String[]{"OK"},this,AlertView.Style.Alert,this).show();
                } else {
                    if(isOnline(RequestLoginActivity.this)) {

                        new ReguestLoginVboost(this).execute();
                    }else{


                    }
                }
            } else {
                new AlertView(null,"Please enter valid email address!",null,null,new String[]{"OK"},this,AlertView.Style.Alert,this).show();
            }
        } else {
            if (companyNameEditText == null) {

                new AlertView(null, "Please enter company name.", null, null, new String[]{"OK"}, this, AlertView.Style.Alert, this).show();

            } else if (flNameEditText == null) {

                new AlertView(null, "Please enter your first and last name.", null, null, new String[]{"OK"}, this, AlertView.Style.Alert, this).show();

            } else if (emailEditText == null) {

                new AlertView(null, "Please enter a valid email address.", null, null, new String[]{"OK"}, this, AlertView.Style.Alert, this).show();

            } else if (phoneEditText == null) {

                new AlertView(null, "Please enter your cell phone number.", null, null, new String[]{"OK"}, this, AlertView.Style.Alert, this).show();

            }
        }

    }

    public void requestLoginBack(View view){

        finish();
    }

    public String  createJson(){

        try {
            JSONObject jsonObjectparent = new JSONObject();
            JSONObject jsonObjectMessage = new JSONObject();
            JSONObject jsonObjectHeader = new JSONObject();
            JSONObject jsonObjectTo = new JSONObject();
            JSONArray jsonArrayTo = new JSONArray();

            jsonObjectHeader.put("Reply-To", email);

            jsonArrayTo.put(jsonObjectTo);
            jsonObjectTo.put("email", "support@vboost.com");
            jsonObjectTo.put("name", "Vboost App");
            jsonObjectTo.put("type", "to");

            jsonObjectMessage.put("images", "");
            jsonObjectMessage.put("subject", "Vboost Mobile App Help");
            jsonObjectMessage.put("headers", jsonObjectHeader);
            jsonObjectMessage.put("text", "Company : "+CompanyName+" , Name : "+fullName+" , Email : "+email+" , Phone number : "+phone);
            jsonObjectMessage.put("from_email", "support@vboost.com");
            jsonObjectMessage.put("to", jsonArrayTo);
            jsonObjectMessage.put("from_name", fullName);

            jsonObjectparent.put("key", "l7g-gBd9VN05QgxGkpar6A");
            jsonObjectparent.put("send_at", "");
            jsonObjectparent.put("message", jsonObjectMessage);

            NewUser newUser = new NewUser(CompanyName,email, fullName, phone);
            newUser.saveToDb();

            Log.d("output", jsonObjectparent.toString(2));
            return jsonObjectparent.toString(2);

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void onItemClick(Object o,int position) {

    }


    private class ReguestLoginVboost extends AsyncTask<String, Void, Void> {

        private final Context context;

        public ReguestLoginVboost(Context c) {
            this.context = c;
        }

        protected void onPreExecute() {
            progress = new ProgressDialog(this.context);
            progress.setMessage("Request Send Please wait...");
            progress.show();
        }

        @Override
        protected Void doInBackground(String... params) {


            try {

                URL url = new URL("https://mandrillapp.com/api/1.0/messages/send.json");

                HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                //add reuqest header
                connection.setRequestMethod("POST");
                connection.setRequestProperty("User-Agent", USER_AGENT);
                connection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

                String urlParameters = createJson();

                // Send post request
                connection.setDoOutput(true);
                DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
                wr.writeBytes(urlParameters);
                wr.flush();
                wr.close();

                final int responseCode = connection.getResponseCode();
                System.out.println("\nSending 'POST' request to URL : " + url);
                System.out.println("Post parameters : " + urlParameters);
                System.out.println("Response Code : " + responseCode);

                BufferedReader in = new BufferedReader(
                        new InputStreamReader(connection.getInputStream()));
                String inputLine;
                final StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                //print result
                System.out.println(response.toString());

                RequestLoginActivity.this.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {

                        if (responseCode == 200) {
                            progress.dismiss();
                            NewUser.destroy();
                            System.out.println("Code Success : " + responseCode);
                            new AlertView("","Your Request Submit Successfully!",null,null,new String[]{"OK"},RequestLoginActivity.this,AlertView.Style.Alert,new OnItemClickListener() {
                                @Override
                                public void onItemClick(Object o,int position) {

                                }
                            }).show();

                        } else if (responseCode == 400) {

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progress.dismiss();
                                    System.out.println("Code Fail : " + responseCode);
                                   // mAlertViewExt.show();
                                }
                            });

                        }
                    }
                });

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                Log.e("Mal exc", ""+e);
                progress.dismiss();
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                Log.e("IO exc", ""+e.getMessage()+":  "+e.getCause());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progress.dismiss();
                        //networkErrorAlert.show();
                    }
                });
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute() {
            //progress.dismiss();
        }

    }

    public boolean isOnline(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        //should check null because in airplane mode it will be null
        return (netInfo != null && netInfo.isConnected());

    }

}
