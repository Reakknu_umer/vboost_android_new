package com.vboost.pro;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bigkoo.alertview.AlertView;
import com.bigkoo.alertview.OnDismissListener;
import com.bigkoo.alertview.OnItemClickListener;
import com.kyanogen.signatureview.SignatureView;

import com.squareup.otto.Subscribe;
import com.vboost.pro.Adapter.CampaignsDataAdapter;
import com.vboost.pro.DBClasses.Company;
import com.vboost.pro.DBClasses.DataLabels;
import com.vboost.pro.DBClasses.Preference;
import com.vboost.pro.Network.HttpPatch;
import com.vboost.pro.Network.NetworkChangeReceiver;
import com.vboost.pro.Util.Constant;
import com.vboost.pro.event.NetworkRequestFailedEvent;
import com.vboost.pro.event.PhotoUploadResponseEvent;
import com.vboost.pro.model.PhotoAndName;
import com.vboost.pro.model.PhotoAndNameStr;
import com.vboost.pro.service.ApiClient;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;

import java.io.File;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class SignatureActivity extends AppCompatActivity implements OnItemClickListener, OnDismissListener {

    private SignatureView signatureView;
    Button clearButton;
    Button okButton;
    TextView signatureText;
    ImageButton acceptTermCondition;
    TextView hardCodedTerms;
    LinearLayout termsCheckBoxLayout;

    boolean accept = false;
    //globally
    private boolean isSignatured = false;

    // String[] imageArray;
    String name;
    String deliveryMethod;
    String flag;
    int campaignId;
    int contactId;
    File file;
    String email;
    String phone;

    private ProgressDialog progress;
    public static final String MyPREFERENCES = "MyPrefs";
    public static final String Token = "tokenKey";
    SharedPreferences sharedpreferences;

    String userCredentials;
    String signature;
    List<DataLabels> imagesAndLabels;

    ApiClient client;
    ArrayList<Uri> browsedUris = new ArrayList<>();
    ArrayList<PhotoAndName> photoAndNameArrayList = new ArrayList<>();
    ArrayList<PhotoAndNameStr> photoAndNameStrArrayList = new ArrayList<>();
    //ArrayList<String> imagesTitle = new ArrayList<>();
    //List<byte[]> fileList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // int orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
        //setRequestedOrientation(orientation);
        setContentView(R.layout.activity_signature_demo);//see xml layout

        initializeUI();

    }

    List<Company> company;

    public void initializeUI() {

        progress = new ProgressDialog(this);
        progress.setMessage("Please wait...");

        signatureView = (SignatureView) findViewById(R.id.signature_view);
        clearButton = (Button) findViewById(R.id.signature_clear_button);
        okButton = (Button) findViewById(R.id.signatur_ok_button);
        acceptTermCondition = (ImageButton) findViewById(R.id.accept_terms_and_conditions);
        hardCodedTerms = (TextView) findViewById(R.id.terms_hard_code_text_view);
        signatureText = (TextView) findViewById(R.id.signature_hint_text);
        termsCheckBoxLayout = (LinearLayout) findViewById(R.id.terms_check_box_layout);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        userCredentials = sharedpreferences.getString(Token, null);
        client = ApiClient.getInstance(getApplicationContext());

        signatureView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                signatureText.setVisibility(View.INVISIBLE);
                isSignatured = true;
                return false;
            }
        });


        acceptTermCondition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (accept) {
                    accept = false;
                    acceptTermCondition.setImageResource(0);
                    acceptTermCondition.setBackgroundResource(R.drawable.r_un);
                } else {
                    accept = true;
                    acceptTermCondition.setImageResource(0);
                    acceptTermCondition.setBackgroundResource(R.drawable.r_sel);

                }

            }
        });

        clearButton.getBackground().setColorFilter(new LightingColorFilter(Color.parseColor("#FBA919"), 0x00000000));
        okButton.getBackground().setColorFilter(new LightingColorFilter(Color.parseColor("#FBA919"), 0x00000000));

        name = getIntent().getStringExtra("name");
        deliveryMethod = getIntent().getStringExtra("deliverMethod");
        campaignId = getIntent().getIntExtra("campaignID", -1);
        contactId = getIntent().getIntExtra("contactID", -1);
        flag = getIntent().getStringExtra("deliveryFlag");

        if (flag.equals("email")) {
            email = deliveryMethod;
            phone = null;
        } else {
            phone = deliveryMethod;
            email = null;
        }




        /*try {
            theString2 = IOUtils.toString(new FileInputStream(new File("/storage/emulated/0/Download/JsonArray.txt")), "UTF-8");
            newsignature = IOUtils.toString(new FileInputStream(new File("/storage/emulated/0/Download/signature.txt")), "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }*/


//        Toast.makeText(getApplicationContext(), "Second Place", Toast.LENGTH_SHORT);



       /* for (int i = 0; i < imagesAndLabels.size(); i++) {
            DataLabels dataLabels = imagesAndLabels.get(i);
            Uri myUriImagePath = Uri.parse(dataLabels.getImage());
            imagesTitle.add(dataLabels.getTitle());
            browsedUris.add(myUriImagePath);
        }

        try {
            for (Uri uri : browsedUris) {
                InputStream imageStream = null;
                try {
                    imageStream = getContentResolver().openInputStream(uri);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                Bitmap compressBitmap = BitmapFactory.decodeStream(imageStream);

                ByteArrayOutputStream outStream = new ByteArrayOutputStream();
                compressBitmap.compress(Bitmap.CompressFormat.JPEG, 20, outStream);
                fileList.add(outStream.toByteArray());
                // fileList.add(IOUtils.toByteArray(this.getContentResolver().openInputStream(uri)));
            }
        } catch (Exception e) {
            throw new IllegalStateException("Could not read file", e);
        }*/

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                okSignature();
            }
        });

        new android.os.AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {


                company = Company.getAll();
                imagesAndLabels = DataLabels.getAll();
                createByteArraylist();

                return null;
            }

            @Override
            protected void onPreExecute() {
                progress.setMessage("Fetching data...");
                progress.show();
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                if (progress != null && progress.isShowing())
                    progress.dismiss();
                if (company != null && company.size() > 0) {
                    String termsText = company.get(0).getTerms();
                    if (termsText.equals("") || termsText == null) {
                        accept = true;
                        termsCheckBoxLayout.setVisibility(View.INVISIBLE);
                        hardCodedTerms.setVisibility(View.VISIBLE);
                    } else {
                        hardCodedTerms.setVisibility(View.INVISIBLE);
                        termsCheckBoxLayout.setVisibility(View.VISIBLE);
                        acceptTermCondition.setImageResource(0);
                        acceptTermCondition.setBackgroundResource(R.drawable.r_un);
                    }
                }
                super.onPostExecute(aVoid);
            }
        }.execute();

    }

    public void createByteArraylist() {
        for (DataLabels dataLabels : imagesAndLabels) {

            String imageTitle = dataLabels.getTitle();

            try {
//                Bitmap bitmap = CampaignsDataAdapter.decodeFile(this, dataLabels.getImage(), 600);

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                Bitmap bitmap = BitmapFactory.decodeFile(dataLabels.getImage(), options);
                bitmap = NetworkChangeReceiver.rotatedBitmap(bitmap, dataLabels.getImage());

                ByteArrayOutputStream outStream = new ByteArrayOutputStream();
//            compressBitmap.compress(Bitmap.CompressFormat.JPEG, 10, outStream);

                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
//                PhotoAndName photoAndName = new PhotoAndName();
                // photoAndName.setPhotos(outStream.toByteArray());
//                photoAndName.setName(imageTitle);
                String imageString = Base64.encodeToString(outStream.toByteArray(), Base64.DEFAULT);
//                photoAndName.setImage(imageString);
//                photoAndNameArrayList.add(photoAndName);

                PhotoAndNameStr str = new PhotoAndNameStr();
                str.setName(imageTitle);
                str.setPhotos(imageString);
                photoAndNameStrArrayList.add(str);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

//    public void onConfigurationChanged(Configuration newConfig) {
//        super.onConfigurationChanged(newConfig);
//
//        int orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
//        setRequestedOrientation(orientation);
//
//        setContentView(R.layout.activity_signature);//see xml layout
//
//        initializeUI();
//    }

    public void openTermsandConditons(View view) {

        Intent termIntent = new Intent(SignatureActivity.this, TermandCondtions.class);
        startActivity(termIntent);

    }

    public void signatureBack(View view) {

        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();
    }


    public void clearSignature(View view) {

        signatureView.clearCanvas();//Clear SignatureView
        signatureText.setVisibility(View.VISIBLE);
        isSignatured = false;
        Toast.makeText(getApplicationContext(),
                "Clear canvas", Toast.LENGTH_SHORT).show();

    }

    public String getUserAgent(){
        return "Vboost Pro Android "+Build.VERSION.SDK_INT;
    }
    
    public void okSignature() {

        if (isSignatured) {
            //do next process

            if (accept) {
                progress.setMessage("Please wait...");
                progress.show();
                File sdCard = Environment.getExternalStorageDirectory();
                File directory = new File(sdCard.getAbsolutePath() + "/Vboost");
//            File directory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                file = new File(directory, System.currentTimeMillis() + "");
                FileOutputStream out = null;
                Bitmap bitmap = signatureView.getSignatureBitmap();
                try {
                    out = new FileOutputStream(file);
                    if (bitmap != null) {
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                    } else {
                        throw new FileNotFoundException();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (out != null) {
                            out.flush();
                            out.close();

                            if (bitmap != null) {
//                        Toast.makeText(getApplicationContext(),
//                                "Image saved successfully at "+ file.getPath(),Toast.LENGTH_LONG).show();
                                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                                    new MyMediaScanner(this, file);
                                } else {
                                    ArrayList<String> toBeScanned = new ArrayList<String>();
                                    toBeScanned.add(file.getAbsolutePath());
                                    String[] toBeScannedStr = new String[toBeScanned.size()];
                                    toBeScannedStr = toBeScanned.toArray(toBeScannedStr);
                                    MediaScannerConnection.scanFile(this, toBeScannedStr, null, null);
                                }
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                if (file.getAbsolutePath() == null) {
                    signature = "";
                } else {
                    signature = imageStringFormat(file.getAbsolutePath());
                }

                /*Preference preference = new Preference(campaignId, contactId, email, name, "1", phone, signature, null, null, null, null, null);
                preference.saveToDb();

                if (isOnline(this)) {

                    client.getBus().post(new PhotoUploadRequestEvent(photoAndNameArrayList, campaignId, contactId, name, email, 1, phone, signature));

                } else {

                    *//*Intent intent = new Intent("my.action.string");
                    intent.putExtra("userCredentials", userCredentials);
                    sendBroadcast(intent);
                    progress.dismiss();
                    Intent intentback = new Intent();
                    setResult(RESULT_OK, intentback);
                    finish();
*//*
                    ViewRetryDialog viewRetryDialog = new ViewRetryDialog();
                    viewRetryDialog.showDialog(SignatureActivity.this);
                }

            } else {
                progress.dismiss();
                new AlertView(null, "\nYou must agree to the terms and conditions.", null, null, new String[]{"OK"}, this, AlertView.Style.Alert, this).show();
                //Toast.makeText(SignatureActivity.this, "Please accept term and conditions first.", Toast.LENGTH_LONG).show();
            }*/
                countDownTimer = new CountDownTimer(45000, 1000) {

                    public void onTick(long millisUntilFinished) {
                        boolean isConnected = Constant.isConnected();
                        Log.e("TAG", "Is Connected : " + isConnected + " with sec : " + millisUntilFinished);
                        if (isConnected) {
                            if (countDownTimer != null) {
                                countDownTimer.cancel();
                            }
                            new SendPostRequest().execute();
                        }
                    }

                    public void onFinish() {
                        if (!Constant.isConnected()) {
                            if (progress != null && progress.isShowing())
                                progress.dismiss();
                            SignatureActivity.ViewRetryDialog viewRetryDialog = new ViewRetryDialog();
                            viewRetryDialog.showDialog(SignatureActivity.this);
                        }
                    }

                }.start();

                //start for minute
                countDownTimerEnd = new CountDownTimer(60000, 1000) {

                    public void onTick(long millisUntilFinished) {

                    }

                    public void onFinish() {
                        if (isUploadingInProcess) {
                            if(progress != null && progress.isShowing())
                                progress.dismiss();
                            isTaskCanceled = true;
                            SignatureActivity.ViewRetryDialog viewRetryDialog = new SignatureActivity.ViewRetryDialog();
                            viewRetryDialog.showDialog(SignatureActivity.this);
                        }
                    }

                }.start();
            }
        } else {
            new AlertView(null, "\nPlease enter your signature.", null, null, new String[]{"OK"}, this, AlertView.Style.Alert, this).show();
            //intimate to sign
        }
    }

    CountDownTimer countDownTimer = null;
    boolean isUploadingInProcess = false;
    boolean isTaskCanceled = false;
    CountDownTimer countDownTimerEnd = null;

    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while (itr.hasNext()) {

            String key = itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    String strSignaturePackId = "";

    /*
    * Used for Send Signature request
    */
    public class SendPostRequest extends AsyncTask<String, Void, String> {

        protected void onPreExecute() {
            if(progress == null)
                progress = new ProgressDialog(SignatureActivity.this);
            isUploadingInProcess = true;
            isTaskCanceled = false;
            progress.setCancelable(false);
            progress.setMessage("Please wait...");
            if(!progress.isShowing())
                progress.show();
        }

        protected String doInBackground(String... arg0) {

            return sendSignaturePostReq();

        }

        @Override
        protected void onPostExecute(String result) {

            if (result.equalsIgnoreCase(getString(R.string.did_not_work))) {
                if (progress != null && progress.isShowing())
                    progress.dismiss();
                SignatureActivity.ViewRetryDialog viewRetryDialog = new ViewRetryDialog();
                viewRetryDialog.showDialog(SignatureActivity.this);
            } else {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.has("id")) {
                        strSignaturePackId = jsonObject.getString("id");
                        uploadingImage();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
//            Toast.makeText(getApplicationContext(), "Post : " + result, Toast.LENGTH_LONG).show();
            }
        }
    }


    public class ViewRetryDialog {

        public void showDialog(Activity activity) {
            final Dialog dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.retry_alert_new);

            dialog.findViewById(R.id.a).setVisibility(View.GONE);
//            ((TextView) dialog.findViewById(R.id.text_dialog)).setText(Html.fromHtml(getString(R.string.txt_error_submit_images)));
            Button dialogButton = (Button) dialog.findViewById(R.id.retry_button_dialog);
            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    if (isOnline(SignatureActivity.this)) {
                        if (progress == null)
                            progress = new ProgressDialog(SignatureActivity.this);
                        progress.setMessage("Please wait...");
//                        if (!isFinishing() && progress != null && !progress.isShowing())
//                            progress.show();
                        dialog.dismiss();
                        new SendPostRequest().execute();
                        //   client.getBus().post(new PhotoUploadRequestEvent(photoAndNameArrayList, campaignId, contactId, name, email, 0, phone, ""));
                    } else {
                        SignatureActivity.ViewRetryDialog viewRetryDialog = new ViewRetryDialog();
                        viewRetryDialog.showDialog(SignatureActivity.this);
                    }
                }
            });

            if(!isFinishing())
                dialog.show();

        }
    }


    /*
    * Used for Send Image request with image as a string
    */
    public class SendImagePostRequest extends AsyncTask<String, String, String> {

        protected void onPreExecute() {

        }

        protected String doInBackground(String... arg0) {

            InputStream inputStream = null;
            try {

                HttpParams httpParameters = new BasicHttpParams();
                ConnManagerParams.setTimeout(httpParameters, 30000);
                HttpConnectionParams.setConnectionTimeout(httpParameters, 30000);
                HttpConnectionParams.setSoTimeout(httpParameters, 30000);
                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient(httpParameters);

                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(Constant.SERVER_URL + "packageimage/?format=json");
                String json = "";

                // 3. build jsonObject
                JSONObject postDataParams = new JSONObject();
                postDataParams.put("package", arg0[0]);
                postDataParams.put("name", "front");
                postDataParams.put("image", arg0[1]);

                // 4. convert JSONObject to JSON to String
                json = postDataParams.toString();

                // 5. set json to StringEntity
                StringEntity se = new StringEntity(json);

                // 6. set httpPost Entity
                httpPost.setEntity(se);

                // 7. Set some headers to inform server about the type of the content
                httpPost.setHeader("Content-Type", "application/json");
                httpPost.setHeader("Authorization", "Token " + userCredentials);
                httpPost.setHeader("User-Agent", getUserAgent());
                httpPost.setHeader("Accept", "application/json");

                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPost);

                // 9. receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // 10. convert inputstream to string
                if (inputStream != null)
                    return convertInputStreamToString(inputStream);
                else
                    return getString(R.string.did_not_work);

            } catch (Exception e) {
                return getString(R.string.did_not_work);
                //   return e.toString();
            }
        }

        @Override
        protected void onPostExecute(String result) {

            if (result == null) {
                if (progress != null && progress.isShowing())
                    progress.dismiss();
                Toast.makeText(SignatureActivity.this, R.string.txt_error_in_image_uploading, Toast.LENGTH_SHORT).show();
            } else {
                if (result.equalsIgnoreCase(getString(R.string.did_not_work))) {
                    if (progress != null && progress.isShowing())
                        progress.dismiss();
                    SignatureActivity.ViewRetryDialog viewRetryDialog = new ViewRetryDialog();
                    viewRetryDialog.showDialog(SignatureActivity.this);
                } else {
                    completionCOunt = completionCOunt + 1;
                    if (!isTaskCanceled && completionCOunt == photoAndNameStrArrayList.size())
                        new SendCompleteGetRequest().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, strSignaturePackId);
                    else
                        uploadingImage();
//            Toast.makeText(getApplicationContext(), "Post : " + result, Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    private void uploadingImage() {
        if(!isTaskCanceled) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new SendImagePostRequest().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, strSignaturePackId, photoAndNameStrArrayList.get(completionCOunt).getPhotos());
            } else {
                new SendImagePostRequest().execute(strSignaturePackId, photoAndNameStrArrayList.get(completionCOunt).getPhotos());
            }
        }
    }

    int completionCOunt = 0;

    /*
    * Used for Send Image and signature completly uploaded request
    */
    public class SendCompleteGetRequest extends AsyncTask<String, String, String> {

        protected void onPreExecute() {

        }

        protected String doInBackground(String... arg0) {

            InputStream inputStream = null;
            try {

                HttpParams httpParameters = new BasicHttpParams();
                ConnManagerParams.setTimeout(httpParameters, 30000);
                HttpConnectionParams.setConnectionTimeout(httpParameters, 30000);
                HttpConnectionParams.setSoTimeout(httpParameters, 30000);
                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient(httpParameters);

                // 2. make POST request to the given URL
                HttpPatch httpPost = new HttpPatch(Constant.SERVER_URL + "package/" + strSignaturePackId + "/complete/");

                // 7. Set some headers to inform server about the type of the content
//                httpPost.setHeader("Content-Type", "application/json");
                httpPost.setHeader("Authorization", "Token " + userCredentials);
                httpPost.setHeader("User-Agent", getUserAgent());
//                httpPost.setHeader("Accept", "application/json");

                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPost);

                // 9. receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // 10. convert inputstream to string
                if (inputStream != null)
                    return convertInputStreamToString(inputStream);
                else
                    return getString(R.string.did_not_work);

            } catch (Exception e) {

                return getString(R.string.did_not_work);
                //   return e.toString();
            }
        }

        @Override
        protected void onPostExecute(String result) {
            if (result.equalsIgnoreCase(getString(R.string.did_not_work))) {
                if (progress != null && progress.isShowing())
                    progress.dismiss();
                ViewRetryDialog viewRetryDialog = new ViewRetryDialog();
                viewRetryDialog.showDialog(SignatureActivity.this);
            } else {
                if (progress != null && progress.isShowing())
                    progress.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.has("result")) {
                        finishOperation();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void finishOperation() {
        if (file.exists()) {
            if (file.delete()) {

                if (file.exists()) {
                    try {
                        file.getCanonicalFile().delete();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (file.exists()) {
                        getApplicationContext().deleteFile(file.getName());
                    }
                }

                callBroadCast(file.getAbsolutePath(), true);
            } else {

            }
        }
        isUploadingInProcess = false;
        DataLabels.destroy();
        Preference.destroy();
        if (progress != null && progress.isShowing())
            progress.dismiss();
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        finish();
    }

    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");
    OkHttpClient okHttpClient = new OkHttpClient();


    private String sendSignaturePostReq() {
        InputStream inputStream = null;
        String result = "";
        try {



            HttpParams httpParameters = new BasicHttpParams();
            ConnManagerParams.setTimeout(httpParameters, 30000);
            HttpConnectionParams.setConnectionTimeout(httpParameters, 30000);
            HttpConnectionParams.setSoTimeout(httpParameters, 30000);
            // 1. create HttpClient
            HttpClient httpclient = new DefaultHttpClient(httpParameters);

            // 2. make POST request to the given URL
            HttpPost httpPost = new HttpPost(Constant.SERVER_URL + "packages/?format=json");

            String json = "";

            // 3. build jsonObject
            JSONObject postDataParams = new JSONObject();
            postDataParams.put("contact", contactId);
            postDataParams.put("campaign", campaignId);
            postDataParams.put("recipient_name", name);
            postDataParams.put("recipient_email", email);
            postDataParams.put("recipient_phone", phone);
            postDataParams.put("recipient_permission", true);
            postDataParams.put("recipient_signature", signature);
            postDataParams.put("images", new JSONArray());

            // 4. convert JSONObject to JSON to String
            json = postDataParams.toString();

            // 5. set json to StringEntity
            StringEntity se = new StringEntity(json);

            // 6. set httpPost Entity
            httpPost.setEntity(se);

            // 7. Set some headers to inform server about the type of the content
            httpPost.setHeader("Content-Type", "application/json");
            httpPost.setHeader("User-Agent", getUserAgent());
            httpPost.setHeader("Authorization", "Token " + userCredentials);
            httpPost.setHeader("Accept", "application/json");

            // 8. Execute POST request to the given URL
            HttpResponse httpResponse = httpclient.execute(httpPost);

            // 9. receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();

            // 10. convert inputstream to string
            if (inputStream != null)
                return convertInputStreamToString(inputStream);
            else
                return getString(R.string.did_not_work);

        } catch (Exception e) {

            return getString(R.string.did_not_work);
            //   return e.toString();
        }
    }

    @Override
    public void onDismiss(Object o) {

    }

    @Override
    public void onItemClick(Object o, int position) {

    }

    public class MyMediaScanner implements MediaScannerConnection.MediaScannerConnectionClient {

        private MediaScannerConnection mSC;
        private File file;

        public MyMediaScanner(Context context, File file) {
            this.file = file;
            mSC = new MediaScannerConnection(context, this);
            mSC.connect();
        }

        @Override
        public void onMediaScannerConnected() {
            mSC.scanFile(file.getAbsolutePath(), null);
        }

        @Override
        public void onScanCompleted(String path, Uri uri) {
            mSC.disconnect();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        client.getBus().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        client.getBus().unregister(this);
    }

    @Subscribe
    public void onPhotoUploadFinished(PhotoUploadResponseEvent event) {

        if (event.isSuccess()) {
            if (!browsedUris.isEmpty()) {
                for (Uri uri : browsedUris) {
                    deleteImage(uri);
                }
            }
            if (file.exists()) {
                if (file.delete()) {

                    if (file.exists()) {
                        try {
                            file.getCanonicalFile().delete();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        if (file.exists()) {
                            getApplicationContext().deleteFile(file.getName());
                        }
                    }

                    callBroadCast(file.getAbsolutePath(), true);
                } else {

                }
            }

            DataLabels.destroy();
            Preference.destroy();
            if (progress != null && progress.isShowing())
                progress.dismiss();
            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
            finish();
//            ViewDialog alert = new ViewDialog();
//            alert.showDialog(SignatureActivity.this);
            //Toast.makeText(this, "Uploaded successfully", Toast.LENGTH_SHORT).show();
        } else {
            if (progress != null && progress.isShowing())
                progress.dismiss();
            ViewRetryDialog viewRetryDialog = new ViewRetryDialog();
            viewRetryDialog.showDialog(SignatureActivity.this);
        }
        //Toast.makeText(this, "Failed to upload", Toast.LENGTH_SHORT).show();

        if (event.getMessage() != null)
            if (progress != null && progress.isShowing())
                progress.dismiss();
        System.out.println(event.getMessage());//logText.setText(event.getMessage());
    }

    @Subscribe
    public void onNetworkRequestFailed(NetworkRequestFailedEvent event) {
        if (progress != null && progress.isShowing())
            progress.dismiss();
        if (event.getCause().getMessage().contains("timeout") || event.getCause().getMessage().contains("connect timed out")) {
            ViewRetryDialog viewRetryDialog = new ViewRetryDialog();
            viewRetryDialog.showDialog(SignatureActivity.this);
        } else {
            Intent intent = new Intent("my.action.string");
            intent.putExtra("userCredentials", userCredentials);
            sendBroadcast(intent);
            if (file.exists()) {
                if (file.delete()) {

                    if (file.exists()) {
                        try {
                            file.getCanonicalFile().delete();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        if (file.exists()) {
                            getApplicationContext().deleteFile(file.getName());
                        }
                    }

                    callBroadCast(file.getAbsolutePath(), true);
                } else {

                }
            }

            if (progress != null && progress.isShowing())
                progress.dismiss();
            Intent intentback = new Intent();
            setResult(RESULT_OK, intentback);
            finish();
        }
        // Toast.makeText(this, "Please make sure network is turned on", Toast.LENGTH_SHORT).show();
    }

    public void sharePermissionBack(View view) {


        finish();
    }


    public String imageStringFormat(String path) {

        Bitmap bitmapOrg = BitmapFactory.decodeFile(path);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmapOrg.compress(Bitmap.CompressFormat.JPEG, 10, stream);
        byte[] byteArray = stream.toByteArray();
        String imageString = Base64.encodeToString(byteArray, 0);

        return imageString;
    }

    public void deleteImage(Uri uri) {
        //String file_dj_path = Environment.getExternalStorageDirectory() + "/ECP_Screenshots/abc";
        File fdelete = new File(uri.getPath());
        if (fdelete.exists()) {
            if (fdelete.delete()) {

                if (fdelete.exists()) {
                    try {
                        fdelete.getCanonicalFile().delete();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (fdelete.exists()) {
                        getApplicationContext().deleteFile(fdelete.getName());
                    }
                }
                Log.e("-->", "file Deleted :" + uri.getPath());
                callBroadCast(uri.getPath(), true);
            } else {
                Log.e("-->", "file not Deleted :" + uri.getPath());
            }
        }
    }

    public void callBroadCast(String path, final boolean isDelete) {
        if (Build.VERSION.SDK_INT >= 14) {
            Log.e("-->", " >= 14");
            MediaScannerConnection.scanFile(this, new String[]{path}, null, new MediaScannerConnection.OnScanCompletedListener() {
                /*
                 *   (non-Javadoc)
                 * @see android.media.MediaScannerConnection.OnScanCompletedListener#onScanCompleted(java.lang.String, android.net.Uri)
                 */
                public void onScanCompleted(String path, Uri uri) {
                    if (isDelete) {
                        if (uri != null) {
                            getApplicationContext().getContentResolver().delete(uri,
                                    null, null);
                        }
                    }
                    Log.e("ExternalStorage", "Scanned " + path + ":");
                    Log.e("ExternalStorage", "-> uri=" + uri);
                }
            });
        } else {
            Log.e("-->", " < 14");
            sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED,
                    Uri.parse("file://" + Environment.getExternalStorageDirectory())));
        }
    }

    public boolean isOnline(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        //should check null because in airplane mode it will be null
        return (netInfo != null && netInfo.isConnected());

    }

}