package com.vboost.pro;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.soundcloud.android.crop.Crop;
import com.vboost.pro.Adapter.CampaignsDataAdapter;
import com.vboost.pro.Network.NetworkChangeReceiver;
import com.vboost.pro.Util.Constant;

import java.io.File;
import java.io.IOException;


/**
 * Created by Nabeel Hafeez on 8/12/2016.
 */
public class PhotoResultActivity extends AppCompatActivity {

    Button retakePhoto;
    Button keepPhoto;
    Button cropPhoto;
    Button retakePhotoEdit;
    Button keepPhotoEdit;
    Button cropPhotoEdit;
    Button deletePhotoEdit;
    ImageView showImage;
    TextView titleActivity;
    LinearLayout normalLayout;
    LinearLayout deleteLayout;


    String imageTitle;
    String imagePath;
    String imageName;
    String previousImage = null;
    int position;
    boolean editPhoto;

    int CROP_PIC = 2;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_photo_result);

        retakePhoto = (Button) findViewById(R.id.retake_button);
        keepPhoto = (Button) findViewById(R.id.keep_photo_button);
        cropPhoto = (Button) findViewById(R.id.crop_button);
        retakePhotoEdit = (Button) findViewById(R.id.retake_button_gone);
        keepPhotoEdit = (Button) findViewById(R.id.keep_photo_button_gone);
        cropPhotoEdit = (Button) findViewById(R.id.crop_button_gone);
        deletePhotoEdit = (Button) findViewById(R.id.delete_button_gone);
        showImage = (ImageView) findViewById(R.id.capture_image_show);
        titleActivity = (TextView) findViewById(R.id.title_photo);
        normalLayout = (LinearLayout) findViewById(R.id.normal_buttons_layout);
        deleteLayout = (LinearLayout) findViewById(R.id.delete_buttons_layout);

        retakePhoto.setAllCaps(false);
        keepPhoto.setAllCaps(false);
        cropPhoto.setAllCaps(false);
        retakePhotoEdit.setAllCaps(false);
        keepPhotoEdit.setAllCaps(false);
        cropPhotoEdit.setAllCaps(false);
        deletePhotoEdit.setAllCaps(false);

        retakePhoto.getBackground().setColorFilter(new LightingColorFilter(Color.parseColor("#8ec63e"),0x00000000));
        keepPhoto.getBackground().setColorFilter(new LightingColorFilter(Color.parseColor("#8ec63e"),0x00000000));
        cropPhoto.getBackground().setColorFilter(new LightingColorFilter(Color.parseColor("#8ec63e"),0x00000000));

        retakePhotoEdit.getBackground().setColorFilter(new LightingColorFilter(Color.parseColor("#8ec63e"),0x00000000));
        keepPhotoEdit.getBackground().setColorFilter(new LightingColorFilter(Color.parseColor("#8ec63e"),0x00000000));
        cropPhotoEdit.getBackground().setColorFilter(new LightingColorFilter(Color.parseColor("#8ec63e"),0x00000000));
        deletePhotoEdit.getBackground().setColorFilter(new LightingColorFilter(Color.parseColor("#8ec63e"),0x00000000));

        imageName = getIntent().getStringExtra("imagename");
        imagePath = getIntent().getStringExtra("imagepath");
        imageTitle = getIntent().getStringExtra("phototitle");
        position = getIntent().getIntExtra("Position",-1);
        editPhoto = getIntent().getBooleanExtra("editphoto",false);

        if (editPhoto) {

            normalLayout.setVisibility(View.GONE);
            deleteLayout.setVisibility(View.VISIBLE);

        } else {

            normalLayout.setVisibility(View.VISIBLE);
            deleteLayout.setVisibility(View.GONE);

        }

        titleActivity.setText(imageTitle);
        Uri myUriImagePath = Uri.parse(imagePath);

        Bitmap bitmapImage = null;
        try {
            bitmapImage = CampaignsDataAdapter.decodeFile(this, imagePath, 600);
            bitmapImage = NetworkChangeReceiver.rotatedBitmap(bitmapImage, myUriImagePath.getPath());

            showImage.setImageBitmap(bitmapImage);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void retakePhotoButton(View view) {

//        previousImage = imagePath;
//        Intent intent = new Intent(PhotoResultActivity.this, CameraActivity.class);
//        intent.putExtra("phototitle",imageTitle);
//        intent.putExtra("Position",position);
//        intent.putExtra("editphoto", editPhoto);
//        startActivityForResult(intent, 1002);

        String filename = imagePath.substring(imagePath.lastIndexOf("/") + 1);
        File file = new File(Environment.getExternalStorageDirectory() + "/" + Constant.FOLDER_NAME, filename);
        Uri imageUri;
        Constant.isParentFolderExist(new File(Environment.getExternalStorageDirectory() + "/" + Constant.FOLDER_NAME));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            imageUri = FileProvider.getUriForFile(PhotoResultActivity.this, getPackageName() + ".provider", file);
        } else {
            imageUri = Uri.fromFile(file);
        }

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
        cameraIntent.putExtra("phototitle",imageTitle);
        cameraIntent.putExtra("Position",position);
        cameraIntent.putExtra("editphoto", editPhoto);
        grantUriPermission(getPackageName(), imageUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(cameraIntent, 1002);
        }

    }

    public void keepPhotoButton(View view) {


        Intent intent = new Intent();
        intent.putExtra("imagePath",imagePath);
        intent.putExtra("Position",position);
        setResult(RESULT_OK,intent);
        finish();

    }

    public void cropPhotoButton(View view) {

        File sdCard = Environment.getExternalStorageDirectory();
        File dir = new File(sdCard.getAbsolutePath() + "/Vboost");
        if(!dir.exists()) {
            dir.mkdirs();
        }

        String fileName = String.format("%d", System.currentTimeMillis());
        previousImage = imagePath;
        if(imagePath.contains("file://")){
            String temp = imagePath.substring(7, imagePath.length());
            imagePath = temp;
        }

        File outFile = new File(imagePath);
        Uri source = Uri.fromFile(outFile);
//        Uri source = Uri.parse(imagePath);

        Uri destination = Uri.fromFile(new File(dir,fileName));
        Crop.of(source,destination).asSquare().start(this);
         //Crop.pickImage(this);
    }

    public void deletePhotoButton(View view) {

        deleteImage(Uri.parse(imagePath));
        imagePath = "";
        Intent intent = new Intent();
        intent.putExtra("imagePath",imagePath);
        intent.putExtra("Position",position);
        setResult(RESULT_OK,intent);
        finish();

    }

    @Override
    protected void onActivityResult(int requestCode,int resultCode,Intent result) {
        if (requestCode == Crop.REQUEST_PICK && resultCode == RESULT_OK) {
            beginCrop(result.getData());
        } else if (requestCode == Crop.REQUEST_CROP) {
            handleCrop(resultCode,result);
        } else if (requestCode == 1002 && resultCode == RESULT_OK) {

            if(result == null) {
                Uri myUriImagePath = Uri.parse(imagePath);

                Bitmap bitmapImage = null;
                bitmapImage = BitmapFactory.decodeFile(myUriImagePath.getEncodedPath());
                bitmapImage = NetworkChangeReceiver.rotatedBitmap(bitmapImage, myUriImagePath.getPath());

                showImage.setImageBitmap(bitmapImage);
            } else {
//            if(previousImage !=null){
//                deleteImage(Uri.parse(previousImage));
//            }
                int position = result.getExtras().getInt("Position");
                String imagePath = result.getExtras().getString("imagePath");
           /* Log.i("TAAAG", "2nd activity - onActivityResult - printing result");
            Log.i("TAAAG", position+"="+imagePath);*/
                Intent intent = new Intent();
                intent.putExtra("imagePath", imagePath);
                intent.putExtra("Position", position);
                setResult(RESULT_OK, intent);
                finish();
            }
        }
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }

    private void beginCrop(Uri source) {
        File sdCard = Environment.getExternalStorageDirectory();
        File dir = new File(sdCard.getAbsolutePath() + "/Vboost");
        if(!dir.exists()) {
            dir.mkdirs();
        }

        String fileName = String.format("%d", System.currentTimeMillis());

        Uri destination = Uri.fromFile(new File(dir,fileName));
        Crop.of(source,destination).asSquare().start(this);
    }

    private void handleCrop(int resultCode,Intent result) {
        if (resultCode == RESULT_OK) {

            Bitmap bitmapImage = null;
            bitmapImage = BitmapFactory.decodeFile(Crop.getOutput(result).getEncodedPath());
            bitmapImage = NetworkChangeReceiver.rotatedBitmap(bitmapImage, Crop.getOutput(result).getPath());
            showImage.setImageBitmap(bitmapImage);

//            showImage.setImageURI(Crop.getOutput(result));
            imagePath = Crop.getOutput(result).toString() ;
            deleteImage(Uri.parse(previousImage));

        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this,Crop.getError(result).getMessage(),Toast.LENGTH_SHORT).show();
        }
    }

    public void deleteImage(Uri uri) {
        //String file_dj_path = Environment.getExternalStorageDirectory() + "/ECP_Screenshots/abc";
        File fdelete = new File(uri.getPath());
        if (fdelete.exists()) {
            if (fdelete.delete()) {

                if(fdelete.exists()){
                    try {
                        fdelete.getCanonicalFile().delete();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if(fdelete.exists()){
                        getApplicationContext().deleteFile(fdelete.getName());
                    }
                }
                Log.e("-->", "file Deleted :" + uri.getPath());
                callBroadCast(uri.getPath(), true);
            } else {
                Log.e("-->", "file not Deleted :" + uri.getPath());
            }
        }
    }

    public void callBroadCast(String path, final boolean isDelete) {
        if (Build.VERSION.SDK_INT >= 14) {
            Log.e("-->", " >= 14");
            MediaScannerConnection.scanFile(PhotoResultActivity.this, new String[]{path}, null, new MediaScannerConnection.OnScanCompletedListener() {
                /*
                 *   (non-Javadoc)
                 * @see android.media.MediaScannerConnection.OnScanCompletedListener#onScanCompleted(java.lang.String, android.net.Uri)
                 */
                public void onScanCompleted(String path, Uri uri) {
                    if (isDelete) {
                        if (uri != null) {
                            getApplicationContext().getContentResolver().delete(uri,
                                    null, null);
                        }
                    }
                    Log.e("ExternalStorage", "Scanned " + path + ":");
                    Log.e("ExternalStorage", "-> uri=" + uri);
                }
            });
        } else {
            Log.e("-->", " < 14");
            getApplicationContext().sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED,
                    Uri.parse("file://" + Environment.getExternalStorageDirectory())));
        }
    }

}
