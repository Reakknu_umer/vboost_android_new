package com.vboost.pro;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bigkoo.alertview.AlertView;
import com.bigkoo.alertview.OnItemClickListener;
import com.google.gson.Gson;
import com.vboost.pro.Adapter.ManageContactsAdapter;
import com.vboost.pro.DBClasses.Campaign;
import com.vboost.pro.DBClasses.Company;
import com.vboost.pro.DBClasses.Contact;
import com.vboost.pro.DBClasses.User;
import com.vboost.pro.Util.Constant;
import com.vboost.pro.Util.SharedPreferencesHelper;
import com.vboost.pro.model.Campaigns;
import com.vboost.pro.model.ManageContacts;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Nabeel Hafeez on 7/27/2016.
 */
public class LoginActivity extends AppCompatActivity implements OnItemClickListener {

    JSONObject jsonObject;
    EditText usernameFeild;
    EditText passwordField;
    Button loginNextButton;

    AlertView mAlertViewExt;
    AlertView networkErrorAlert;

    String userName;
    String password;
    String error;

    private final String USER_AGENT = "Mozilla/5.0";
    private ProgressDialog progress;


    public static final String MyPREFERENCES = "MyPrefs";
    public static final String Username = "usernameKey";
    public static final String Password = "passwordKey";
    public static final String Token = "tokenKey";
    SharedPreferences sharedpreferences;
    boolean login;
    boolean judgeProspect;

    User user;

    // ---------------------------------------------------------------------------------------------
    String userCredentials;

    String[] contactsName;
    ArrayList<ManageContacts> manageContactsArray = new ArrayList<ManageContacts>();
    List<Contact> contactList;
    ArrayList<Campaigns> campaignsArray = new ArrayList<Campaigns>();
    List<Campaign> campaigns;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_login);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        userCredentials = sharedpreferences.getString(Token, null);
        login = sharedpreferences.getBoolean(Constant.Login, false);
        judgeProspect = sharedpreferences.getBoolean(Constant.JudgeProspect, false);

        usernameFeild = (EditText) findViewById(R.id.login_company_identifier_edit_text);
        passwordField = (EditText) findViewById(R.id.login_company_paswword_edit_text);
        loginNextButton = (Button) findViewById(R.id.login_next_button);
        loginNextButton.getBackground().setColorFilter(new LightingColorFilter(Color.parseColor("#FBA919"), 0x00000000));
        usernameFeild.setSingleLine(true);

        loginNextButton.setAllCaps(false);

        mAlertViewExt = new AlertView("Error", "We're sorry, company identifier and password are incorrect", "Try Again", null, new String[]{"Request Login"}, this, AlertView.Style.Alert, this);
        networkErrorAlert = new AlertView("Network Error", "Please ensure a strong internet\nconnectivity and try again", null, null, new String[]{"OK"}, this, AlertView.Style.Alert, this);

        userName = sharedpreferences.getString(Username, null);
        password = sharedpreferences.getString(Password, null);

        if (login) {
            if (isOnline(LoginActivity.this)) {
                Contact.destroy();
                Campaign.destroy();

                new LoginActivity.GetCompany(this).execute();

            } else {

                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                builder.setMessage(getString(R.string.no_internet_conn))
                        .setCancelable(false)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                                finish();
                            }
                        });
                AlertDialog alert11 = builder.create();
                alert11.show();

                //showDialog(LoginActivity.this);
                /*contactList  = Contact.getAll();
                for(int i = 0; i<contactList.size(); i++) {
                    Contact contact = contactList.get(i);
                    ManageContacts manageContacts = new ManageContacts(Integer.parseInt(contact.getCompany()), contact.getContact_id(), contact.getEmail(), contact.getIs_active(), contact.getName(), contact.getPhone(), contact.getPhoto(), contact.getTitle(), contact.getType());
                    manageContactsArray.add(manageContacts);
                }
                extractNameArray(manageContactsArray);

                // todo adapter
//            adapter = new ManageContactsAdapter(LoginActivity.this, manageContactsArray, userCredentials, contactsName, false);
//            listViewContacts.setAdapter(adapter);

                campaigns  = Campaign.getAll();
                for(int i = 0; i<campaigns.size(); i++) {
                    Campaign campaign = campaigns.get(i);
                    Campaigns cam = new Campaigns(campaign.getCampaign_Color(), campaign.getCampaign_Details(), campaign.getCampaignID(),campaign.getCompany(), campaign.getDefault_count(), campaign.getKey(), campaign.getLogo(), campaign.getMax_count(), campaign.getMin_count(), campaign.getName());
                    campaignsArray.add(cam);
                }

                Collections.sort(campaignsArray, new Comparator<Campaigns>() {
                    public int compare(Campaigns c1, Campaigns c2) {
                        return c1.getName().compareTo(c1.getName());
                    }
                });

                performLoginCheck();*/

            }
        }


    }

    public void showDialog(Activity activity) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.retry_alert_new);

        dialog.findViewById(R.id.a).setVisibility(View.GONE);
        Button dialogButton = (Button) dialog.findViewById(R.id.retry_button_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (isOnline(LoginActivity.this)) {
                    if (progress == null)
                        progress = new ProgressDialog(LoginActivity.this);
                    progress.setMessage("Please wait...");
//                        if (!isFinishing() && progress != null && !progress.isShowing())
//                            progress.show();
                    dialog.dismiss();
                    //  client.getBus().post(new PhotoUploadRequestEvent(photoAndNameArrayList, campaignId, contactId, name, email, 0, phone, ""));
                } else {
                }
            }
        });
        if(!isFinishing())
            dialog.show();
    }

    private void performLoginCheck() {
        if (userName != null && password != null) {

            usernameFeild.setText(userName);
            passwordField.setText(password);

            if (isOnline(LoginActivity.this)) {

                User.destroy();
                new loginVboost(this).execute();
            } else {
                user = User.findByUsername("'" + usernameFeild.getText().toString() + "'");
                if (user != null) {
                    if (user.getUsername().equals(usernameFeild.getText().toString()) && user.getPassword().equals(passwordField.getText().toString())) {

                        if (campaignsArray.size() == 1) {

                            Intent campaignsDataIntent = new Intent(LoginActivity.this, CampaignsDataActivity.class);
                            campaignsDataIntent.putExtra("contactsName", contactsName);
                            campaignsDataIntent.putExtra("campaignrow", campaignsArray.get(0));
                            campaignsDataIntent.putExtra("contacts", manageContactsArray);
                            campaignsDataIntent.putExtra("campaignArray", campaignsArray);
                            startActivity(campaignsDataIntent);

                        } else {
                            Intent contactIntent = new Intent(LoginActivity.this, CampaignsActivity.class);
                            contactIntent.putExtra("contactsName", contactsName);
                            contactIntent.putExtra("contacts", manageContactsArray);
                            contactIntent.putExtra("campaignArray", campaignsArray);
                            startActivity(contactIntent);
                            finish();
                        }

//                        Intent splashIntent = new Intent(LoginActivity.this, ManageContatcsAvtivity.class);
//                        startActivity(splashIntent);
//                        finish();
                    } else {
                        mAlertViewExt.show();
                    }
                } else {

                    networkErrorAlert.show();

                }

            }
        }
    }

    public void attemptLogin(View view) {

        userName = usernameFeild.getText().toString();
        password = passwordField.getText().toString();

        if (userName.isEmpty() || userName.equals(null) || userName == null) {

            new AlertView(null, "\nPlease enter the company identifier.", null, null, new String[]{"OK"}, this, AlertView.Style.Alert, this).show();

        } else if (password.isEmpty() || password.equals(null) || password == null) {

            new AlertView(null, "\nPlease enter the company password.", null, null, new String[]{"OK"}, this, AlertView.Style.Alert, this).show();


        } else {

            new loginVboost(this).execute();
        }
    }

    @Override
    public void onItemClick(Object o, int position) {

        if (o == mAlertViewExt && position != AlertView.CANCELPOSITION) {

            Intent requestIntent = new Intent(LoginActivity.this, RequestLoginActivity.class);
            startActivity(requestIntent);

            return;
        }

    }

    private class loginVboost extends AsyncTask<String, Void, Void> {

        private final Context context;

        public loginVboost(Context c) {
            this.context = c;
        }

        protected void onPreExecute() {
            progress = new ProgressDialog(this.context);
            progress.setMessage("Login Please wait...");
            progress.show();
        }

        @Override
        protected Void doInBackground(String... params) {


            try {

                URL url = new URL(Constant.SERVER_URL + "auth/login");

                HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                //add reuqest header
                connection.setRequestMethod("POST");
                connection.setRequestProperty("User-Agent", USER_AGENT);
                connection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

                String urlParameters = "username=" + userName + "&password=" + password;

                // Send post request
                connection.setDoOutput(true);
                DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
                wr.writeBytes(urlParameters);
                wr.flush();
                wr.close();

                final int responseCode = connection.getResponseCode();
                System.out.println("\nSending 'POST' request to URL : " + url);
                System.out.println("Post parameters : " + urlParameters);
                System.out.println("Response Code : " + responseCode);

                BufferedReader in = new BufferedReader(
                        new InputStreamReader(connection.getInputStream()));
                String inputLine;
                final StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                //print result
                System.out.println("Login Response:  " + response.toString());

                LoginActivity.this.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {

                        if (responseCode == 200) {
                            try {
                                String responseString = response.toString();
                                System.out.println(response.toString());
                                /**
                                 * Parse JSON response to Gson library
                                 */
                                jsonObject = new JSONObject(responseString);
                                String token = jsonObject.getString("auth_token");
                                Log.e("token", token);

                                SharedPreferences.Editor editor = sharedpreferences.edit();

                                // todo save username and pass
                                editor.putString(Username, userName);
                                editor.putString(Password, password);
                                editor.putString(Token, token);
                                editor.putBoolean(Constant.Login, true);
                                editor.commit();

                                User user = new User(token, password, userName, null, null, null, null);
                                user.saveToDb(LoginActivity.this);


                                progress.dismiss();

                                if (judgeProspect) {
                                    if (campaignsArray.size() == 1) {

                                        Intent campaignsDataIntent = new Intent(LoginActivity.this, CampaignsDataActivity.class);
                                        campaignsDataIntent.putExtra("contactsName", contactsName);
                                        campaignsDataIntent.putExtra("campaignrow", campaignsArray.get(0));
                                        campaignsDataIntent.putExtra("contacts", manageContactsArray);
                                        campaignsDataIntent.putExtra("campaignArray", campaignsArray);
                                        startActivity(campaignsDataIntent);

                                    } else {
                                        Intent contactIntent = new Intent(LoginActivity.this, CampaignsActivity.class);
                                        contactIntent.putExtra("contactsName", contactsName);
                                        contactIntent.putExtra("contacts", manageContactsArray);
                                        contactIntent.putExtra("campaignArray", campaignsArray);
                                        startActivity(contactIntent);
                                        finish();
                                    }
                                } else {
                                    Intent splashIntent = new Intent(LoginActivity.this, ManageContatcsAvtivity.class);
                                    startActivity(splashIntent);
                                    finish();
                                }

                            } catch (final JSONException e) {
                                e.printStackTrace();
                                progress.dismiss();
                            }
                        } else if (responseCode == 400) {

                            String responseString = response.toString();
                            System.out.println(response.toString());

                            try {
                                jsonObject = new JSONObject(responseString);
                                error = jsonObject.getString("non_field_errors");

                                Log.e("non_field_errors", error);


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progress.dismiss();

                                    mAlertViewExt.show();
                                }
                            });

                        }
                    }
                });

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                Log.e("Mal exc", "" + e);
                progress.dismiss();
                e.printStackTrace();
            } catch (final IOException e) {
                // TODO Auto-generated catch block
                Log.e("IO exc", "" + e.getMessage() + ":  " + e.getCause());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progress.dismiss();
                        if (e.getMessage().contains("Unable to resolve host")) {
                            networkErrorAlert.show();
                        } else {
                            mAlertViewExt.show();
                        }
                    }
                });
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute() {
            //progress.dismiss();
        }

    }

    public boolean isOnline(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        //should check null because in airplane mode it will be null
        return (netInfo != null && netInfo.isConnected());

    }

    // ---------------------------------------------------------------------------------------------

    public String[] extractNameArray(ArrayList<ManageContacts> contactsArray) {
        int i = contactsArray.size();
        int n = ++i;
        contactsName = new String[contactsArray.size()/*n*/];
        for (int c = 0; c < contactsArray.size(); c++) {
            contactsName[c] = contactsArray.get(c).getName();
        }
        return contactsName;
    }

    private class GetContacts extends AsyncTask<String, Void, Void> {

        private final Context context;

        public GetContacts(Context c) {
            this.context = c;
        }

        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(String... params) {
            try {

                URL url = new URL(Constant.SERVER_URL + "contacts/");

                HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                // userCredentials = "96ea502e088bc46dd3e863f1c82db3e160a4a56a";
                byte[] encodedBytes = Base64.encode(userCredentials.getBytes(), 0);
                String tokenAuth = "Token " + userCredentials;/*new String(encodedBytes);*/
                connection.setRequestProperty("Authorization", tokenAuth);
                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                connection.setRequestProperty("Content-Language", "en-US");
                connection.setUseCaches(false);
                connection.setDoInput(true);

                connection.setRequestMethod("GET");
                connection.setRequestProperty("USER-AGENT", "Mozilla/5.0");


                int responseCode = connection.getResponseCode();

                System.out.println("\nSending 'POST' request to URL : " + url);
                System.out.println("Post parameters : " + tokenAuth);
                System.out.println("Response Code : " + responseCode);

                final StringBuilder output = new StringBuilder("Request URL " + url);
                //output.append(System.getProperty("line.separator") + "Request Parameters " + urlParameters);
                output.append(System.getProperty("line.separator") + "Response Code " + responseCode);
                output.append(System.getProperty("line.separator") + "Type " + "GET");
                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                String line = "";

                StringBuilder responseOutput = new StringBuilder();
                System.out.println("output===============" + br);
                while ((line = br.readLine()) != null) {
                    //responseOutput.append(line);

                    JSONArray jsonArray = new JSONArray(line);
                    System.out.println(line);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                        ManageContacts manageContacts = null;
                        try {
                            manageContacts = new ManageContacts(jsonObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        manageContactsArray.add(manageContacts);
                        responseOutput.append(jsonObject.getString("name")).append("  /  ");
                    }

                }
                br.close();

                output.append(System.getProperty("line.separator") + "Response " + System.getProperty("line.separator") + System.getProperty("line.separator") + responseOutput.toString());

                LoginActivity.this.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {

                        for (int i = 0; i < manageContactsArray.size(); i++) {

                            ManageContacts mc = manageContactsArray.get(i);
                            Contact contact = new Contact(String.valueOf(mc.getCompany()), mc.getId(), mc.getEmail(), mc.getIs_active(), mc.getName(), mc.getPhone(), mc.getPhoto(), null, mc.getTitle(), mc.getType(), null);
                            contact.saveToDb();
                        }
                        extractNameArray(manageContactsArray);
                        // todo adapter
//                        adapter = new ManageContactsAdapter(LoginActivity.this, manageContactsArray, userCredentials, contactsName, true);
//                        listViewContacts.setAdapter(adapter);

                        progress.dismiss();

                        performLoginCheck();

                    }
                });


            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute() {
            //progress.dismiss();
        }

    }

    private class GetCampaigns extends AsyncTask<String, Void, Void> {

        private final Context context;

        public GetCampaigns(Context c) {
            this.context = c;
        }

        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(String... params) {
            try {

                URL url = new URL(Constant.SERVER_URL + "campaigns/");

                HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                //String userCredentials = "96ea502e088bc46dd3e863f1c82db3e160a4a56a";
                byte[] encodedBytes = Base64.encode(userCredentials.getBytes(), 0);
                String tokenAuth = "Token " + userCredentials;/*new String(encodedBytes);*/
                connection.setRequestProperty("Authorization", tokenAuth);
                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                connection.setRequestProperty("Content-Language", "en-US");
                connection.setUseCaches(false);
                connection.setDoInput(true);

                connection.setRequestMethod("GET");
                connection.setRequestProperty("USER-AGENT", "Mozilla/5.0");


                int responseCode = connection.getResponseCode();

                System.out.println("\nSending 'POST' request to URL : " + url);
                System.out.println("Post parameters : " + tokenAuth);
                System.out.println("Response Code : " + responseCode);

                final StringBuilder output = new StringBuilder("Request URL " + url);
                //output.append(System.getProperty("line.separator") + "Request Parameters " + urlParameters);
                output.append(System.getProperty("line.separator") + "Response Code " + responseCode);
                output.append(System.getProperty("line.separator") + "Type " + "GET");
                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                String line = "";

                StringBuilder responseOutput = new StringBuilder();
                System.out.println("output===============" + br);
                while ((line = br.readLine()) != null) {
                    //responseOutput.append(line);

                    JSONArray jsonArray = new JSONArray(line);
                    System.out.println(line);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                        Campaigns campaigns = null;
                        try {
                            campaigns = new Campaigns(jsonObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        campaignsArray.add(campaigns);
                        if (campaignsArray.size() > 0) {
                            Collections.sort(campaignsArray, new Comparator<Campaigns>() {
                                @Override
                                public int compare(final Campaigns object1, final Campaigns object2) {
                                    return object1.getName().compareTo(object2.getName());
                                }
                            });
                        }

                        if (campaignsArray.size() == 1) {
                            for (int c = 0; c < campaignsArray.size(); c++) {
                                Campaigns singleRow = campaignsArray.get(c);
                                Campaign campaign = new Campaign(singleRow.getColor(), singleRow.getDetails(), singleRow.getId(), singleRow.getCompany(), singleRow.getImage().getDefault_count(), singleRow.getKey(), singleRow.getImage(), singleRow.getLogo(), singleRow.getImage().getMax_count(), singleRow.getImage().getMin_count(), singleRow.getName(), null, null, null);
                                campaign.saveToDb();
                            }

                        }
                        responseOutput.append(jsonObject.getString("name")).append("  /  ");
                    }
                    SharedPreferencesHelper.getInstance(LoginActivity.this).setString("data", new Gson().toJson(campaignsArray));

                }
                br.close();

                output.append(System.getProperty("line.separator") + "Response " + System.getProperty("line.separator") + System.getProperty("line.separator") + responseOutput.toString());

                new LoginActivity.GetContacts(LoginActivity.this).execute();
            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute() {
            //progress.dismiss();
        }

    }

    private class GetCompany extends AsyncTask<String, Void, Void> {

        private final Context context;

        public GetCompany(Context c) {
            this.context = c;
        }

        protected void onPreExecute() {
            progress = new ProgressDialog(this.context);
            progress.setMessage("Loading");
            progress.show();
        }

        @Override
        protected Void doInBackground(String... params) {
            try {

                URL url = new URL(Constant.SERVER_URL + "company/");

                HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                //String userCredentials = "96ea502e088bc46dd3e863f1c82db3e160a4a56a";
                byte[] encodedBytes = Base64.encode(userCredentials.getBytes(), 0);
                String tokenAuth = "Token " + userCredentials;/*new String(encodedBytes);*/
                connection.setRequestProperty("Authorization", tokenAuth);
                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                connection.setRequestProperty("Content-Language", "en-US");
                connection.setUseCaches(false);
                connection.setDoInput(true);

                connection.setRequestMethod("GET");
                connection.setRequestProperty("USER-AGENT", "Mozilla/5.0");


                int responseCode = connection.getResponseCode();

                System.out.println("\nSending 'POST' request to URL : " + url);
                System.out.println("Post parameters : " + tokenAuth);
                System.out.println("Response Code : " + responseCode);

                BufferedReader in = new BufferedReader(
                        new InputStreamReader(connection.getInputStream()));
                String inputLine;
                final StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                //print result
                System.out.println("Login Response:  " + response.toString());


                try {
                    String responseString = response.toString();
                    System.out.println(response.toString());
                    /**
                     * Parse JSON response to Gson library
                     */
                    JSONObject jsonObject = new JSONObject(responseString);
                    int id = jsonObject.getInt("id");
                    String key = jsonObject.getString("key");
                    String name = jsonObject.getString("name");
                    String logo = jsonObject.getString("logo");
                    String status = jsonObject.getString("status");
                    String terms = jsonObject.getString("terms");
                    Log.e("Company Id", "" + id);
                    Log.e("Company Key", key);
                    Log.e("Company Name", name);
                    Log.e("Company Logo", logo);
                    Log.e("Company Status", status);
                    Log.e("Company Term", terms);

                                /*SharedPreferences.Editor editor = sharedpreferences.edit();
                                editor.putInt(Id, id);
                                editor.putString(Key, key);
                                editor.putString(Name, name);
                                editor.putString(Logo, logo);
                                editor.putString(Status, status);
                                editor.putString(Terms, terms);
                                editor.commit();*/

                    Company company = new Company(id, null, key, logo, name, status, terms, null);
                    company.saveToDb();

                } catch (final JSONException e) {
                    e.printStackTrace();
                }

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            new LoginActivity.GetCampaigns(LoginActivity.this).execute();
        }

    }
}
