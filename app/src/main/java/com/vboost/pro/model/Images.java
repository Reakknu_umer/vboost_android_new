package com.vboost.pro.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Nabeel Hafeez on 8/9/2016.
 */
public class Images implements Serializable {

    private int min_count;
    private int max_count;
    private int default_count;
    private ArrayList<Labels> labels;

    public int getMin_count() {
        return min_count;
    }

    public void setMin_count(int min_count) {
        this.min_count = min_count;
    }

    public int getMax_count() {
        return max_count;
    }

    public void setMax_count(int max_count) {
        this.max_count = max_count;
    }

    public int getDefault_count() {
        return default_count;
    }

    public void setDefault_count(int default_count) {
        this.default_count = default_count;
    }

    public ArrayList<Labels> getLabels() {
        return labels;
    }

    public void setLabels(ArrayList<Labels> labels) {
        this.labels = labels;
    }

    public Images(int _min_count, int _max_count, int _default_count, ArrayList<Labels> _labels){

        this.setMin_count(_min_count);
        this.setMax_count(_max_count);
        this.setDefault_count(_default_count);
        this.setLabels(_labels);
    }

    public Images(JSONObject dict) throws JSONException {
        this.setWithResponseDict(dict);
    }



    public Images setWithResponseDict(JSONObject dict) throws JSONException {

        if (dict.has("min_count")) {
            this.min_count = dict.getInt("min_count");
            if (dict.isNull("min_count")) {
                this.min_count = 0;
            }
        }
        if (dict.has("max_count")) {
            this.max_count = dict.getInt("max_count");
            if (dict.isNull("max_count")) {
                this.max_count = 0;
            }
        }

        if (dict.has("default_count")) {
            this.default_count = dict.getInt("default_count");
            if (dict.isNull("default_count")) {
                this.default_count = 0;
            }
        }

        if (dict.has("labels")) {
            labels = new ArrayList<>();
            for(int i = 0; i<dict.getJSONArray("labels").length(); i++){
                JSONObject labalDict = (JSONObject) dict.getJSONArray("labels").get(i);
                Labels label = new Labels(labalDict);
                labels.add(label);
            }
        }

        if(this.default_count > dict.getJSONArray("labels").length()){
            int newLen = this.default_count - dict.getJSONArray("labels").length();
            for (int i=0, k=8; i<newLen; i++, k++){
                JSONObject labalDict = new JSONObject();
                labalDict.put("name", "image"+k);
                labalDict.put("title", "Image #"+k);
                Labels label = new Labels(labalDict);
                labels.add(label);
            }
        }

        return this;
    }

}
