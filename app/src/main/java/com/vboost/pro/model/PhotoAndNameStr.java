package com.vboost.pro.model;

public class PhotoAndNameStr {
    private String photoStr;
    private String name;


    public String getPhotos() {
        return photoStr;
    }

    public void setPhotos(String photoStr) {
        this.photoStr = photoStr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
