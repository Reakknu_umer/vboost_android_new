package com.vboost.pro.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Nabeel Hafeez on 8/9/2016.
 */
public class Labels implements Serializable {

    private String name;
    private String title;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Labels(JSONObject dict) throws JSONException {
        this.setWithResponseDict(dict);
    }

    public Labels setWithResponseDict(JSONObject dict) throws JSONException {



        if (dict.has("name")) {
            this.name = dict.getString("name");
            if (dict.isNull("name")) {
                this.name = "";
            }
        }
        if (dict.has("title")) {
            this.title = dict.getString("title");
            if (dict.isNull("title")) {
                this.title = "";
            }
        }

        return this;
    }

}
