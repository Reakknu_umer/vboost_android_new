package com.vboost.pro;

/**
 * @author Jose Davis Nidhin
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.KeyEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bigkoo.alertview.AlertView;
import com.bigkoo.alertview.OnDismissListener;
import com.bigkoo.alertview.OnItemClickListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;


public class CameraActivity extends Activity implements OnItemClickListener, OnDismissListener {

    private static final String TAG = "CamTestActivity";

    private static int RESULT_LOAD_IMAGE_GALLERY = 1;
    String imageDecodableString;
    String title;
    int position;
    boolean editPhoto;

    Preview preview;
    ImageButton takePhoto;
    TextView titleCameraPhoto;
    Camera camera;
    Activity act;
    Context ctx;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ctx = this;
        act = this;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_camera);

        titleCameraPhoto  = (TextView) findViewById(R.id.title_camera_photo);
        title = getIntent().getStringExtra("phototitle");
        position = getIntent().getIntExtra("Position", -1);
        editPhoto = getIntent().getBooleanExtra("editphoto", false);
        titleCameraPhoto.setText(title);


        preview = new Preview(this, (SurfaceView) findViewById(R.id.surfaceView));
        preview.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        ((FrameLayout) findViewById(R.id.layout)).addView(preview);
        preview.setKeepScreenOn(true);

//		preview.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View arg0) {
//				camera.takePicture(shutterCallback, rawCallback, jpegCallback);
//			}
//		});


        takePhoto = (ImageButton) findViewById(R.id.btnCapture);

        takePhoto.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                //				preview.camera.takePicture(shutterCallback, rawCallback, jpegCallback);
                camera.takePicture(shutterCallback, rawCallback, jpegCallback);
            }
        });

        takePhoto.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View arg0) {
                camera.autoFocus(new Camera.AutoFocusCallback() {
                    @Override
                    public void onAutoFocus(boolean arg0, Camera arg1) {
                        //camera.takePicture(shutterCallback, rawCallback, jpegCallback);
                    }
                });
                return true;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        int numCams = Camera.getNumberOfCameras();
        if (numCams > 0) {
            try {
                camera = Camera.open(0);
                camera.startPreview();
                preview.setCamera(camera);
            } catch (RuntimeException ex) {
                Toast.makeText(ctx, getString(R.string.camera_not_found), Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onPause() {
        if (camera != null) {
            camera.stopPreview();
            preview.setCamera(null);
            camera.release();
            camera = null;
        }
        super.onPause();
    }

    private void resetCam() {
        camera.startPreview();
        preview.setCamera(camera);
    }

    private void refreshGallery(File file) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(Uri.fromFile(file));
        sendBroadcast(mediaScanIntent);
    }

    ShutterCallback shutterCallback = new ShutterCallback() {
        public void onShutter() {
            //			 Log.d(TAG, "onShutter'd");
        }
    };

    PictureCallback rawCallback = new PictureCallback() {
        public void onPictureTaken(byte[] data, Camera camera) {
            //			 Log.d(TAG, "onPictureTaken - raw");
        }
    };

    PictureCallback jpegCallback = new PictureCallback() {
        public void onPictureTaken(byte[] data, Camera camera) {
            new SaveImageTask().execute(data);
            resetCam();
            Log.d(TAG, "onPictureTaken - jpeg");
        }
    };

    private class SaveImageTask extends AsyncTask<byte[], Void, Void> {

        @Override
        protected Void doInBackground(byte[]... data) {
            FileOutputStream outStream = null;

            // Write to SD Card
            try {
                File sdCard = Environment.getExternalStorageDirectory();
                File dir = new File(sdCard.getAbsolutePath() + "/Vboost");
                dir.mkdirs();

                String fileName = String.format("%d", System.currentTimeMillis());
                File outFile = new File(dir, fileName);

                outStream = new FileOutputStream(outFile);
                outStream.write(data[0]);
                outStream.flush();
                outStream.close();

                Log.d(TAG, "onPictureTaken - wrote bytes: " + data.length + " to " + outFile.toURI());

                refreshGallery(outFile);


               Uri uriImagePath =  Uri.fromFile(outFile);
                Intent cameraIntent = new Intent(CameraActivity.this, PhotoResultActivity.class);
                cameraIntent.putExtra("imagepath", uriImagePath.toString());
                cameraIntent.putExtra("imagename", fileName);
                cameraIntent.putExtra("phototitle", title);
                cameraIntent.putExtra("Position", position);
                cameraIntent.putExtra("editphoto", editPhoto);
               // startActivity(cameraIntent);

                startActivityForResult(cameraIntent, 1009);

//                Intent intent2 = new Intent();
//                intent2.putExtra("Position", position);
//
//
//                setResult(RESULT_OK, intent2);

                //finish();


            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
            }
            return null;
        }


    }

    public void cameraMoreOptions(View view){

        alertMoreOptions();
    }

    public void alertMoreOptions() {
        new AlertView(null, null, null, null,
                new String[]{"Choose from Gallery",
                        "Back to Home Screen",
                },
                this, AlertView.Style.Alert, this).setCancelable(true).setOnDismissListener(this).show();
    }

    @Override
    public void onItemClick(Object o, int position) {

        if (position == 0) {

            loadImagefromGallery();

        } else if (position == 1) {

           finish();

        }


    }

    @Override
    public void onDismiss(Object o) {


    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
//            if(mAlertView!=null && mAlertView.isShowing()){
//                mAlertView.dismiss();
//                return false;
//            }
        }

        return super.onKeyDown(keyCode, event);

    }

    public void loadImagefromGallery() {
        // Create intent to Open Image applications like Gallery, Google Photos
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Start the Intent
        startActivityForResult(galleryIntent, RESULT_LOAD_IMAGE_GALLERY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            // When an Image is picked

            if (requestCode == 1009 && resultCode == RESULT_OK
                    && null != data) {

                int position = data.getExtras().getInt("Position");
                String imagePath = data.getExtras().getString("imagePath");

               /* Log.i("TAAAG", "2nd activity - onActivityResult - printing result");
                Log.i("TAAAG", position+"="+imagePath);*/
                Intent intent = new Intent();
                intent.putExtra("imagePath", imagePath);
                intent.putExtra("Position", position);
                setResult(RESULT_OK, intent);
                finish();
            }else if (requestCode == RESULT_LOAD_IMAGE_GALLERY && resultCode == RESULT_OK
                    && null != data) {
                // Get the Image from data

                Uri selectedImage = data.getData();
                String[] filePathColumn = { MediaStore.Images.Media.DATA };

                // Get the cursor
                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                // Move to first row
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);

                imageDecodableString = cursor.getString(columnIndex);
                cursor.close();

                Intent galleryIntent = new Intent(CameraActivity.this, PhotoResultActivity.class);
                galleryIntent.putExtra("imagepath", selectedImage.toString());
                galleryIntent.putExtra("imagename", "");
                galleryIntent.putExtra("phototitle", title);
                galleryIntent.putExtra("Position", position);
                // startActivity(cameraIntent);

                startActivityForResult(galleryIntent, 1009);


            } else {
                Toast.makeText(this, "You haven't picked Image",
                        Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                    .show();
        }

    }
}


