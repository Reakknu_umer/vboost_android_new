package com.vboost.pro;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;

import com.vboost.pro.Util.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Nabeel Hafeez on 8/22/2016.
 */
public class GetHelpActivity extends AppCompatActivity {

   // private TextView textView;
   WebView webView;
    private ProgressDialog progress;
    Spanned helpText;
    String htmlText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_get_help);


        webView = (WebView) findViewById(R.id.help_text_view);
        //textView = (TextView) findViewById(R.id.help_text_view);
        new GetHelp(this).execute();




    }

    public void infoClose(View view){

        finish();
    }

    private class GetHelp extends AsyncTask<String, Void, Void> {

        private final Context context;

        public GetHelp(Context c) {
            this.context = c;
        }

        protected void onPreExecute() {
            progress = new ProgressDialog(this.context);
            progress.setMessage("Loading");
            progress.show();
        }

        @Override
        protected Void doInBackground(String... params) {
            try {

                URL url = new URL(Constant.SERVER_URL+"help/");
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                connection.setRequestProperty("Content-Language", "en-US");
                connection.setUseCaches(false);
                connection.setDoInput(true);
                connection.setRequestMethod("GET");
                connection.setRequestProperty("USER-AGENT", "Mozilla/5.0");

                int responseCode = connection.getResponseCode();

                final StringBuilder output = new StringBuilder("Request URL " + url);
                //output.append(System.getProperty("line.separator") + "Request Parameters " + urlParameters);
                output.append(System.getProperty("line.separator") + "Response Code " + responseCode);
                output.append(System.getProperty("line.separator") + "Type " + "GET");
                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                String line = "";

                StringBuilder responseOutput = new StringBuilder();
                System.out.println("output===============" + br);
                while ((line = br.readLine()) != null) {
                    responseOutput.append(line);

                   // JSONArray jsonArray = new JSONArray(line);
                    System.out.println(line);

                        JSONObject jsonObject = new JSONObject(line);
                       helpText = Html.fromHtml(jsonObject.getString("help_text"));
                    htmlText = jsonObject.getString("help_text");
                        Log.e("Json Object", ""+helpText);



                }
                br.close();

                output.append(System.getProperty("line.separator") + "Response " + System.getProperty("line.separator") + System.getProperty("line.separator") + responseOutput.toString());

                GetHelpActivity.this.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {

                        webView.loadDataWithBaseURL(null, htmlText, "text/html", "utf-8", null);

                        //textView.setText(helpText);
                        progress.dismiss();

                    }
                });


            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute() {
            //progress.dismiss();
        }

    }
}
