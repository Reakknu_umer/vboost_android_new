package com.vboost.pro;

import android.app.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import android.os.CountDownTimer;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;

import android.text.Html;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;
import com.vboost.pro.Adapter.CampaignsDataAdapter;
import com.vboost.pro.DBClasses.Company;
import com.vboost.pro.DBClasses.DataLabels;
import com.vboost.pro.DBClasses.Preference;
import com.vboost.pro.Network.HttpPatch;
import com.vboost.pro.Network.NetworkChangeReceiver;
import com.vboost.pro.Util.Constant;
import com.vboost.pro.event.NetworkRequestFailedEvent;
import com.vboost.pro.event.PhotoUploadRequestEvent;
import com.vboost.pro.event.PhotoUploadResponseEvent;
import com.vboost.pro.model.Campaigns;
import com.vboost.pro.model.ManageContacts;
import com.vboost.pro.model.PhotoAndName;
import com.vboost.pro.model.PhotoAndNameStr;
import com.vboost.pro.service.ApiClient;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.ConnectionPoolTimeoutException;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * Created by Nabeel Hafeez on 8/12/2016.
 */
public class SharePermissionActivity extends AppCompatActivity {

    ArrayList<ManageContacts> contactsList;
    ArrayList<Campaigns> campaignsArray;
    Campaigns campaign;
    String[] contactsName;
    boolean singleCampaign;
    Button givesPermission;
    Button notGivesPermission;
    String signature;
    //String[] imageArray;
    String name;
    String deliveryMethod;
    String flag;
    int campaignId;
    int contactId;
    String email;
    String phone;

    List<DataLabels> imagesAndLabels;
    //List<byte[]> fileList = new ArrayList<>();
    //ArrayList<String> imagesTitle = new ArrayList<>();

    ArrayList<PhotoAndName> photoAndNameArrayList = new ArrayList<>();
    ArrayList<PhotoAndNameStr> photoAndNameStrArrayList = new ArrayList<>();

    private ProgressDialog progress;
    public static final String MyPREFERENCES = "MyPrefs";
    public static final String Token = "tokenKey";
    SharedPreferences sharedpreferences;
    ImageView companyLogo;

    String userCredentials;

    ApiClient client;
    ArrayList<Uri> browsedUris = new ArrayList<>();
    List<Company> company;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_share_permission);

        givesPermission = (Button) findViewById(R.id.gives_permission_button);
        notGivesPermission = (Button) findViewById(R.id.not_gives_permission_button);
        givesPermission.setAllCaps(false);
        notGivesPermission.setAllCaps(false);
        progress = new ProgressDialog(this);
        progress.setCancelable(false);
        progress.setMessage("Please wait...");
        companyLogo = (ImageView) findViewById(R.id.logo2);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        userCredentials = sharedpreferences.getString(Token, null);
        client = ApiClient.getInstance(getApplicationContext());
        givesPermission.getBackground().setColorFilter(new LightingColorFilter(Color.parseColor("#8ec63e"), 0x00000000));
        notGivesPermission.getBackground().setColorFilter(new LightingColorFilter(Color.parseColor("#8ec63e"), 0x00000000));
        name = getIntent().getStringExtra("name");
        deliveryMethod = getIntent().getStringExtra("deliverMethod");
        campaignId = getIntent().getIntExtra("campaignID", -1);
        contactId = getIntent().getIntExtra("contactID", -1);
        flag = getIntent().getStringExtra("deliveryFlag");
        contactsName = getIntent().getStringArrayExtra("contactsName");
        contactsList = (ArrayList<ManageContacts>) getIntent().getSerializableExtra("contacts");
        campaign = (Campaigns) getIntent().getSerializableExtra("campaignrow");
        campaignsArray = (ArrayList<Campaigns>) getIntent().getSerializableExtra("campaignArray");
        singleCampaign = getIntent().getBooleanExtra("singleCampaign", false);
        if (flag.equals("email")) {
            email = deliveryMethod;
            phone = null;
        } else {
            phone = deliveryMethod;
            email = null;
        }

        givesPermission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                yesPermission();
            }
        });

        notGivesPermission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progress.setMessage("Please wait...");
                progress.show();
                noPermission();
            }
        });


        new android.os.AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {

                company = Company.getAll();
                imagesAndLabels = DataLabels.getAll();
                createByteArraylist(imagesAndLabels);
                return null;
            }

            @Override
            protected void onPreExecute() {
                progress.setMessage("Fetching data...");
                progress.show();
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                if (progress != null && progress.isShowing())
                    progress.dismiss();
                if (company != null && company.size() > 0) {
                    Picasso.with(getApplicationContext())
                            .load(company.get(0).getLogo())
                            .placeholder(R.drawable.logo2)
                            .error(R.drawable.logo2)
                            .into(companyLogo);
                }
                super.onPostExecute(aVoid);
            }
        }.execute();













/*
        for (int i = 0; i < imagesAndLabels.size(); i++) {
            DataLabels dataLabels = imagesAndLabels.get(i);
            Uri myUriImagePath = Uri.parse(dataLabels.getImage());
            imagesTitle.add(dataLabels.getTitle());
            browsedUris.add(myUriImagePath);
        }

        try {
            for (Uri uri : browsedUris) {
                InputStream imageStream = null;
                try {
                    imageStream = getContentResolver().openInputStream(uri);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                Bitmap compressBitmap = BitmapFactory.decodeStream(imageStream);

                ByteArrayOutputStream outStream = new ByteArrayOutputStream();
                compressBitmap.compress(Bitmap.CompressFormat.JPEG, 20, outStream);
                fileList.add(outStream.toByteArray());
                //fileList.add(IOUtils.toByteArray(this.getContentResolver().openInputStream(uri)));
            }
        } catch (Exception e) {
            throw new IllegalStateException("Could not read file", e);
        }*/


    }

    private void createByteArraylist(List<DataLabels> imagesAndLabels) {
        //        Toast.makeText(getApplicationContext(), "First Place", Toast.LENGTH_SHORT);
        for (DataLabels dataLabels : imagesAndLabels) {

//            Uri myUriImagePath = Uri.parse(dataLabels.getImage());
//            browsedUris.add(myUriImagePath);
            String imageTitle = dataLabels.getTitle();
//            try {
//            InputStream imageStream = null;
//            try {
//                imageStream = getContentResolver().openInputStream(myUriImagePath);
//            } catch (FileNotFoundException e) {
//                e.printStackTrace();
//            }
//
//            Bitmap compressBitmap = BitmapFactory.decodeStream(imageStream);


//            Bitmap bitmap = BitmapFactory.decodeFile(dataLabels.getImage());
            try {
                //Bitmap bitmap = CampaignsDataAdapter.decodeFile(this, dataLabels.getImage(), 600);


                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                Bitmap bitmap = BitmapFactory.decodeFile(dataLabels.getImage(), options);
                bitmap = NetworkChangeReceiver.rotatedBitmap(bitmap, dataLabels.getImage());

                ByteArrayOutputStream outStream = new ByteArrayOutputStream();
//            compressBitmap.compress(Bitmap.CompressFormat.JPEG, 10, outStream);

                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
//                PhotoAndName photoAndName = new PhotoAndName();
//                photoAndName.setPhotos(outStream.toByteArray());
//                photoAndName.setName(imageTitle);
                String imageString = Base64.encodeToString(outStream.toByteArray(), Base64.DEFAULT);
//                photoAndNameArrayList.add(photoAndName);

                PhotoAndNameStr str = new PhotoAndNameStr();
                str.setName(imageTitle);
                str.setPhotos(imageString);
                photoAndNameStrArrayList.add(str);
            } catch (Exception e) {
                e.printStackTrace();
            }
//            } catch (Exception e) {
//                throw new IllegalStateException("Could not read file", e);
//            }

        }
    }

    public void yesPermission() {

        Intent yesIntent = new Intent(SharePermissionActivity.this, SignatureActivity.class);
        yesIntent.putExtra("name", name);
        yesIntent.putExtra("deliverMethod", deliveryMethod);
        yesIntent.putExtra("campaignID", campaignId);
        yesIntent.putExtra("contactID", contactId);
        yesIntent.putExtra("deliveryFlag", flag);
        // startActivity(yesIntent);
        startActivityForResult(yesIntent, 1);
    }


    public void noPermission() {

        /*Preference preference = new Preference(campaignId, contactId, email, name, "0", phone, null, null, null, null, null, null);
        preference.saveToDb();


        if (isOnline(this)) {

            client.getBus().post(new PhotoUploadRequestEvent(photoAndNameArrayList, campaignId, contactId, name, email, 0, phone, ""));

        } else {

            Intent intent = new Intent("my.action.string");
            intent.putExtra("userCredentials", userCredentials);
            sendBroadcast(intent);
            progress.dismiss();
//            ViewDialog alert = new ViewDialog();
//            alert.showDialog(SharePermissionActivity.this);
            ViewRetryDialog viewRetryDialog = new ViewRetryDialog();
            viewRetryDialog.showDialog(SharePermissionActivity.this);

        }*/

        countDownTimer = new CountDownTimer(45000, 1000) {

            public void onTick(long millisUntilFinished) {
                boolean isConnected = Constant.isConnected();
                Log.e("TAG", "Is Connected : " + isConnected + " with sec : " + millisUntilFinished);
                if (isConnected) {
                    if (countDownTimer != null) {
                        countDownTimer.cancel();
                    }
                    new SendPostRequest().execute();
                }
            }

            public void onFinish() {
                if (!Constant.isConnected()) {
                    progress.dismiss();
                    SharePermissionActivity.ViewRetryDialog viewRetryDialog = new ViewRetryDialog();
                    viewRetryDialog.showDialog(SharePermissionActivity.this);
                }
            }

        }.start();
        
        //start for minute
        countDownTimerEnd = new CountDownTimer(60000, 1000) {

            public void onTick(long millisUntilFinished) {
                
            }

            public void onFinish() {
                if (isUploadingInProcess) {
                    if(progress != null && progress.isShowing())
                        progress.dismiss();
                    isTaskCanceled = true;
                    SharePermissionActivity.ViewRetryDialog viewRetryDialog = new ViewRetryDialog();
                    viewRetryDialog.showDialog(SharePermissionActivity.this);
                }
            }

        }.start();
        
//        new SendPostRequest().execute();
    }

    CountDownTimer countDownTimer = null;
    boolean isUploadingInProcess = false;
    boolean isTaskCanceled = false;
    CountDownTimer countDownTimerEnd = null;

    String strSignaturePackId = "";

    /*
    * Used for Send Signature request
    */
    public class SendPostRequest extends AsyncTask<String, Void, String> {

        protected void onPreExecute() {
            isUploadingInProcess = true;
            isTaskCanceled = false;
            if(progress == null)
                progress = new ProgressDialog(SharePermissionActivity.this);
            progress.setCancelable(false);
            progress.setMessage("Please wait...");
            if(!progress.isShowing())
                progress.show();
        }

        protected String doInBackground(String... arg0) {

            return sendSignaturePostReq();

        }

        @Override
        protected void onPostExecute(String result) {
            if (result.equalsIgnoreCase(getString(R.string.did_not_work))) {
                progress.dismiss();
                SharePermissionActivity.ViewRetryDialog viewRetryDialog = new ViewRetryDialog();
                viewRetryDialog.showDialog(SharePermissionActivity.this);
            } else {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.has("id")) {
                        strSignaturePackId = jsonObject.getString("id");
                        uploadingImage();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
//            Toast.makeText(getApplicationContext(), "Post : " + result, Toast.LENGTH_LONG).show();
        }
    }

    public String getUserAgent(){
        return "Vboost Pro Android "+Build.VERSION.SDK_INT;
    }

    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");

    OkHttpClient okHttpClient = new OkHttpClient();


    /*
    * Used for Send Image request with image as a string
    */
    public class SendImagePostRequest extends AsyncTask<String, String, String> {

        protected void onPreExecute() {

        }

        protected String doInBackground(String... arg0) {

            InputStream inputStream = null;
            try {

                HttpParams httpParameters = new BasicHttpParams();
                ConnManagerParams.setTimeout(httpParameters, 30000);
                HttpConnectionParams.setConnectionTimeout(httpParameters, 30000);
                HttpConnectionParams.setSoTimeout(httpParameters, 30000);
                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient(httpParameters);

                // 2. make POST request to the given URL
                HttpPost httpPost = new HttpPost(Constant.SERVER_URL + "packageimage/?format=json");
                String json = "";

                // 3. build jsonObject
                JSONObject postDataParams = new JSONObject();
                postDataParams.put("package", arg0[0]);
                postDataParams.put("name", "front");
                postDataParams.put("image", arg0[1]);

                // 4. convert JSONObject to JSON to String
                json = postDataParams.toString();

                // 5. set json to StringEntity
                StringEntity se = new StringEntity(json);

                // 6. set httpPost Entity
                httpPost.setEntity(se);

                // 7. Set some headers to inform server about the type of the content
                httpPost.setHeader("Content-Type", "application/json");
                httpPost.setHeader("Authorization", "Token " + userCredentials);
                httpPost.setHeader("User-Agent", getUserAgent());
                httpPost.setHeader("Accept", "application/json");

                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPost);

                // 9. receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // 10. convert inputstream to string
                if (inputStream != null)
                    return convertInputStreamToString(inputStream);
                else
                    return getString(R.string.did_not_work);


            } catch (Exception e) {

                return getString(R.string.did_not_work);
                //return e.toString();
            }
        }

        @Override
        protected void onPostExecute(String result) {

            if (result == null) {
                if (progress != null && progress.isShowing())
                    progress.dismiss();
                Toast.makeText(SharePermissionActivity.this, R.string.txt_error_in_image_uploading, Toast.LENGTH_SHORT).show();
            } else {
                if (result.equalsIgnoreCase(getString(R.string.did_not_work))) {
                    if (progress != null && progress.isShowing())
                        progress.dismiss();
                    SharePermissionActivity.ViewRetryDialog viewRetryDialog = new ViewRetryDialog();
                    viewRetryDialog.showDialog(SharePermissionActivity.this);
                } else {
                    completionCOunt = completionCOunt + 1;
                    if (!isTaskCanceled && completionCOunt == photoAndNameStrArrayList.size())
                        new SendCompleteGetRequest().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, strSignaturePackId);
                    else
                        uploadingImage();
//            Toast.makeText(getApplicationContext(), "Post : " + result, Toast.LENGTH_LONG).show();
                }
            }

        }
    }

    public void uploadingImage() {

        if(!isTaskCanceled) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new SendImagePostRequest().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, strSignaturePackId, photoAndNameStrArrayList.get(completionCOunt).getPhotos());
            } else {
                new SendImagePostRequest().execute(strSignaturePackId, photoAndNameStrArrayList.get(completionCOunt).getPhotos());
            }
        }

    }

    int completionCOunt = 0;

    /*
    * Used for Send Image and signature completly uploaded request
    */
    public class SendCompleteGetRequest extends AsyncTask<String, String, String> {

        protected void onPreExecute() {

        }

        protected String doInBackground(String... arg0) {

            InputStream inputStream = null;
            try {

                HttpParams httpParameters = new BasicHttpParams();
                ConnManagerParams.setTimeout(httpParameters, 30000);
                HttpConnectionParams.setConnectionTimeout(httpParameters, 30000);
                HttpConnectionParams.setSoTimeout(httpParameters, 30000);
                // 1. create HttpClient
                HttpClient httpclient = new DefaultHttpClient(httpParameters);

                // 2. make POST request to the given URL
                HttpPatch httpPost = new HttpPatch(Constant.SERVER_URL + "package/" + strSignaturePackId + "/complete/");

                // 7. Set some headers to inform server about the type of the content
//                httpPost.setHeader("Content-Type", "application/json");
                httpPost.setHeader("Authorization", "Token " + userCredentials);
                httpPost.setHeader("User-Agent", getUserAgent());
//                httpPost.setHeader("Accept", "application/json");

                // 8. Execute POST request to the given URL
                HttpResponse httpResponse = httpclient.execute(httpPost);

                // 9. receive response as inputStream
                inputStream = httpResponse.getEntity().getContent();

                // 10. convert inputstream to string
                if (inputStream != null)
                    return convertInputStreamToString(inputStream);
                else
                    return getString(R.string.did_not_work);

            } catch (Exception e) {
                return getString(R.string.did_not_work);
                //   return e.toString();
            }
        }

        @Override
        protected void onPostExecute(String result) {

            if (result.equalsIgnoreCase(getString(R.string.did_not_work))) {
                if (progress != null && progress.isShowing())
                    progress.dismiss();
                SharePermissionActivity.ViewRetryDialog viewRetryDialog = new ViewRetryDialog();
                viewRetryDialog.showDialog(SharePermissionActivity.this);
            } else {
                if (progress != null && progress.isShowing())
                    progress.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.has("result")) {
                        finishOperation();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void finishOperation() {
        if (!browsedUris.isEmpty()) {
            for (Uri uri : browsedUris) {
                deleteImage(uri);
            }
        }
        DataLabels.destroy();
        Preference.destroy();
        if (progress != null && progress.isShowing())
            progress.dismiss();
        isUploadingInProcess = false;
        ViewDialog alert = new ViewDialog();
        alert.showDialog(SharePermissionActivity.this);
    }

    private final String USER_AGENT = "Mozilla/5.0";

    private String sendSignaturePostReq() {
        InputStream inputStream = null;
        String result = "";
        try {

            HttpParams httpParameters = new BasicHttpParams();
            ConnManagerParams.setTimeout(httpParameters, 10000);
            HttpConnectionParams.setConnectionTimeout(httpParameters, 30000);
            HttpConnectionParams.setSoTimeout(httpParameters, 30000);
            // 1. create HttpClient
            HttpClient httpclient = new DefaultHttpClient(httpParameters);

            // 2. make POST request to the given URL
            HttpPost httpPost = new HttpPost(Constant.SERVER_URL + "packages/?format=json");

            String json = "";

            // 3. build jsonObject
            JSONObject postDataParams = new JSONObject();
            postDataParams.put("contact", contactId);
            postDataParams.put("campaign", campaignId);
            postDataParams.put("recipient_name", name);
            postDataParams.put("recipient_email", email);
            postDataParams.put("recipient_phone", phone);
            postDataParams.put("recipient_permission", true);
            postDataParams.put("images", new JSONArray());

            // 4. convert JSONObject to JSON to String
            json = postDataParams.toString();

            // 5. set json to StringEntity
            StringEntity se = new StringEntity(json);

            // 6. set httpPost Entity
            httpPost.setEntity(se);

            // 7. Set some headers to inform server about the type of the content
            httpPost.setHeader("Content-Type", "application/json");
            httpPost.setHeader("Authorization", "Token " + userCredentials);
            httpPost.setHeader("User-Agent", getUserAgent());
            httpPost.setHeader("Accept", "application/json");

            // 8. Execute POST request to the given URL
            HttpResponse httpResponse = httpclient.execute(httpPost);

            // 9. receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();

            // 10. convert inputstream to string
            if (inputStream != null)
                return convertInputStreamToString(inputStream);
            else
                return getString(R.string.did_not_work);
            

        } catch (ConnectTimeoutException e) {
            return getString(R.string.did_not_work);
            //return e.toString();
        }catch (Exception e) {
            return getString(R.string.did_not_work);
            //return e.toString();
        }
    }

    private StringBuilder getStringBuilderFromHashMap(Map<String, Object> params) {
        StringBuilder postData = new StringBuilder();
        for (Map.Entry<String, Object> param : params.entrySet()) {
            if (postData.length() != 0) postData.append('&');
            try {
                postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));

                postData.append('=');
                postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return postData;
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    @Override
    protected void onStart() {
        super.onStart();
        client.getBus().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        client.getBus().unregister(this);
    }

    @Subscribe
    public void onPhotoUploadFinished(PhotoUploadResponseEvent event) {

        if (event.isSuccess()) {
            if (!browsedUris.isEmpty()) {
                for (Uri uri : browsedUris) {
                    deleteImage(uri);
                }
            }
            DataLabels.destroy();
            Preference.destroy();
            if (progress != null && progress.isShowing())
                progress.dismiss();
            ViewDialog alert = new ViewDialog();
            alert.showDialog(SharePermissionActivity.this);
            //Toast.makeText(this, "Uploaded successfully", Toast.LENGTH_SHORT).show();
        } else {
            if (progress != null && progress.isShowing())
                progress.dismiss();
            SharePermissionActivity.ViewRetryDialog viewRetryDialog = new SharePermissionActivity.ViewRetryDialog();
            viewRetryDialog.showDialog(SharePermissionActivity.this);
        }

        if (event.getMessage() != null)
            if (progress != null && progress.isShowing())
                progress.dismiss();
        System.out.println("Data not Send Becuase:  " + event.getMessage() + " Cause" + event.isSuccess());//logText.setText(event.getMessage());
    }

    @Subscribe
    public void onNetworkRequestFailed(NetworkRequestFailedEvent event) {
        if (progress != null && progress.isShowing())
            progress.dismiss();
        if (event.getCause().getMessage().contains("timeout") || event.getCause().getMessage().contains("connect timed out")) {
            ViewRetryDialog viewRetryDialog = new ViewRetryDialog();
            viewRetryDialog.showDialog(SharePermissionActivity.this);
        } else {
            Intent intent = new Intent("my.action.string");
            intent.putExtra("userCredentials", userCredentials);
            sendBroadcast(intent);
            ViewDialog alert = new ViewDialog();
            alert.showDialog(SharePermissionActivity.this);
        }
        // Toast.makeText(this, "Please make sure network is turned on", Toast.LENGTH_SHORT).show();
    }

    public void sharePermissionBack(View view) {


        finish();
    }

    public class ViewDialog {

        public void showDialog(Activity activity) {
            final Dialog dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.permission_ok_alert);

            Button dialogButton = (Button) dialog.findViewById(R.id.done_button_dialog);
            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Constant.deleteAllCampaignImages();
                    dialog.dismiss();
                    if (singleCampaign) {

                        Intent campaignsDataIntent = new Intent(SharePermissionActivity.this, CampaignsDataActivity.class);
                        campaignsDataIntent.putExtra("contactsName", contactsName);
                        campaignsDataIntent.putExtra("campaignrow", campaign);
                        campaignsDataIntent.putExtra("contacts", contactsList);
                        campaignsDataIntent.putExtra("createNew", true);
                        campaignsDataIntent.putExtra("campaignArray", campaignsArray);
                        startActivity(campaignsDataIntent);
                        finish();

                    } else {
                        Intent contactIntent = new Intent(SharePermissionActivity.this, CampaignsActivity.class);
                        contactIntent.putExtra("contactsName", contactsName);
                        contactIntent.putExtra("contacts", contactsList);
                        contactIntent.putExtra("campaignArray", campaignsArray);
                        contactIntent.putExtra("createNew", true);
                        startActivity(contactIntent);
                        finish();
                    }
                }
            });

            dialog.show();

        }
    }

    public class ViewRetryDialog {

        public void showDialog(Activity activity) {
            final Dialog dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.retry_alert_new);

//            ((TextView) dialog.findViewById(R.id.text_dialog)).setText(Html.fromHtml(getString(R.string.txt_error_submit_images)));
            dialog.findViewById(R.id.a).setVisibility(View.GONE);
            Button dialogButton = (Button) dialog.findViewById(R.id.retry_button_dialog);
            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    if (isOnline(SharePermissionActivity.this)) {
                        if (progress == null)
                            progress = new ProgressDialog(SharePermissionActivity.this);
                        progress.setMessage("Please wait...");
//                        if (!isFinishing() && progress != null && !progress.isShowing())
//                            progress.show();
                        dialog.dismiss();
                        //  client.getBus().post(new PhotoUploadRequestEvent(photoAndNameArrayList, campaignId, contactId, name, email, 0, phone, ""));
                        new SendPostRequest().execute();
                    } else {
                        SharePermissionActivity.ViewRetryDialog viewRetryDialog = new SharePermissionActivity.ViewRetryDialog();
                        viewRetryDialog.showDialog(SharePermissionActivity.this);
                    }
                }
            });
            if(!isFinishing())
                dialog.show();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {

                ViewDialog alert = new ViewDialog();
                alert.showDialog(SharePermissionActivity.this);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }//onActivityResult


 /*   public String imageStringFormat(String path) {

        Bitmap bitmapOrg = BitmapFactory.decodeFile(path);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmapOrg.compress(Bitmap.CompressFormat.JPEG, 80, stream);
        byte[] byteArray = stream.toByteArray();
        String imageString = Base64.encodeToString(byteArray, 0);

        return imageString;
    }*/
/*
    public String createJson() {

        try {


            JSONArray jsonArrayTo = new JSONArray();

            for (int i = 0; i < imagesAndLabels.size(); i++) {
                DataLabels dataLabels = imagesAndLabels.get(i);
                // Log.d("Image String", imageStringFormat(dataLabels.getImage()));
                JSONObject jsonObjectTo = new JSONObject();
                String image = dataLabels.getImage();
                String newString = image.replaceAll("\n", "");
                writeToSDFile(newString, "ImageNo"+i);
                jsonObjectTo.put("image", newString);
                jsonObjectTo.put("name", dataLabels.getName());
                jsonArrayTo.put(jsonObjectTo);
            }


            System.out.println("output:   "+ jsonArrayTo.toString(2));
            return jsonArrayTo.toString(2);

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    private class ShareNoPermission extends AsyncTask<String, Void, Void> {

        private final Context context;

        public ShareNoPermission(Context c) {
            this.context = c;
        }

        protected void onPreExecute() {
            progress = new ProgressDialog(this.context);
            progress.setMessage("Loading");
            progress.show();
        }

        @Override
        protected Void doInBackground(String... params) {

           // postmanFunction();
            try {

                URL url = new URL("http://vboostoffice.criterion-dev.net/api/v1/packages/");

                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                String finalImageJson = imagesJson.replaceAll("\\\\", "");
                String urlParameters = "campaign=" + campaignId +
                        "&contact=" + contactId +
                        "&recipient_name=" + name +
                        "&recipient_phone=" + deliveryMethod +
                        "&recipient_permission=0"+
                        "&images="+theString;

                String tokenAuth = "Token " + userCredentials;
                connection.setRequestProperty("Authorization", tokenAuth);

                //connection.setDoInput(true);
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                connection.setRequestProperty("User-Agent", USER_AGENT+android.os.Build.VERSION.SDK_INT);
                connection.setRequestProperty("charset", "utf-8");
                connection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
                connection.setRequestProperty("Content-Length", "" + Integer.toString(urlParameters.getBytes().length));
                connection.setUseCaches(false);


                // Send post request
                connection.setDoOutput(true);
                DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
                wr.write(urlParameters.getBytes());
                wr.flush();
                wr.close();

                final int responseCode = connection.getResponseCode();
                System.out.println("\nSending 'POST' request to URL : " + url);
                System.out.println("Post parameters : " + urlParameters);
                System.out.println("Response Code : " + responseCode);

                try {

                    BufferedReader reader = null;

                    if(connection.getResponseCode() == 201)
                    {
                        reader = new BufferedReader(new
                                InputStreamReader(connection.getInputStream()));
                        String inputLine;
                        StringBuffer response = new StringBuffer();

                        while ((inputLine = reader.readLine()) != null) {
                            response.append(inputLine);
                        }
                        reader.close();
                        System.out.println(response.toString());
                    }
                    else
                    {
                        reader = new BufferedReader(new
                                InputStreamReader(connection.getErrorStream()));
                        String inputLine;
                        StringBuffer response = new StringBuffer();

                        while ((inputLine = reader.readLine()) != null) {
                            response.append(inputLine);
                        }
                        reader.close();
                        System.out.println(response.toString());
                    }

                    // BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));


                    //print result

                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Catch Error:   "+ e.getLocalizedMessage());
                }


                SharePermissionActivity.this.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {

                        if (responseCode == 201) {
                            progress.dismiss();
                            DataLabels.destroy();
                            Preference.destroy();
                            ViewDialog alert = new ViewDialog();
                            alert.showDialog(SharePermissionActivity.this);

                        }else if(responseCode == 500){
//                            DataLabels.destroy();
//                            Preference.destroy();
                            Toast.makeText(SharePermissionActivity.this, "Sorry, unexpected error has occured. Our administrators are already notified and shall do their best to solve it.", Toast.LENGTH_LONG).show();
                            progress.dismiss();

                        }else {
//                            DataLabels.destroy();
//                            Preference.destroy();
                            Toast.makeText(SharePermissionActivity.this, "Data not send", Toast.LENGTH_LONG).show();
                            progress.dismiss();
                        }

                    }
                });

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute() {
            progress.dismiss();
        }

    }*/


    public boolean isOnline(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        //should check null because in airplane mode it will be null
        return (netInfo != null && netInfo.isConnected());

    }

    public void deleteImage(Uri uri) {
        //String file_dj_path = Environment.getExternalStorageDirectory() + "/ECP_Screenshots/abc";
        File fdelete = new File(uri.getPath());
        if (fdelete.exists()) {
            if (fdelete.delete()) {

                if (fdelete.exists()) {
                    try {
                        fdelete.getCanonicalFile().delete();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (fdelete.exists()) {
                        getApplicationContext().deleteFile(fdelete.getName());
                    }
                }
                Log.e("-->", "file Deleted :" + uri.getPath());
                callBroadCast(uri.getPath(), true);
            } else {
                Log.e("-->", "file not Deleted :" + uri.getPath());
            }
        }
    }

    public void callBroadCast(String path, final boolean isDelete) {
        if (Build.VERSION.SDK_INT >= 14) {
            Log.e("-->", " >= 14");
            MediaScannerConnection.scanFile(this, new String[]{path}, null, new MediaScannerConnection.OnScanCompletedListener() {
                /*
                 *   (non-Javadoc)
                 * @see android.media.MediaScannerConnection.OnScanCompletedListener#onScanCompleted(java.lang.String, android.net.Uri)
                 */
                public void onScanCompleted(String path, Uri uri) {
                    if (isDelete) {
                        if (uri != null) {
                            getApplicationContext().getContentResolver().delete(uri,
                                    null, null);
                        }
                    }
                    Log.e("ExternalStorage", "Scanned " + path + ":");
                    Log.e("ExternalStorage", "-> uri=" + uri);
                }
            });
        } else {
            Log.e("-->", " < 14");
            sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED,
                    Uri.parse("file://" + Environment.getExternalStorageDirectory())));
        }
    }

}
