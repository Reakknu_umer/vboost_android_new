package com.vboost.pro.Util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;

import com.vboost.pro.service.MyApplication;

import java.io.File;

public class Constant {
    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String Login = "Login" ;
    public static final String JudgeProspect = "JudgeProspect" ;

    // file path
    public static final String IMAGE_CAMERA = "CameraImage";
    public static final String IMAGE_GALLERY = "GalleryImage";
    public static final String TRACKER_FOLDER = "Vboost";
    public static final Object FOLDER_NAME = "vBoost";

    public static final String DEVELOPMENT_URL = "http://vboostoffice.criterion-dev.net/api/v1/";
    public static final String PRODUCTION_URL = "http://vboostoffice.com/api/v1/";
    public static final String SERVER_URL = PRODUCTION_URL;

    public static void isParentFolderExist(File mainFolder) {
        if (!mainFolder.exists()) {
            mainFolder.mkdirs();
        }
    }

    public static boolean isConnected() {
        ConnectivityManager cm = (ConnectivityManager) MyApplication.getInstance().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    public static void deleteAllCampaignImages() {
        File mainFolder = new File(Environment.getExternalStorageDirectory() + "/" + Constant.FOLDER_NAME);

        if (mainFolder.isDirectory())
        {
            String[] children = mainFolder.list();
            for (int i = 0; i < children.length; i++)
            {
                new File(mainFolder, children[i]).delete();
            }
        }

        if (!mainFolder.exists()) {
            mainFolder.mkdirs();
        }
    }
}
