package com.vboost.pro;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.vboost.pro.DBClasses.Company;

import com.squareup.picasso.Picasso;
import com.vboost.pro.Util.Constant;

import java.io.File;
import java.util.List;

/**
 * Created by Nabeel Hafeez on 8/15/2016.
 */
public class LogOutActivity extends AppCompatActivity {

    Button logout;
    Button next;

    private SharedPreferences sharedpreferences;
    private SharedPreferences.Editor editor;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logout);

        sharedpreferences = getSharedPreferences(Constant.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = getSharedPreferences(Constant.MyPREFERENCES, MODE_PRIVATE).edit();

        List<Company> company = Company.getAll();

        ImageView companyLogo = (ImageView) findViewById(R.id.logo2);

        Picasso.with(this)
                .load(company.get(0).getLogo())
                .placeholder(R.drawable.logo2)
                .error(R.drawable.logo2)
                .into(companyLogo);

        logout = (Button) findViewById(R.id.logout_button);
        logout.setAllCaps(false);
        next = (Button) findViewById(R.id.logout_next_button);
        next.setAllCaps(false);

        logout.getBackground().setColorFilter(new LightingColorFilter(Color.parseColor("#8ec63e"), 0x00000000));
        next.getBackground().setColorFilter(new LightingColorFilter(Color.parseColor("#FBA919"), 0x00000000));
    }

    public void logoutButton(View view) {

       /* SharedPreferences settings = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        settings.edit().clear().commit();
        deleteCache(getApplicationContext());
        Campaign.destroy();
        Company.destroy();
        Contact.destroy();
        DataLabels.destroy();
        NewUser.destroy();
        Preference.destroy();
        User.destroy();
        MyApplication.getInstance().clearApplicationData();
*/
        boolean cacheClear = ((ActivityManager) getApplicationContext().getSystemService(ACTIVITY_SERVICE)).clearApplicationUserData(); // note: it has a return value!
        System.out.println("Cache Clear" + cacheClear);
        Log.e("Cache Clear", "" + cacheClear);

        editor.putBoolean(Constant.Login, false);
        editor.commit();

        Intent intent = new Intent(LogOutActivity.this, SplashActivity.class);
        startActivity(intent);
        finish();

    }

    public void getHelp(View view) {

        Intent helpIntent = new Intent(LogOutActivity.this, GetHelpActivity.class);
        startActivity(helpIntent);
    }

    public void gotoContacts(View view) {

        Intent intent = new Intent(LogOutActivity.this, ManageContatcsAvtivity.class);
        startActivity(intent);
        finish();
    }

    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile())
            return dir.delete();
        else {
            return false;
        }
    }

    private static void killProcessesAround(Activity activity) throws PackageManager.NameNotFoundException {

        ActivityManager am = (ActivityManager) activity.getSystemService(Context.ACTIVITY_SERVICE);

        String myProcessPrefix = activity.getApplicationInfo().processName;

        String myProcessName = activity.getPackageManager().getActivityInfo(activity.getComponentName(), 0).processName;

        for (ActivityManager.RunningAppProcessInfo proc : am.getRunningAppProcesses()) {
            if (proc.processName.startsWith(myProcessPrefix) && !proc.processName.equals(myProcessName)) {
                android.os.Process.killProcess(proc.pid);
            }
        }
    }

}



