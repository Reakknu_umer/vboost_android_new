package com.vboost.pro;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.vboost.pro.DBClasses.Company;

import java.util.List;

/**
 * Created by Nabeel Hafeez on 9/28/2016.
 */

public class TermandCondtions extends AppCompatActivity {

    TextView termsConditionText;
    String termsText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
        // or = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
        setRequestedOrientation(orientation);


        setContentView(R.layout.term_and_codition_layout);

        List<Company> company = Company.getAll();
        termsConditionText = (TextView) findViewById(R.id.terms_and_conditons_textview);
        termsConditionText.setText(company.get(0).getTerms());

    }

    public void closeActivityTermCondition(View view){

        finish();
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        int orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
        // or = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
        setRequestedOrientation(orientation);
        // Add code if needed
    }

}
