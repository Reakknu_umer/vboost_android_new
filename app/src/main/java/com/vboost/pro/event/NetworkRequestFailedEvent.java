package com.vboost.pro.event;

/**
 * @author Samvel Abrahamyan
 */

public class NetworkRequestFailedEvent {
    private Throwable cause;

    public NetworkRequestFailedEvent(Throwable cause) {
        this.cause = cause;
    }

    public Throwable getCause() {
        return cause;
    }
}
