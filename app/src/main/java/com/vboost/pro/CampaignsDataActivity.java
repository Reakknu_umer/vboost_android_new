package com.vboost.pro;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.BaseColumns;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.bigkoo.alertview.AlertView;
import com.bigkoo.alertview.OnDismissListener;
import com.bigkoo.alertview.OnItemClickListener;
import com.darsh.multipleimageselect.activities.AlbumSelectActivity;
import com.darsh.multipleimageselect.activities.ImageSelectActivity;
import com.darsh.multipleimageselect.helpers.Constants;
import com.darsh.multipleimageselect.models.Image;
import com.squareup.picasso.Picasso;
import com.vboost.pro.Adapter.CampaignsDataAdapter;
import com.vboost.pro.DBClasses.Company;
import com.vboost.pro.DBClasses.DataLabels;
import com.vboost.pro.Network.NetworkChangeReceiver;
import com.vboost.pro.Util.Constant;
import com.vboost.pro.Util.FileUtils;
import com.vboost.pro.model.Campaigns;
import com.vboost.pro.model.Labels;
import com.vboost.pro.model.ManageContacts;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nabeel Hafeez on 8/10/2016.
 */
public class CampaignsDataActivity extends AppCompatActivity implements CampaignsDataAdapter.CallbackInterface, OnItemClickListener, OnDismissListener {

    ArrayList<ManageContacts> contactsList;
    ArrayList<Labels> labalesArrayList;
    CampaignsDataAdapter campaignsDataAdapter;

    boolean isCameraOpen = false;
    ArrayList<Campaigns> campaignsArray;
    ArrayList<String> labalesDBArrayList;
    String[] imagesArray;
    Campaigns campaign;
    String[] contactsName;
    ListView cameraImageList;
    String selectedProspector, yourName;
    int minCount;
    int maxCount;
    int defaultCount;
    int campaingID;
    int contactID;

    String emailPattern;
    int keyDel;

    Button phoneTextButton;
    Button emailTextButton;
    Button nextButton;
    Button nameText;
    EditText phoneText;
    EditText emailText;
    AlertView selectName;
    AlertView campaignDataMenu;
    AlertView DataMenu;

    private ImageView toggleButton;
    private boolean judgeToggleBtn;

    ScrollView scrollView;

    String editTextFlag;
    String deliverText;


    private int currentPosition;

    private static final int PERMISSION_REQUEST_CODE = 1;
    public static final int MY_REQUEST_CAMERA = 1001;
    public static final int MY_REQUEST_GALLERY = 1011;
    public static final int MY_REQUEST_AGAIN = 1012;
    public static final int MY_REQUEST_TO_CAMERA = 1013;

    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs";
    public static final String TEMPORARY_IMAGEPATH = "tempo_path";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_campaigns_data);

        List<Company> company = Company.getAll();

        ImageView companyLogo = (ImageView) findViewById(R.id.campaigns_data_logo);


        Picasso.with(this)
                .load(company.get(0).getLogo())
                .error(R.drawable.logo2)
                .into(companyLogo);

        contactsName = getIntent().getStringArrayExtra("contactsName");
        contactsList = (ArrayList<ManageContacts>) getIntent().getSerializableExtra("contacts");
        campaign = (Campaigns) getIntent().getSerializableExtra("campaignrow");
        campaignsArray = (ArrayList<Campaigns>) getIntent().getSerializableExtra("campaignArray");
        selectedProspector = getIntent().hasExtra("selectedProspector") ? getIntent().getStringExtra("selectedProspector") : "";

//        campaignSelectedUser =

//        int itemNumber = (int) getIntent().getSerializableExtra("position");
//        int maxLen;
//        if(itemNumber == 2) {
//            maxLen = 6;
//        } else {
//            maxLen = 4;
//        }

        emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        scrollView = (ScrollView) findViewById(R.id.campaigns_data_scroll_view);
        phoneTextButton = (Button) findViewById(R.id.campaigns_data_phone_button);
        emailTextButton = (Button) findViewById(R.id.campaigns_data_mail_button);
        nextButton = (Button) findViewById(R.id.campaigns_data_next_button);
        nextButton.setAllCaps(false);
        cameraImageList = (ListView) findViewById(R.id.list_view_campaigns_images);
        nameText = (Button) findViewById(R.id.contact_name);
        nameText.setAllCaps(false);


        phoneText = (EditText) findViewById(R.id.phone_edit_text);
        emailText = (EditText) findViewById(R.id.email_edit_text);

        toggleButton = (ImageView) findViewById(R.id.toggleButton);
        judgeToggleBtn = false;
        changeToggleImage();

        onClickToggleButton();

        if (Build.VERSION.SDK_INT >= 23) {
            if (checkPermission()) {

                Log.e("value", "Permission already Granted, Now you can get image.");
            } else {
                requestPermission();
            }
        } else {

            Log.e("value", "Not required for requesting runtime permission");
        }

        selectName = new AlertView(null, null, null, null,
                contactsName,
                this, AlertView.Style.ActionSheet, this);

        campaignDataMenu = new AlertView(null, null, null, null,
                new String[]{"Change Customer Type",
                        "Manage Contacts",
                        "Switch Companies",
                        "Get Help",
                },
                this, AlertView.Style.Alert, this).setCancelable(true).setOnDismissListener(this);

        DataMenu = new AlertView(null, null, null, null,
                new String[]{
                        "Manage Contacts",
                        "Switch Companies",
                        "Get Help",
                },
                this, AlertView.Style.Alert, this).setCancelable(true).setOnDismissListener(this);

        scrollView.bringToFront();
        minCount = campaign.getImage().getMin_count();
        maxCount = campaign.getImage().getMax_count();
        defaultCount = campaign.getImage().getDefault_count();
        labalesArrayList = campaign.getImage().getLabels();
        campaingID = campaign.getId();


        imagesArray = new String[defaultCount];

        phoneTextButton.getBackground().setColorFilter(new LightingColorFilter(Color.parseColor("#569fd7"), 0x00000000));
        emailTextButton.getBackground().setColorFilter(new LightingColorFilter(Color.parseColor("#569fd7"), 0x00000000));
        nextButton.getBackground().setColorFilter(new LightingColorFilter(Color.parseColor("#FBA919"), 0x00000000));

        List<DataLabels> dataLabels;
        dataLabels = DataLabels.getAll();
        int len = dataLabels.size();

        if (len != 0) {

            if (len > defaultCount) {
                len = defaultCount;
            }

            for (int i = 0; i < len; i++) {

                String imageString = dataLabels.get(i).getImage();

                if (!imageString.equals("")) {
                    imagesArray[i] = imageString;
                }
            }
            campaignsDataAdapter = new CampaignsDataAdapter(CampaignsDataActivity.this, imagesArray, minCount, maxCount, defaultCount, labalesArrayList, true, false);
            cameraImageList.setAdapter(campaignsDataAdapter);


        } else {
            if (isOnline(this)) {
                campaignsDataAdapter = new CampaignsDataAdapter(CampaignsDataActivity.this, imagesArray, minCount, maxCount, defaultCount, labalesArrayList, false, false);
                cameraImageList.setAdapter(campaignsDataAdapter);
            } else {

            }
        }

        for (int i = 0; i < contactsName.length; i++) {
            if (contactsName[i] != null && campaign.getName() != null) {
                String currentName = contactsName[i].toLowerCase();
                String NameWithProspect = campaign.getName().toLowerCase();


                if (NameWithProspect.contains(currentName)) {
                    yourName = contactsName[i];
                    contactID = contactsList.get(i).getId();
                    nameText.setText(yourName);
                    break;

                }
            }

        }
    }

    private boolean checkIsExist(String selectedProspector) {
        for (int i = 0; i < contactsName.length; i++) {
            if (contactsName[i].equalsIgnoreCase(selectedProspector))
                contactID = contactsList.get(i).getId();
            return true;
        }
        return false;
    }

    public void changeToggleImage() {
        toggleButton.setBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent));
        if (!judgeToggleBtn) {
            isCameraOpen = false;
            toggleButton.setImageResource(R.drawable.ic_toggle);
        } else {
            toggleButton.setImageResource(R.drawable.ic_camera_3);
            if (imagesArray != null) {
                int findCount = -1;
                for (int i = 0; i < imagesArray.length; i++) {
                    if (imagesArray[i] == null)
                        findCount++;
                }

                if (findCount != -1) {
                    Intent intent = new Intent(getApplicationContext(), ImageSelectActivity.class);
                    intent.putExtra(Constants.INTENT_EXTRA_LIMIT, findCount + 1);
                    intent.putExtra(Constants.INTENT_EXTRA_ALBUM, "Camera");

                    startActivityForResult(intent, Constants.REQUEST_CODE);
                }
            }
        }
    }

    public void onClickToggleButton() {
        toggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                judgeToggleBtn = !judgeToggleBtn;
                changeToggleImage();
            }
        });
//        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//
//
////                if(isChecked){
////                    // display camera, image from gallery
////
////                    // Make a toast to display toggle button status
////                    Toast.makeText(getApplicationContext(),
////                            "Toggle is on", Toast.LENGTH_SHORT).show();
////                } else{
////                    // display gallery, image from camera
////
////                    // Make a toast to display toggle button status
////                    Toast.makeText(getApplicationContext(),
////                            "Toggle is off", Toast.LENGTH_SHORT).show();
////                }
//            }
//        });
    }

    public void enterPhoneNumber(View view) {

        phoneTextButton.getBackground().setColorFilter(new LightingColorFilter(Color.parseColor("#8ec63e"), 0x00000000));
        emailTextButton.getBackground().setColorFilter(new LightingColorFilter(Color.parseColor("#569fd7"), 0x00000000));
        phoneText.setHint(R.string.delivery_phone_text_method);
        phoneText.setVisibility(View.VISIBLE);
        emailText.setVisibility(View.INVISIBLE);
        phoneText.setSingleLine();
        phoneText.setInputType(InputType.TYPE_CLASS_PHONE);

        phoneText.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(phoneText, InputMethodManager.SHOW_IMPLICIT);

        editTextFlag = "phone";

        phoneText.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() != KeyEvent.ACTION_DOWN)
                    return false;

                if (keyCode == KeyEvent.KEYCODE_DEL && phoneText.getText().toString().length() > 0)
                    keyDel = 1;
                return false;
            }
        });
        phoneText.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            public void afterTextChanged(Editable s) {
                if (keyDel == 0) {


                    if (s.toString().length() == 2 && s.toString().charAt(1) != '-') {
                        String str = s.toString().substring(0, 1) + "-";
                        String str1 = s.toString().substring(1, s.toString().length());
                        phoneText.setText(str + str1);

                    }

                    if (s.toString().length() == 6 && s.toString().charAt(5) != '-') {
                        String str = s.toString().substring(0, 5) + "-";
                        String str1 = s.toString().substring(5, s.toString().length());
                        phoneText.setText(str + str1);
                    }

                    if (s.toString().length() == 10 && s.toString().charAt(9) != '-') {
                        String str = s.toString().substring(0, 9) + "-";
                        String str1 = s.toString().substring(9, s.toString().length());
                        phoneText.setText(str + str1);
                    }
                    phoneText.setSelection(phoneText.getText().toString().length());

                    if (s.toString().length() == 1) {
                        if (s.toString().equalsIgnoreCase("1")) {
                            String str = s.toString() + "-";
                            phoneText.setText(str);
                            phoneText.setSelection(str.length());
                        } else {
                            String str = "1-" + s.toString();
                            phoneText.setText(str);
                            phoneText.setSelection(str.length());
                        }
                    } else {
                        if (s.toString().length() == 5) {
                            String str = s.toString() + "-";
                            phoneText.setText(str);
                            phoneText.setSelection(str.length());
                        } else if (s.toString().length() == 9) {
                            String str = s.toString() + "-";
                            phoneText.setText(str);
                            phoneText.setSelection(str.length());
                        }
                    }

                    if (phoneText.getText().toString().length() > 0) {

                    }

                    //String strInput = phoneText.getText().toString().replaceAll("-", "");
                    /*if (strInput.length() > 9) {
                        String str1 = strInput.substring(0, 1);
                        String str2 = strInput.substring(1, 4);
                        String str3 = strInput.substring(4, 7);
                        String str4 = strInput.substring(7);
                        phoneText.setText(str1 + "-" + str2 + "-" + str3 + "-" + str4);

                    } else if (strInput.length() > 4) {
                        String str1 = strInput.substring(0, 1);
                        String str2 = strInput.substring(1, 4);
                        String str3 = strInput.substring(4);
                        phoneText.setText(str1 + "-" + str2 + "-" + str3);

                    } else if (strInput.length() > 1) {
                        String str1 = strInput.substring(0, 1);
                        String str2 = strInput.substring(1);
                        phoneText.setText(str1 + "-" + str2);
                    }*/
                    deliverText = phoneText.getText().toString();
                } else {
                    keyDel = 0;
                    if (s.toString().equalsIgnoreCase("1-")) {
                        phoneText.setText("");
                    }
                    deliverText = phoneText.getText().toString();
                }
            }

        });

    }

    public void enterEmail(View view) {

        phoneTextButton.getBackground().setColorFilter(new LightingColorFilter(Color.parseColor("#569fd7"), 0x00000000));
        emailTextButton.getBackground().setColorFilter(new LightingColorFilter(Color.parseColor("#8ec63e"), 0x00000000));
        emailText.setHint(R.string.delivery_mail_text_method);
        emailText.setVisibility(View.VISIBLE);
        phoneText.setVisibility(View.INVISIBLE);
        emailText.setSingleLine();
        emailText.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        editTextFlag = "email";

        emailText.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(emailText, InputMethodManager.SHOW_IMPLICIT);

    }

    public void campaignsPermissionButton(View view) {
        String campaignName = nameText.getText().toString();
        //campaignName =  campaignName.trim();
        boolean imagesArrayUncomplete = false;

        for (int i=0; i<imagesArray.length; i++){
            if(TextUtils.isEmpty(imagesArray[i])) {
                imagesArrayUncomplete = true;
                break;
            }
            else{

            }
        }

        if (imagesArrayUncomplete) {

            new AlertView(null, "\nPlease take at least " + defaultCount + " photos.", null, null, new String[]{"OK"}, this, AlertView.Style.Alert, this).show();

        } else {
            if (campaignName.equals("Select Your Name")) {

                new AlertView(null, "\nPlease select your name from the \ndropdown menu.", null, null, new String[]{"OK"}, this, AlertView.Style.Alert, this).show();

            } else {

                if (editTextFlag != null) {
                    if (editTextFlag.equals("email")) {
                        deliverText = emailText.getText().toString();
                        if (deliverText.matches(emailPattern)) {

                            Intent sharePermissionIntent = new Intent(CampaignsDataActivity.this, SharePermissionActivity.class);
                            sharePermissionIntent.putExtra("name", nameText.getText().toString());
                            sharePermissionIntent.putExtra("deliverMethod", deliverText);
                            sharePermissionIntent.putExtra("campaignID", campaingID);
                            sharePermissionIntent.putExtra("contactID", contactID);
                            sharePermissionIntent.putExtra("deliveryFlag", editTextFlag);
                            sharePermissionIntent.putExtra("contactsName", contactsName);
                            sharePermissionIntent.putExtra("campaignrow", campaign);
                            sharePermissionIntent.putExtra("contacts", contactsList);
                            sharePermissionIntent.putExtra("campaignArray", campaignsArray);
                            if (campaignsArray.size() == 1) {
                                sharePermissionIntent.putExtra("singleCampaign", true);
                            } else {
                                sharePermissionIntent.putExtra("singleCampaign", false);
                            }
                            startActivity(sharePermissionIntent);

                            finish();


                        } else {

                            new AlertView(null, "\nPlease enter valid email address!", null, null, new String[]{"OK"}, this, AlertView.Style.Alert, this).show();

                        }

                    } else {
                        deliverText = phoneText.getText().toString();
                        if (deliverText.length() < 14) {
                            new AlertView(null, "\nPlease Enter 10 digit phone number!", null, null, new String[]{"OK"}, this, AlertView.Style.Alert, this).show();

                        } else {

                            deliverText = deliverText.substring(2, deliverText.length());

                            Intent sharePermissionIntent = new Intent(CampaignsDataActivity.this, SharePermissionActivity.class);
                            sharePermissionIntent.putExtra("name", nameText.getText().toString());
                            sharePermissionIntent.putExtra("deliverMethod", deliverText);
                            sharePermissionIntent.putExtra("campaignID", campaingID);
                            sharePermissionIntent.putExtra("contactID", contactID);
                            sharePermissionIntent.putExtra("deliveryFlag", editTextFlag);
                            sharePermissionIntent.putExtra("contactsName", contactsName);
                            sharePermissionIntent.putExtra("campaignrow", campaign);
                            sharePermissionIntent.putExtra("contacts", contactsList);
                            sharePermissionIntent.putExtra("campaignArray", campaignsArray);
                            if (campaignsArray.size() == 1) {
                                sharePermissionIntent.putExtra("singleCampaign", true);
                            } else {
                                sharePermissionIntent.putExtra("singleCampaign", false);
                            }
                            startActivity(sharePermissionIntent);

                            finish();

                        }

                    }

                } else {
                    new AlertView(null, "\nPlease select the delivery method.", null, null, new String[]{"OK"}, this, AlertView.Style.Alert, this).show();

                }
            }
        }
    }

    public void selectYourName(View view) {

        selectName.show();
    }

    public void closeActivity(View view) {

        finish();

    }

    public void alertMenuCampaignData(View view) {
        if (campaignsArray.size() == 1) {
            DataMenu.show();
        } else {
            campaignDataMenu.show();
        }
    }

    SharedPreferences.Editor editor;

    public void savePreferencePath(String path) {
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        editor = getSharedPreferences(Constant.MyPREFERENCES, MODE_PRIVATE).edit();
        editor.putString(TEMPORARY_IMAGEPATH, path);
        editor.commit();
    }

    public String getImagePath() {
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        String path = sharedpreferences.getString(TEMPORARY_IMAGEPATH, "");
        savePreferencePath("");
        return path;

    }

    @Override
    public void onItemClick(Object o, int position) {

        if (o == selectName && position != AlertView.CANCELPOSITION) {
            for (int i = 0; i < contactsName.length; i++) {
                if (position == i) {

                    yourName = contactsName[i];
                    contactID = contactsList.get(i).getId();
                }
            }
            nameText.setText(yourName);
        }

        if (o == campaignDataMenu && position != AlertView.CANCELPOSITION) {
            if (position == 0) {
                Intent contactIntent = new Intent(CampaignsDataActivity.this, CampaignsActivity.class);
                contactIntent.putExtra("contactsName", contactsName);
                contactIntent.putExtra("contacts", contactsList);
                contactIntent.putExtra("campaignArray", campaignsArray);
                startActivity(contactIntent);
                finish();

            } else if (position == 1) {

                Intent contactIntent = new Intent(CampaignsDataActivity.this, ManageContatcsAvtivity.class);
                startActivity(contactIntent);
                finish();

            } else if (position == 2) {

                Intent logoutIntent = new Intent(CampaignsDataActivity.this, LogOutActivity.class);
                startActivity(logoutIntent);
                finish();

            } else if (position == 3) {

                Intent helpIntent = new Intent(CampaignsDataActivity.this, GetHelpActivity.class);
                startActivity(helpIntent);

            }
        }

        if (o == DataMenu && position != AlertView.CANCELPOSITION) {
            if (position == 0) {

                Intent contactIntent = new Intent(CampaignsDataActivity.this, ManageContatcsAvtivity.class);
                startActivity(contactIntent);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    finishAffinity();
                }
                finish();

            } else if (position == 1) {

                Intent logoutIntent = new Intent(CampaignsDataActivity.this, LogOutActivity.class);
                startActivity(logoutIntent);
                finish();

            } else if (position == 2) {

                Intent helpIntent = new Intent(CampaignsDataActivity.this, GetHelpActivity.class);
                startActivity(helpIntent);
            }
        }


    }

    @Override
    public void onDismiss(Object o) {


    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
//            if(mAlertView!=null && mAlertView.isShowing()){
//                mAlertView.dismiss();
//                return false;
//            }
        }

        return super.onKeyDown(keyCode, event);

    }

   /* @Override
    protected void onActivityResult(int requestCode,int resultCode,Intent data) {
        super.onActivityResult(requestCode,resultCode,data);

        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK) {
                String path = getIntent().getStringExtra("imagePath");
                images.add(path);
                Log.e("Activity Result Call", path);
            }
            if (resultCode == Activity.RESULT_CANCELED) {

            }
        }

    }*/

    /**
     * Interface Method which communicates to the Acitivty here from the {@link }
     *
     * @param position - the position
     * @param text     - the text to pass back
     */
    @Override
    public void onHandleSelection(int position, String text) {
        boolean editPhoto;

        if (imagesArray[position] != null) {

            editPhoto = true;
            String imagePath = imagesArray[position].toString();
            Intent intent = new Intent(CampaignsDataActivity.this, PhotoResultActivity.class);
            intent.putExtra("imagepath", imagePath);
            intent.putExtra("phototitle", text);
            intent.putExtra("Position", position);
            intent.putExtra("editphoto", editPhoto);

            if (judgeToggleBtn) {
                // image from gallery

//                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
//                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, MY_REQUEST_AGAIN/*MY_REQUEST_GALLERY*/);
            } else {
                // image from camera

                startActivityForResult(intent, MY_REQUEST_AGAIN);
            }

            currentPosition = position;
        } else {

            editPhoto = false;

//            Long cameraClickTime = System.currentTimeMillis();
//            String fileNameExtension = cameraClickTime + "";
//
//            //  destinationFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM)+File.separator+"Camera"+File.separator, fileNameExtension);
//
//            String fileName = fileNameExtension;

            // Create parameters for Intent with filename

//            ContentValues values = new ContentValues();
//
//            values.put(MediaStore.Images.Media.TITLE, fileName);
//
//            values.put(MediaStore.Images.Media.DESCRIPTION, "Image capture for Customer and Vehicle");
//
//            /****** imageUri is the current activity attribute, define and save it for later usage  *****/
//            imageUri = getContentResolver().insert(
//                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

            if (judgeToggleBtn) {
                // image from gallery

//                Intent intent = new Intent(Intent.ACTION_PICK,
//                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                startActivityForResult(intent, MY_REQUEST_GALLERY);

                int findCount = 0;
                for (int i = 0; i < imagesArray.length; i++) {
                    if (imagesArray[i] == null)
                        findCount++;
                }


//                Intent intent = new Intent(this, AlbumSelectActivity.class);
//                //set limit on number of images that can be selected, default is 10
//                intent.putExtra(Constants.INTENT_EXTRA_LIMIT, findCount);
//                startActivityForResult(intent, Constants.REQUEST_CODE);

                Intent intent = new Intent(getApplicationContext(), ImageSelectActivity.class);
                intent.putExtra(Constants.INTENT_EXTRA_LIMIT, findCount);
                intent.putExtra(Constants.INTENT_EXTRA_ALBUM, "Camera");

                startActivityForResult(intent, Constants.REQUEST_CODE);


            } else {
                // image from camera

                if(!isCameraOpen) {
                    Constant.isParentFolderExist(new File(Environment.getExternalStorageDirectory() + "/" + Constant.FOLDER_NAME));
                    File file = new File(Environment.getExternalStorageDirectory() + "/" + Constant.FOLDER_NAME, String.format("%d", System.currentTimeMillis()));
                    Uri imageUri;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        imageUri = FileProvider.getUriForFile(CampaignsDataActivity.this, getPackageName() + ".provider", file);
                    } else {
                        imageUri = Uri.fromFile(file);
                    }

                    savePreferencePath(file.getAbsolutePath());
                    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    cameraIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    cameraIntent.putExtra("phototitle", text);
                    cameraIntent.putExtra("Position", position);
                    cameraIntent.putExtra("editphoto", editPhoto);
                    grantUriPermission(getPackageName(), imageUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                    if (cameraIntent.resolveActivity(getPackageManager()) != null) {
                        //campaignsDataAdapter.setClicked(false);
                        FillPhotoList();
                        isCameraOpen = true;
                        startActivityForResult(cameraIntent, MY_REQUEST_CAMERA);
                    }
                }
            }

            currentPosition = position;
        }
    }

// use of nonNullElemExist;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (campaignsDataAdapter != null)
            campaignsDataAdapter.setClicked(true);
        switch (resultCode) {

            case RESULT_OK:

                // ... Check for some data from the intent
                if (requestCode == MY_REQUEST_CAMERA) {
                    // .. lets toast again
                    removeLastAddedFile();
                    int position = -1;
                    String imagePath = "";
                    if (data == null) {
                        String tempoPath = getImagePath();
                        if (!tempoPath.isEmpty()) {
                            File file = new File(tempoPath);
                            imagePath = file.getAbsolutePath();
                            position = currentPosition;

                            Log.e("Image URI", "" + file.getAbsolutePath());
                            if (imagePath.equals("") || imagePath == null) {
                                imagesArray[position] = null;
                            } else {
                                imagesArray[position] = imagePath; //imageUri;
                            }
                        }
                    } else {
                        Bundle extras = data.getExtras();
                        if(extras != null) {
                            Bitmap imageBitmap = (Bitmap) extras.get("data");
                            imagePath = FileUtils.saveBitmapToCameraWithDate(CampaignsDataActivity.this, imageBitmap, true, null);
                            position = currentPosition;

//                        position = data.getIntExtra("Position", 0);
//                        imagePath = data.getStringExtra("imagePath");
                            if (imagePath.equals("") || imagePath == null) {
                                imagesArray[position] = null;
                            } else {
                                imagesArray[position] = imagePath; //imageUri;
                            }
                        }
                        else{
                            imagesArray[position] = null;
                        }
                    }

                    if (position != -1) {
                        new LabelDataSave(imagePath, position, labalesArrayList.get(position).getName(), labalesArrayList.get(position).getTitle(), String.valueOf(campaingID), null).execute();
                               /* cameraImageList.setAdapter(new CampaignsDataAdapter(CampaignsDataActivity.this , imagesArray , minCount , maxCount , defaultCount , labalesArrayList, false , false));
                                String imageAsString = imageStringFormat(imagePath);
                                DataLabels dataLabels = new DataLabels(imageAsString,position, labalesArrayList.get(position).getName(), labalesArrayList.get(position).getTitle(), null, null);
                                dataLabels.saveToDb();*/
                        openGalleryAgain = false;
                        openCameraAgain = true;
                        // Toast.makeText(this, "Handled the result successfully at position " + position, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(this, "Failed to get data from intent", Toast.LENGTH_SHORT).show();
                    }
                }


                if (requestCode == MY_REQUEST_AGAIN || requestCode == MY_REQUEST_TO_CAMERA) {
                    int position = -1;
                    String imagePath = "";

                    if (requestCode == MY_REQUEST_AGAIN) {
                        campaignsDataAdapter.setClickPreference(getApplicationContext(), true);
                        isCameraOpen = false;
                        position = data.getIntExtra("Position", 0);
                        imagePath = data.getStringExtra("imagePath");
                        if (imagePath.equals("") || imagePath == null) {
                            imagesArray[position] = null;
                        } else {
                            imagesArray[position] = imagePath; //imageUri;
                        }
                    } else if (requestCode == MY_REQUEST_TO_CAMERA) {

                        String tempoPath = getImagePath();
                        if (!tempoPath.isEmpty()) {
                            File file = new File(tempoPath);
                            imagePath = file.getAbsolutePath();
                            position = currentPosition;

                            Log.e("Image URI", "" + tempoPath);
                            if (imagePath.equals("") || imagePath == null) {
                                imagesArray[position] = null;
                            } else {
                                imagesArray[position] = imagePath; //imageUri;
                                new LabelDataSave(imagePath, position, labalesArrayList.get(position).getName(), labalesArrayList.get(position).getTitle(), String.valueOf(campaingID), null).execute();
                                openGalleryAgain = false;
                                openCameraAgain = true;
                            }
                        }
                    }

                    if (position != -1) {
                        new LabelDataSave(imagePath, position, labalesArrayList.get(position).getName(), labalesArrayList.get(position).getTitle(), String.valueOf(campaingID), null).execute();
                    } else {
                        Toast.makeText(this, "Failed to get data from intent", Toast.LENGTH_SHORT).show();
                    }
                }

                // image from gallery
                if (requestCode == MY_REQUEST_GALLERY) {
                    // .. lets toast again
                    int position = -1;
                    String imagePath = "";
                    if (data != null) {

                        Uri selectedImage = data.getData();
                        imagePath = FileUtils.getRealPathFromURI(this, selectedImage);
                        try {
                            Bitmap imageBitmap = CampaignsDataAdapter.decodeFile(CampaignsDataActivity.this, imagePath, 600);
                            imageBitmap = NetworkChangeReceiver.rotatedBitmap(imageBitmap, imagePath);//BitmapFactory.decodeFile(imagePath);

                            imagePath = FileUtils.saveBitmapToCameraWithDate(CampaignsDataActivity.this, imageBitmap, true, null);

                            position = currentPosition;

//                        position = data.getIntExtra("Position", 0);
//                        imagePath = data.getStringExtra("imagePath");
                            if (imagePath.equals("") || imagePath == null) {
                                imagesArray[position] = null;
                            } else {
                                imagesArray[position] = imagePath; //imageUri;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    if (position != -1) {
                        new LabelDataSave(imagePath, position, labalesArrayList.get(position).getName(), labalesArrayList.get(position).getTitle(), String.valueOf(campaingID), null).execute();
                               /* cameraImageList.setAdapter(new CampaignsDataAdapter(CampaignsDataActivity.this , imagesArray , minCount , maxCount , defaultCount , labalesArrayList, false , false));
                                String imageAsString = imageStringFormat(imagePath);
                                DataLabels dataLabels = new DataLabels(imageAsString,position, labalesArrayList.get(position).getName(), labalesArrayList.get(position).getTitle(), null, null);
                                dataLabels.saveToDb();*/
                        openGalleryAgain = true;
                        openCameraAgain = false;


                        // Toast.makeText(this, "Handled the result successfully at position " + position, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(this, "Failed to get data from intent", Toast.LENGTH_SHORT).show();
                    }
                }

                if (requestCode == Constants.REQUEST_CODE) {
                    if (requestCode == Constants.REQUEST_CODE && resultCode == RESULT_OK && data != null) {
                        //The array list has the image paths of the selected images
                        ArrayList<Image> images = data.getParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES);
                        new LableDataSaveMultiSelect(String.valueOf(campaingID), null, images).execute();
                    }

                }

                break;


            case RESULT_CANCELED:
                isCameraOpen = false;
                campaignsDataAdapter.setClickPreference(getApplicationContext(), true);
                // ... Handle this situation
                break;
        }
    }

    public ArrayList<String> GalleryList = new ArrayList<String>();
    private void FillPhotoList()
    {
        // initialize the list!
        GalleryList.clear();
        // intialize the Uri and the Cursor, and the current expected size.
        Cursor cursor = null;
        Uri u = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        //
        // Query the Uri to get the data path.  Only if the Uri is valid.
        if (u != null)
        {
            cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection,
                    MediaStore.Images.Media.BUCKET_DISPLAY_NAME + " =?", new String[]{ "Camera" }, MediaStore.Images.Media.DATE_ADDED);
        }

        // If we found the cursor and found a record in it (we also have the id).
        if ((cursor != null) && (cursor.moveToFirst()))
        {
            do
            {
                String name = cursor.getString(cursor.getColumnIndex(projection[1]));
                // Loop each and add to the list.
                GalleryList.add(name);
            }
            while (cursor.moveToNext());
        }
    }

    private final String[] projection = new String[]{ MediaStore.Images.Media._ID, MediaStore.Images.Media.DISPLAY_NAME, MediaStore.Images.Media.DATA };
    private void removeLastAddedFile() {

        // intialize the Uri and the Cursor, and the current expected size.
        Cursor cursor = null;
        Uri u = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        //
        // Query the Uri to get the data path.  Only if the Uri is valid.
        if (u != null)
        {

            cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection,
                    MediaStore.Images.Media.BUCKET_DISPLAY_NAME + " =?", new String[]{ "Camera" }, MediaStore.Images.Media.DATE_ADDED);

        }

        if ((cursor != null) && (cursor.moveToFirst()))
        {
            do
            {
                String name = cursor.getString(cursor.getColumnIndex(projection[1]));
                String path = cursor.getString(cursor.getColumnIndex(projection[2]));
                if(GalleryList.contains(name)) {
                    // Loop each and add to the list.


                    File file = new File(path);
                    file.delete();
                    break;
                }
            }
            while (cursor.moveToNext());
        }
    }



       /*Runtime Permission Code Start Here*/

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(CampaignsDataActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(CampaignsDataActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(CampaignsDataActivity.this, "Write External Storage permission allows us to do store images. Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(CampaignsDataActivity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("value", "Permission Granted, Now you can save image .");
                } else {
                    Log.e("value", "Permission Denied, You cannot save image.");
                }
                break;
        }
    }

    public boolean isOnline(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        //should check null because in airplane mode it will be null
        return (netInfo != null && netInfo.isConnected());

    }

    public class LabelDataSave extends AsyncTask<String, String, String> {

        DataLabels dataLabels;
        String image;
        int imageSequence;
        String name;
        String title;
        String campaignOwner;
        String labelPreference;


        public LabelDataSave(String _image,
                             int _imageSequence,
                             String _name,
                             String _title,
                             String _campaignOwner,
                             String _labelPreference) {

            isCameraOpen = true;
            this.image = _image;
            this.imageSequence = _imageSequence;
            this.name = _name;
            this.title = _title;
            this.campaignOwner = _campaignOwner;
            this.labelPreference = _labelPreference;

        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... strings) {

            // String imageAsString = imageStringFormat(image);
            dataLabels = new DataLabels(image, imageSequence, name, title, campaignOwner, labelPreference);
            dataLabels.saveToDb();

            return null;
        }

        @Override
        protected void onPostExecute(String unused) {

            campaignsDataAdapter.updateArray(imagesArray);
            //cameraImageList.setAdapter(new CampaignsDataAdapter(CampaignsDataActivity.this, imagesArray, minCount, maxCount, defaultCount, labalesArrayList, false, false));

//            openCameraAgain = false;
            if (openCameraAgain) {
                openCameraAgain = false;
                final int newposition = currentPosition + 1;

                CountDownTimer timer = new CountDownTimer(1500, 1000) {

                    public void onTick(long millisUntilFinished) {

                    }

                    public void onFinish() {
                        if (newposition < defaultCount && imagesArray[newposition] == null) {

//                                    Intent intent = new Intent(CampaignsDataActivity.this, CameraActivity.class);
//                                    intent.putExtra("phototitle", labalesArrayList.get(newposition).getTitle());
//                                    intent.putExtra("Position", newposition);
//                                    startActivityForResult(intent, MY_REQUEST_TO_CAMERA);

                            Constant.isParentFolderExist(new File(Environment.getExternalStorageDirectory() + "/" + Constant.FOLDER_NAME));
                            File file = new File(Environment.getExternalStorageDirectory() + "/" + Constant.FOLDER_NAME, String.format("%d", System.currentTimeMillis()));
                            Uri imageUri;
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                imageUri = FileProvider.getUriForFile(CampaignsDataActivity.this, getPackageName() + ".provider", file);
                            } else {
                                imageUri = Uri.fromFile(file);
                            }
                            savePreferencePath(file.getAbsolutePath());
                            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            cameraIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            cameraIntent.putExtra("phototitle", labalesArrayList.get(newposition).getTitle());
                            cameraIntent.putExtra("Position", newposition);

                            currentPosition = newposition;
                            if (cameraIntent.resolveActivity(getPackageManager()) != null) {
                                grantUriPermission(getPackageName(), imageUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                                campaignsDataAdapter.setClicked(false);
                                FillPhotoList();
                                isCameraOpen = true;
                                startActivityForResult(cameraIntent, MY_REQUEST_CAMERA);
                            }
                        }
                        else
                            isCameraOpen = false;
                    }
                };
                timer.start();
            } else if (openGalleryAgain) {
                openGalleryAgain = false;
                final int newposition = currentPosition + 1;

                CountDownTimer timer = new CountDownTimer(1000, 1000) {

                    public void onTick(long millisUntilFinished) {

                    }

                    public void onFinish() {
                        if (newposition < defaultCount && imagesArray[newposition] == null) {

                            Intent intent = new Intent(Intent.ACTION_PICK,
                                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                            currentPosition = newposition;
                            if (intent.resolveActivity(getPackageManager()) != null) {
                                startActivityForResult(intent, MY_REQUEST_GALLERY);
                            }
                        }
                    }
                };
                timer.start();
            }
            else
                isCameraOpen = false;


        }
    }


    public class LableDataSaveMultiSelect extends AsyncTask<String, String, String> {

        DataLabels dataLabels;
        String campaignOwner;
        String labelPreference;
        ArrayList<Image> images;

        public LableDataSaveMultiSelect(
                String _campaignOwner,
                String _labelPreference,
                ArrayList<Image> images) {

            this.images = images;
            this.campaignOwner = _campaignOwner;
            this.labelPreference = _labelPreference;

        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... strings) {

            for (int i = 0, imageCounts = 0; i < imagesArray.length; i++) {
                if (imagesArray[i] == null && imageCounts < images.size()) {
                    String imagePath = images.get(imageCounts).path;
                    try {
                        Bitmap imageBitmap = CampaignsDataAdapter.decodeFile(CampaignsDataActivity.this, imagePath, 1000);
                        imageBitmap = NetworkChangeReceiver.rotatedBitmap(imageBitmap, imagePath);//BitmapFactory.decodeFile(imagePath);
                        imagePath = FileUtils.saveBitmapToCameraWithDate(CampaignsDataActivity.this, imageBitmap, true, null);

                        if (imagePath.equals("") || imagePath == null) {
                            imagesArray[i] = null;
                        } else {
                            imagesArray[i] = imagePath; //imageUri;
                            // String imageAsString = imageStringFormat(image);
                            dataLabels = new DataLabels(imagePath, i, labalesArrayList.get(i).getName(), labalesArrayList.get(i).getTitle(), campaignOwner, labelPreference);
                            dataLabels.saveToDb();
                            imageCounts++;
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }


            return null;
        }

        @Override
        protected void onPostExecute(String unused) {

            campaignsDataAdapter.updateArray(imagesArray);
//            cameraImageList.setAdapter(new CampaignsDataAdapter(CampaignsDataActivity.this, imagesArray, minCount, maxCount, defaultCount, labalesArrayList, false, false));
            openGalleryAgain = false;
            final int newposition = currentPosition + 1;


        }
    }

    public boolean openCameraAgain = false;
    public boolean openGalleryAgain = false;

}
