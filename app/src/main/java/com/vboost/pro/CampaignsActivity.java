package com.vboost.pro;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;


import com.bigkoo.alertview.AlertView;
import com.bigkoo.alertview.OnDismissListener;
import com.bigkoo.alertview.OnItemClickListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;
import com.vboost.pro.Adapter.CampaignsAdapter;
import com.vboost.pro.DBClasses.Campaign;
import com.vboost.pro.DBClasses.Company;
import com.vboost.pro.DBClasses.Contact;
import com.vboost.pro.Util.Constant;
import com.vboost.pro.Util.SharedPreferencesHelper;
import com.vboost.pro.event.PhotoUploadRequestEvent;
import com.vboost.pro.model.Campaigns;
import com.vboost.pro.model.Images;
import com.vboost.pro.model.ManageContacts;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Nabeel Hafeez on 7/27/2016.
 */
public class CampaignsActivity extends AppCompatActivity implements OnItemClickListener, OnDismissListener {
    private SharedPreferences sharedpreferences;
    private SharedPreferences.Editor editor;

    public static LinearLayout liUserPicker;
    public static TextView tvCancel;
    public static TextView tvDone;
    public static NumberPicker numberPicker1;

    public String selectedProspects;

    String[] contactsName;
    ArrayList<ManageContacts> contactsList;
    ArrayList<Campaigns> prospectCampaings;

    Campaigns currentCampain;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_campaigns);

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.retry_alert_new);
        dialog.findViewById(R.id.a).setVisibility(View.GONE);
//        ((TextView) dialog.findViewById(R.id.text_dialog)).setText(Html.fromHtml(getString(R.string.txt_error_submit_images)));
        Button dialogButton = (Button) dialog.findViewById(R.id.retry_button_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        // dialog.show();

        sharedpreferences = getSharedPreferences(Constant.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = getSharedPreferences(Constant.MyPREFERENCES, MODE_PRIVATE).edit();

        liUserPicker = (LinearLayout) findViewById(R.id.liUserPicker);
        tvCancel = (TextView) findViewById(R.id.tvCancel);
        tvDone = (TextView) findViewById(R.id.tvDone);
        numberPicker1 = (NumberPicker) findViewById(R.id.numberPicker1);


        // todo consideration
        List<Company> company = Company.getAll();

        ImageView companyLogo = (ImageView) findViewById(R.id.campaign_company_logo);

        Picasso.with(this)
                .load(company.get(0).getLogo())
                .error(R.drawable.logo2)
                .into(companyLogo);


        ArrayList<Campaigns> campaignsArray = new ArrayList<>();
        int len = campaignsArray.size();
        boolean judgeProspect;
        final String[] prospectNames;
        if (getIntent() != null && getIntent().hasExtra("createNew") && getIntent().getBooleanExtra("createNew", false)) {
            List<Campaign> campaigns  = Campaign.getAll();
            String value= SharedPreferencesHelper.getInstance(CampaignsActivity.this).getString("data", "");
            if(!TextUtils.isEmpty(value)) {
                campaignsArray = new Gson().fromJson(value, new TypeToken<ArrayList<Campaigns>>() {
                }.getType());
            }
            else {
                for (int i = 0; i < campaigns.size(); i++) {
                    Campaign campaign = campaigns.get(i);
                    Campaigns cam = new Campaigns(campaign.getCampaign_Color(), campaign.getCampaign_Details(), campaign.getCampaignID(), campaign.getCompany(), campaign.getDefault_count(), campaign.getKey(), campaign.getLogo(), campaign.getMax_count(), campaign.getMin_count(), campaign.getName());
                    campaignsArray.add(cam);
                }
            }

        } else {

            campaignsArray = (ArrayList<Campaigns>) getIntent().getSerializableExtra("campaignArray");


        }
        contactsList = (ArrayList<ManageContacts>) getIntent().getSerializableExtra("contacts");
        contactsName = getIntent().getStringArrayExtra("contactsName");
        len = campaignsArray.size();
        prospectCampaings = new ArrayList<Campaigns>(campaignsArray);

        if (len > 1) {
            if (campaignsArray.get(0).getName().contains("Prospect") || campaignsArray.get(1).getName().contains("Prospect")) {
                judgeProspect = true;

                prospectNames = new String[len - 1];

//                prospectCampaings = campaignsArray;
                campaignsArray.clear();
                campaignsArray.add(prospectCampaings.get(0));

                Campaigns campaigns = prospectCampaings.get(1);
                campaignsArray.add(campaigns);

                int lenOfName;
                for (int i = 1; i < len; i++) {
                    lenOfName = prospectCampaings.get(i).getName().length();
                    prospectNames[i - 1] = prospectCampaings.get(i).getName().substring(11, lenOfName);
                }

                numberPicker1.setMinValue(0);
                numberPicker1.setMaxValue(prospectNames.length - 1);
                numberPicker1.setDisplayedValues(prospectNames);
                numberPicker1.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
            } else {
                judgeProspect = false;
                prospectNames = new String[1];
                prospectNames[0] = "";
            }
        } else {
            judgeProspect = false;

            prospectNames = new String[1];
            prospectNames[0] = "";
        }

        editor.putBoolean(Constant.JudgeProspect, judgeProspect);
        editor.commit();

        CampaignsAdapter campaignsAdapter = new CampaignsAdapter(this, campaignsArray, contactsList,
                contactsName, true, judgeProspect, prospectNames, prospectCampaings);

        ListView listViewCampaigns = (ListView) findViewById(R.id.listView_campaignc);
        listViewCampaigns.setAdapter(campaignsAdapter);


        // -----------------------------------------------------------------------------------------


        selectedProspects = prospectNames[0];
        currentCampain = prospectCampaings.get(1);
        NumberPicker.OnValueChangeListener myValChangedListener = new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                selectedProspects = prospectNames[newVal];
                currentCampain = prospectCampaings.get(newVal + 1);
                int p = oldVal;
            }
        };

        numberPicker1.setOnValueChangedListener(myValChangedListener);

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                liUserPicker.setVisibility(View.INVISIBLE);
            }
        });

        tvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                liUserPicker.setVisibility(View.INVISIBLE);

                Intent campaignsDataIntent = new Intent(CampaignsActivity.this, CampaignsDataActivity.class);
                campaignsDataIntent.putExtra("contactsName", contactsName);
                campaignsDataIntent.putExtra("campaignrow", currentCampain);
                campaignsDataIntent.putExtra("contacts", contactsList);
                campaignsDataIntent.putExtra("campaignArray", prospectCampaings);
                campaignsDataIntent.putExtra("selectedProspector", selectedProspects);

                startActivity(campaignsDataIntent);
            }
        });
    }

    public void alertMenuCampaign(View view) {
        new AlertView(null, null, null, null,
                new String[]{"Manage Contacts",
                        "Switch Companies",
                        "Get Help",
                },
                this, AlertView.Style.Alert, this).setCancelable(true).setOnDismissListener(this).show();
    }

    @Override
    public void onItemClick(Object o, int position) {

        if (position == 0) {

            Intent contactIntent = new Intent(CampaignsActivity.this, ManageContatcsAvtivity.class);
            startActivity(contactIntent);
            finish();

        } else if (position == 1) {

            Intent logoutIntent = new Intent(CampaignsActivity.this, LogOutActivity.class);
            startActivity(logoutIntent);

        } else if (position == 2) {

            Intent helpIntent = new Intent(CampaignsActivity.this, GetHelpActivity.class);
            startActivity(helpIntent);
        }


    }

    @Override
    public void onDismiss(Object o) {


    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
//            if(mAlertView!=null && mAlertView.isShowing()){
//                mAlertView.dismiss();
//                return false;
//            }
        }

        return super.onKeyDown(keyCode, event);

    }


}
