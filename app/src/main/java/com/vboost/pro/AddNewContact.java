package com.vboost.pro;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.bigkoo.alertview.AlertView;
import com.bigkoo.alertview.OnDismissListener;
import com.bigkoo.alertview.OnItemClickListener;
import com.vboost.pro.DBClasses.Company;
import com.vboost.pro.Util.Constant;
import com.vboost.pro.model.ManageContacts;
import com.squareup.picasso.Picasso;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Nabeel Hafeez on 8/15/2016.
 */
public class AddNewContact extends AppCompatActivity implements OnItemClickListener, OnDismissListener {

    private final String USER_AGENT = "Mozilla/5.0";
    private ProgressDialog progress;

    Button saveContact;
    EditText nameEditText;
    EditText emailEditText;
    EditText phoneEditText;
    AlertView saveAlert;
    AlertView menuAlert;

    String emailPattern;
    int keyDel;

    String name;
    String email;
    String phone;

    ArrayList<ManageContacts> contactsList;
    String[] contactsName;


    public static final String MyPREFERENCES = "MyPrefs";
    public static final String Token = "tokenKey";
    SharedPreferences sharedpreferences;

    String userCredentials;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_contact);

        List<Company> company = Company.getAll();

        ImageView companyLogo = (ImageView) findViewById(R.id.add_new_contact__company_logo);

        Picasso.with(this)
                .load(company.get(0).getLogo())
                .placeholder(R.drawable.logo2)
                .error(R.drawable.logo2)
                .into(companyLogo);

        contactsName = getIntent().getStringArrayExtra("contactsName");
        contactsList = (ArrayList<ManageContacts>) getIntent().getSerializableExtra("contacts");

        saveContact = (Button) findViewById(R.id.new_conatct_save_button);
        saveContact.setAllCaps(false);
        saveContact.getBackground().setColorFilter(new LightingColorFilter(Color.parseColor("#FBA919"), 0x00000000));

        nameEditText = (EditText) findViewById(R.id.add_conatct_name_edit_text);
        nameEditText.setImeOptions(EditorInfo.IME_ACTION_NEXT);

        nameEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS);
        nameEditText.setSingleLine(true);
        nameEditText.setAllCaps(false);
        emailEditText = (EditText) findViewById(R.id.add_conatct_email_edit_text);
        emailEditText.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        emailEditText.setSingleLine(true);
        emailEditText.setAllCaps(false);
        phoneEditText = (EditText) findViewById(R.id.add_conatct_phone_edit_text);
        phoneEditText.setImeOptions(EditorInfo.IME_ACTION_DONE);
        phoneEditText.setSingleLine(true);
        phoneEditText.setAllCaps(false);

        emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        saveAlert = new AlertView("Contact added.", null, null, null, new String[]{"Done"}, this, AlertView.Style.Alert, this);

        menuAlert = new AlertView(null, null, null, null,
                new String[]{"Change Customer Type",
                        "Manage Contacts",
                        "Switch Companies",
                        "Get Help",
                },
                this, AlertView.Style.Alert, this).setCancelable(true).setOnDismissListener(this);


        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        userCredentials = sharedpreferences.getString(Token, null);

        phoneEditText.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {

                boolean flag = true;
                String eachBlock[] = phoneEditText.getText().toString().split("-");
                for (int i = 0; i < eachBlock.length; i++) {
                    if (eachBlock[i].length() > 3) {
                        Log.v("11111111111111111111", "cc" + flag + eachBlock[i].length());
                    }
                }
                if (flag) {
                    phoneEditText.setOnKeyListener(new View.OnKeyListener() {

                        public boolean onKey(View v, int keyCode, KeyEvent event) {

                            if (keyCode == KeyEvent.KEYCODE_DEL)
                                keyDel = 1;
                            return false;
                        }
                    });

                    if (keyDel == 0) {

                        if (((phoneEditText.getText().length() + 1) % 4) == 0) {
                            if (phoneEditText.getText().toString().split("-").length <= 2) {
                                phoneEditText.setText(phoneEditText.getText() + "-");
                                phoneEditText.setSelection(phoneEditText.getText().length());
                            }
                        }
                        phone = phoneEditText.getText().toString();
                    } else {
                        phone = phoneEditText.getText().toString();
                        keyDel = 0;
                    }

                } else {
                    phoneEditText.setText(phone);
                }

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            public void afterTextChanged(Editable s) {


            }


        });
    }

    public void saveContactButton(View view) {
        if (nameEditText != null && emailEditText != null && phoneEditText != null) {

            name = nameEditText.getText().toString();
            email = emailEditText.getText().toString();
            phone = phoneEditText.getText().toString();

            if (email.matches(emailPattern)) {

                if (phone.length() < 12) {
                    new AlertView(null, "Please Enter 10 digit phone number!", null, null, new String[]{"OK"}, this, AlertView.Style.Alert, this).show();
                } else {
                    new SaveContact(this).execute();
                }
            } else {
                new AlertView(null, "Please enter valid email address!", null, null, new String[]{"OK"}, this, AlertView.Style.Alert, this).show();
            }

        } else {
            if (name.equals("") || name.equals(null) || nameEditText == null) {

                new AlertView(null, "Please enter your first and last name.", null, null, new String[]{"OK"}, this, AlertView.Style.Alert, this).show();

            } else if (email.equals("") || email.equals(null) || emailEditText == null) {

                new AlertView(null, "Please enter a valid email address.", null, null, new String[]{"OK"}, this, AlertView.Style.Alert, this).show();

            } else if (phone.equals("") || phone.equals(null) || phoneEditText == null) {

                new AlertView(null, "Please enter your cell phone number.", null, null, new String[]{"OK"}, this, AlertView.Style.Alert, this).show();

            }
        }
    }

    public void closeActivity(View view) {

        finish();

    }

    public void alertMenu(View view) {
        menuAlert.show();
    }


    @Override
    public void onItemClick(Object o, int position) {

        if (o == saveAlert && position != AlertView.CANCELPOSITION) {
            setResult(RESULT_OK);
            finish();
        } else if (o == menuAlert && position != AlertView.CANCELPOSITION) {
            if (position == 0) {

                Intent contactIntent = new Intent(AddNewContact.this, CampaignsActivity.class);
                contactIntent.putExtra("contactsName", contactsName);
                contactIntent.putExtra("contacts", contactsList);
                startActivity(contactIntent);

            } else if (position == 1) {

                Intent logoutIntent = new Intent(AddNewContact.this, ManageContatcsAvtivity.class);
                startActivity(logoutIntent);

            } else if (position == 2) {

                Intent logoutIntent = new Intent(AddNewContact.this, LogOutActivity.class);
                startActivity(logoutIntent);

            } else if (position == 3) {

                Intent helpIntent = new Intent(AddNewContact.this, GetHelpActivity.class);
                startActivity(helpIntent);
            }
        }

    }

    @Override
    public void onDismiss(Object o) {

    }

    private class SaveContact extends AsyncTask<String, Void, Void> {

        private final Context context;

        public SaveContact(Context c) {
            this.context = c;
        }

        protected void onPreExecute() {
            progress = new ProgressDialog(this.context);
            progress.setMessage("Loading");
            progress.show();
        }

        @Override
        protected Void doInBackground(String... params) {


            try {


                URL url = new URL(Constant.SERVER_URL+"contacts/");

                HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                // userCredentials = "96ea502e088bc46dd3e863f1c82db3e160a4a56a";
                byte[] encodedBytes = Base64.encode(userCredentials.getBytes(), 0);
                String tokenAuth = "Token " + userCredentials;/*new String(encodedBytes);*/
                connection.setRequestProperty("Authorization", tokenAuth);

                //add reuqest header
                connection.setRequestMethod("POST");
                connection.setRequestProperty("User-Agent", USER_AGENT);
                connection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

                String urlParameters = "name=" + name + "&email=" + email + "&phone=" + phone;

                // Send post request
                connection.setDoOutput(true);
                DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
                wr.writeBytes(urlParameters);
                wr.flush();
                wr.close();

                final int responseCode = connection.getResponseCode();
                System.out.println("\nSending 'POST' request to URL : " + url);
                System.out.println("Post parameters : " + urlParameters);
                System.out.println("Response Code : " + responseCode);

                BufferedReader in = new BufferedReader(
                        new InputStreamReader(connection.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                //print result
                System.out.println(response.toString());

                AddNewContact.this.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {

                        if (responseCode == 200 || responseCode == 201) {
                            saveAlert.show();

                        } else {

                            finish();


                        }
                        progress.dismiss();


                    }
                });

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute() {
            //progress.dismiss();
        }

    }


}
