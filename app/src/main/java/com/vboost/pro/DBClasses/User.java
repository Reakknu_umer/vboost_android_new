package com.vboost.pro.DBClasses;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.vboost.pro.VboostDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nabeel Hafeez on 9/2/2016.
 */
public class User extends VboostDatabase {

    private String authToken;
    private String password;
    private String username;
    private String userCampaigns;
    private String userCompany;
    private String userContacts;
    private String userPreference;

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserCampaigns() {
        return userCampaigns;
    }

    public void setUserCampaigns(String userCampaigns) {
        this.userCampaigns = userCampaigns;
    }

    public String getUserCompany() {
        return userCompany;
    }

    public void setUserCompany(String userCompany) {
        this.userCompany = userCompany;
    }

    public String getUserContacts() {
        return userContacts;
    }

    public void setUserContacts(String userContacts) {
        this.userContacts = userContacts;
    }

    public String getUserPreference() {
        return userPreference;
    }

    public void setUserPreference(String userPreference) {
        this.userPreference = userPreference;
    }

    public User(){

    }

    public User(String _authToken,
            String _password,
            String _username,
            String _userCampaigns,
            String _userCompany,
            String _userContacts,
            String _userPreference) {

        this.setAuthToken(_authToken);
        this.setPassword(_password);
        this.setUsername(_username);
        this.setUserCampaigns(_userCampaigns);
        this.setUserCompany(_userCompany);
        this.setUserContacts(_userContacts);
        this.setUserPreference(_userPreference);

    }


    private static User eventFromStatement(Cursor cursor) {
        if (cursor != null) {
            try {
                if (cursor.getCount() == 0) {
                    return null;
                }

                String authToken = cursor.getString(0);
                String password = cursor.getString(1);
                String username = cursor.getString(2);
                String userCampaigns = cursor.getString(3);
                String userCompany = cursor.getString(4);
                String userContacts = cursor.getString(5);
                String userPreference = cursor.getString(6);
                User user = new User(authToken, password, username, userCampaigns, userCompany, userContacts, userPreference);
                return user;
            } catch (Exception e) {
                Log.e("TAG", "Error in cursor memory allocation: " + e.toString());
                return null;
            }
        } else {
            return null;
        }
    }

    public static List<User> getAll(Context context) {
        String selectQuery = "SELECT authToken, password, username, userCampaigns, userCompany, userContacts, userPreference FROM User";
        Log.d("SELECT", selectQuery);
        Cursor c = selectDataForQuery(selectQuery);

        ArrayList<User> users = new ArrayList<User>();
        User user;
        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    user = eventFromStatement(c);
                    users.add(user);
                } while (c.moveToNext());
            }
            c.close();
        }
        return users;
    }

    private static List<User> findAllByCursor(Cursor c) {
        ArrayList<User> users = new ArrayList<User>();
        User user;
        if (c != null) {

            if (c.moveToFirst()) {
                do {
                    user = eventFromStatement(c);
                    users.add(user);
                } while (c.moveToNext());
            }
            c.close();
        }
        return users;
    }

    public static User findByUsername(String UserName) {

        String selectQuery = "SELECT authToken, password, username, userCampaigns, userCompany, userContacts, userPreference FROM User WHERE username =" + UserName;
        Log.d("SELECT", selectQuery);
        Cursor c = selectDataForQuery(selectQuery);
        if (c != null) {
            if (c.getCount() > 0) {
                User user = eventFromStatement(c);
                c.close();
                return user;
            }
            c.close();
        }
        return null;
    }

    /*public static Blog findByBlogId(int blogId) {
        String selectQuery = "SELECT id, backend_id, title, content, thumb_image, permalink, excerpt, date FROM Blogs WHERE id = " + blogId;
        Log.d("SELECT", selectQuery);
        Cursor c = selectDataForQuery(selectQuery);
        if (c != null) {
            if (c.getCount() > 0) {
                Blog blog = eventFromStatement(c);
                c.close();
                return blog;
            }
            c.close();
        }
        return null;
    }*/

    private static final String table = "User";

    public boolean saveToDb(Context context) {

                ContentValues values = new ContentValues();
                values.put("authToken", this.authToken);
                values.put("password", this.password);
                values.put("username", this.username);
                values.put("userCampaigns", this.userCampaigns);
                values.put("userCompany", this.userCompany);
                values.put("userContacts", this.userContacts);
                values.put("userPreference", this.userPreference);

                return (insertDataForQuery(table, values) != -1);

        /*User user = this.findByUsername("'"+this.username+"'");
        if (user == null) {
            ContentValues values = new ContentValues();
            values.put("authToken", this.authToken);
            values.put("password", this.password);
            values.put("username", this.username);
            values.put("userCampaigns", this.userCampaigns);
            values.put("userCompany", this.userCompany);
            values.put("userContacts", this.userContacts);
            values.put("userPreference", this.userPreference);

            return (insertDataForQuery(table, values) != -1);

        } else {
            if (!this.equals(user)) {
                String whereClause = "username = " + "'"+this.username+"'";
                ContentValues values = new ContentValues();
                values.put("authToken", this.authToken);
                values.put("password", this.password);
                values.put("username", this.username);
                values.put("userCampaigns", this.userCampaigns);
                values.put("userCompany", this.userCompany);
                values.put("userContacts", this.userContacts);
                values.put("userPreference", this.userPreference);

                return (updateDataForQuery(table, values, whereClause) != -1);
            } else {
                return true;
            }
        }*/

    }

    public static void destroy() {
        sqLiteOpenHelper.getWritableDatabase().execSQL(String.format("DELETE FROM User"));
    }
}
