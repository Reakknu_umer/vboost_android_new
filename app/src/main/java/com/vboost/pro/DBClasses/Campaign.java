package com.vboost.pro.DBClasses;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;
import android.util.Log;

import com.darsh.multipleimageselect.models.Image;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vboost.pro.VboostDatabase;
import com.vboost.pro.model.Images;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nabeel Hafeez on 9/2/2016.
 */
public class Campaign extends VboostDatabase {

    private String campaign_Color;
    private String campaign_Details;
    private int campaignID;
    private int company;
    private int default_count;
    private Images images;
    private String key;
    private String logo;
    private int max_count;
    private int min_count;
    private String name;
    private String campaignLabel;
    private String campaignOwner;
    private String campaignPreferences;

    public String getCampaign_Color() {
        return campaign_Color;
    }

    public void setCampaign_Color(String campaign_Color) {
        this.campaign_Color = campaign_Color;
    }

    public String getCampaign_Details() {
        return campaign_Details;
    }

    public void setCampaign_Details(String campaign_Details) {
        this.campaign_Details = campaign_Details;
    }

    public int getCampaignID() {
        return campaignID;
    }

    public void setCampaignID(int campaignID) {
        this.campaignID = campaignID;
    }

    public int getCompany() {
        return company;
    }

    public void setCompany(int company) {
        this.company = company;
    }

    public int getDefault_count() {
        return default_count;
    }

    public void setDefault_count(int default_count) {
        this.default_count = default_count;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public int getMax_count() {
        return max_count;
    }

    public void setMax_count(int max_count) {
        this.max_count = max_count;
    }

    public int getMin_count() {
        return min_count;
    }

    public void setMin_count(int min_count) {
        this.min_count = min_count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCampaignLabel() {
        return campaignLabel;
    }

    public void setCampaignLabel(String campaignLabel) {
        this.campaignLabel = campaignLabel;
    }

    public String getCampaignOwner() {
        return campaignOwner;
    }

    public void setCampaignOwner(String campaignOwner) {
        this.campaignOwner = campaignOwner;
    }

    public String getCampaignPreferences() {
        return campaignPreferences;
    }

    public void setCampaignPreferences(String campaignPreferences) {
        this.campaignPreferences = campaignPreferences;
    }

    public Campaign(){

    }

    public Campaign(String _campaign_Color,
                    String _campaign_Details,
                    int _campaignID,
                    int _company,
                    int _default_count,
                    String _key,
                    Images images,
                    String _logo,
                    int _max_count,
                    int _min_count,
                    String _name,
                    String _campaignLabel,
                    String _campaignOwner,
                    String _campaignPreferences) {

        this.images = images;
        this.setCampaign_Color(_campaign_Color);
        this.setCampaign_Details(_campaign_Details);
        this.setCampaignID(_campaignID);
        this.setCompany(_company);
        this.setDefault_count(_default_count);
        this.setKey(_key);
        this.setLogo(_logo);
        this.setMax_count(_max_count);
        this.setMin_count(_min_count);
        this.setName(_name);
        this.setCampaignLabel(_campaignLabel);
        this.setCampaignOwner(_campaignOwner);
        this.setCampaignPreferences(_campaignPreferences);

    }

    private static Campaign eventFromStatement(Cursor cursor) {
        if (cursor != null) {
            try {
                if (cursor.getCount() == 0) {
                    return null;
                }

                String campaign_Color = cursor.getString(0);
                String campaign_Details = cursor.getString(1);
                int campaignID = cursor.getInt(2);
                int company = cursor.getInt(3);
                int default_count = cursor.getInt(4);
                String key = cursor.getString(5);
                String logo = cursor.getString(6);
                int max_count = cursor.getInt(7);
                int min_count = cursor.getInt(8);
                String name = cursor.getString(9);
                String campaignLabel = cursor.getString(10);
                String campaignOwner = cursor.getString(11);
                String campaignPreferences = cursor.getString(12);
                Images images = null;
//                if(!TextUtils.isEmpty(cursor.getString(13))) {
//                    images = new Gson().fromJson(cursor.getString(13), new TypeToken<ArrayList<Images>>() {
//                    }.getType());
//                }
                Campaign campaign = new Campaign(campaign_Color,campaign_Details,campaignID,company,default_count,key,images ,logo,max_count,min_count,
                        name,campaignLabel,campaignOwner,campaignPreferences);
                return campaign;
            } catch (Exception e) {
                Log.e("TAG","Error in cursor memory allocation: " + e.toString());
                return null;
            }
        } else {
            return null;
        }
    }

    public static List<Campaign> getAll() {
        String selectQuery = "SELECT campaign_Color, campaign_Details, campaignID, company, default_count, key, logo, max_count, min_count, name, campaignLabel, campaignOwner, campaignPreferences FROM Campaign";
        Log.d("SELECT",selectQuery);
        Cursor c = selectDataForQuery(selectQuery);

        ArrayList<Campaign> campaigns = new ArrayList<Campaign>();
        Campaign campaign;
        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    campaign = eventFromStatement(c);
                    campaigns.add(campaign);
                } while (c.moveToNext());
            }
            c.close();
        }
        return campaigns;
    }

    private static List<Campaign> findAllByCursor(Cursor c) {
        ArrayList<Campaign> campaigns = new ArrayList<Campaign>();
        Campaign campaign;
        if (c != null) {

            if (c.moveToFirst()) {
                do {
                    campaign = eventFromStatement(c);
                    campaigns.add(campaign);
                } while (c.moveToNext());
            }
            c.close();
        }
        return campaigns;
    }

    public static Campaign findCampaignById(int campaignId) {
        String selectQuery = "SELECT campaign_Color, campaign_Details, campaignID, company, default_count, key, logo, max_count, min_count, name, campaignLabel, campaignOwner, campaignPreferences FROM Campaign WHERE campaignID = " + campaignId;
        Log.d("SELECT",selectQuery);
        Cursor c = selectDataForQuery(selectQuery);
        if (c != null) {
            if (c.getCount() > 0) {
                Campaign campaign = eventFromStatement(c);
                c.close();
                return campaign;
            }
            c.close();
        }
        return null;
    }

    private static final String table = "Campaign";

    public boolean saveToDb() {

        if (this.campaignID > 0) {
            Campaign campaign = this.findCampaignById(this.campaignID);
            if (campaign == null) {
                ContentValues values = new ContentValues();
                values.put("campaign_Color",this.campaign_Color);
                values.put("campaign_Details",this.campaign_Details);
                values.put("campaignID",this.campaignID);
                values.put("company",this.company);
                values.put("default_count",this.default_count);
                values.put("key",this.key);
                values.put("logo",this.logo);
                values.put("max_count",this.max_count);
                values.put("min_count",this.min_count);
                values.put("name",this.name);
                values.put("campaignLabel",this.campaignLabel);
                values.put("campaignOwner",this.campaignOwner);
                values.put("campaignPreferences",this.campaignPreferences);
//                if(images != null)
//                    values.put("images", new Gson().toJson(this.images));

                return (insertDataForQuery(table,values) != -1);

            } else {
                if (!this.equals(campaign)) {
                    String whereClause = "campaignID = " + this.campaignID;
                    ContentValues values = new ContentValues();
                    values.put("campaign_Color",this.campaign_Color);
                    values.put("campaign_Details",this.campaign_Details);
                    values.put("campaignID",this.campaignID);
                    values.put("company",this.company);
                    values.put("default_count",this.default_count);
                    values.put("key",this.key);
                    values.put("logo",this.logo);
                    values.put("max_count",this.max_count);
                    values.put("min_count",this.min_count);
                    values.put("name",this.name);
                    values.put("campaignLabel",this.campaignLabel);
                    values.put("campaignOwner",this.campaignOwner);
                    values.put("campaignPreferences",this.campaignPreferences);
//                    if(images != null)
//                        values.put("images", new Gson().toJson(this.images));
                    return (updateDataForQuery(table, values, whereClause) != -1);
                } else {
                    return true;
                }
            }
        } else {
            ContentValues values = new ContentValues();
            values.put("campaign_Color",this.campaign_Color);
            values.put("campaign_Details",this.campaign_Details);
            values.put("campaignID",this.campaignID);
            values.put("company",this.company);
            values.put("default_count",this.default_count);
            values.put("key",this.key);
            values.put("logo",this.logo);
            values.put("max_count",this.max_count);
            values.put("min_count",this.min_count);
            values.put("name",this.name);
            values.put("campaignLabel",this.campaignLabel);
            values.put("campaignOwner",this.campaignOwner);
            values.put("campaignPreferences",this.campaignPreferences);
//            if(images != null)
//                values.put("images", new Gson().toJson(this.images));
            return (insertDataForQuery(table,values) != -1);
        }




    }

    public static void destroy() {
        sqLiteOpenHelper.getWritableDatabase().execSQL(String.format("DELETE FROM Campaign"));
    }


}
