package com.vboost.pro.DBClasses;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import com.vboost.pro.VboostDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nabeel Hafeez on 9/2/2016.
 */
public class DataLabels extends VboostDatabase {

    private String image;
    private int imageSequence;
    private String name;
    private String title;
    private String campaignOwner;
    private String labelPreference;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getImageSequence() {
        return imageSequence;
    }

    public void setImageSequence(int imageSequence) {
        this.imageSequence = imageSequence;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCampaignOwner() {
        return campaignOwner;
    }

    public void setCampaignOwner(String campaignOwner) {
        this.campaignOwner = campaignOwner;
    }

    public String getLabelPreference() {
        return labelPreference;
    }

    public void setLabelPreference(String labelPreference) {
        this.labelPreference = labelPreference;
    }

    public DataLabels(String _image,
                      int _imageSequence,
                      String _name,
                      String _title,
                      String _campaignOwner,
                      String _labelPreference) {

        this.setImage(_image);
        this.setImageSequence(_imageSequence);
        this.setName(_name);
        this.setTitle(_title);
        this.setCampaignOwner(_campaignOwner);
        this.setLabelPreference(_labelPreference);

    }

    private static DataLabels eventFromStatement(Cursor cursor) {
        if (cursor != null) {
            try {
                if (cursor.getCount() == 0) {
                    return null;
                }

                String image = cursor.getString(0);
                int imageSequence = cursor.getInt(1);
                String name = cursor.getString(2);
                String title = cursor.getString(3);
                String campaignOwner = cursor.getString(4);
                String labelPreference = cursor.getString(5);
                DataLabels dataLabels = new DataLabels(image, imageSequence, name, title, campaignOwner, labelPreference);
                return dataLabels;
            } catch (Exception e) {
                Log.e("TAG", "Error in cursor memory allocation: " + e.toString());
                return null;
            }
        } else {
            return null;
        }
    }

    public static List<DataLabels> getAll() {
        String selectQuery = "SELECT image, imageSequence, name, title, campaignOwner, labelPreference FROM Labels";
        Log.d("SELECT", selectQuery);
        Cursor c = selectDataForQuery(selectQuery);

        ArrayList<DataLabels> labelses = new ArrayList<DataLabels>();
        DataLabels dataLabels;
        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    dataLabels = eventFromStatement(c);
                    labelses.add(dataLabels);
                } while (c.moveToNext());
            }
            c.close();
        }
        return labelses;
    }

    private static List<DataLabels> findAllByCursor(Cursor c) {
        ArrayList<DataLabels> labelses = new ArrayList<DataLabels>();
        DataLabels dataLabels;
        if (c != null) {

            if (c.moveToFirst()) {
                do {
                    dataLabels = eventFromStatement(c);
                    labelses.add(dataLabels);
                } while (c.moveToNext());
            }
            c.close();
        }
        return labelses;
    }

    public static DataLabels findByname(String labelName) {

        String selectQuery = "SELECT image, imageSequence, name, title, campaignOwner, labelPreference FROM Labels WHERE name =" + labelName;
        Log.d("SELECT", selectQuery);
        Cursor c = selectDataForQuery(selectQuery);
        if (c != null) {
            if (c.getCount() > 0) {
                DataLabels dataLabels = eventFromStatement(c);
                c.close();
                return dataLabels;
            }
            c.close();
        }
        return null;
    }

    public static DataLabels findByPosition(String position) {

        String selectQuery = "SELECT image, imageSequence, name, title, campaignOwner, labelPreference FROM Labels WHERE imageSequence =" + position;
        Log.d("SELECT", selectQuery);
        Cursor c = selectDataForQuery(selectQuery);
        if (c != null) {
            if (c.getCount() > 0) {
                DataLabels dataLabels = eventFromStatement(c);
                c.close();
                return dataLabels;
            }
            c.close();
        }
        return null;
    }

    private static final String table = "Labels";

    public boolean saveToDb() {

        DataLabels dataLabels = this.findByname("'"+this.name+"'");
        if (dataLabels == null) {
            ContentValues values = new ContentValues();
            values.put("image", this.image);
            values.put("imageSequence", this.imageSequence);
            values.put("name", this.name);
            values.put("title", this.title);
            values.put("campaignOwner", this.campaignOwner);
            values.put("labelPreference", this.labelPreference);

            return (insertDataForQuery(table, values) != -1);

        } else {
            if (!this.equals(dataLabels)) {
                String whereClause = "name = " + "'"+this.name+"'";
                ContentValues values = new ContentValues();
                values.put("image", this.image);
                values.put("imageSequence", this.imageSequence);
                values.put("name", this.name);
                values.put("title", this.title);
                values.put("campaignOwner", this.campaignOwner);
                values.put("labelPreference", this.labelPreference);
                return (updateDataForQuery(table, values, whereClause) != -1);
            } else {
                return true;
            }
        }



    }

    public static void destroy() {
        sqLiteOpenHelper.getWritableDatabase().execSQL(String.format("DELETE FROM Labels"));
    }


}
