package com.vboost.pro.DBClasses;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import com.vboost.pro.VboostDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nabeel Hafeez on 9/2/2016.
 */
public class Contact extends VboostDatabase {

    private String company;
    private int contact_id;
    private String email;
    private String is_active;
    private String name;
    private String phone;
    private String photo;
    private String sync_status;
    private String title;
    private String type;
    private String contactOwner;

    public int getContact_id() {
        return contact_id;
    }

    public void setContact_id(int contact_id) {
        this.contact_id = contact_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIs_active() {
        return is_active;
    }

    public void setIs_active(String is_active) {
        this.is_active = is_active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getSync_status() {
        return sync_status;
    }

    public void setSync_status(String sync_status) {
        this.sync_status = sync_status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContactOwner() {
        return contactOwner;
    }

    public void setContactOwner(String contactOwner) {
        this.contactOwner = contactOwner;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }


    public Contact(String _company,
                   int _contact_id,
                   String _email,
                   String _is_active,
                   String _name,
                   String _phone,
                   String _photo,
                   String _sync_status,
                   String _title,
                   String _type,
                   String _contactOwner) {

        this.setCompany(_company);
        this.setContact_id(_contact_id);
        this.setEmail(_email);
        this.setIs_active(_is_active);
        this.setName(_name);
        this.setPhone(_phone);
        this.setPhoto(_photo);
        this.setSync_status(_sync_status);
        this.setTitle(_title);
        this.setType(_type);
        this.setContactOwner(_contactOwner);

    }

    private static Contact eventFromStatement(Cursor cursor) {
        if (cursor != null) {
            try {
                if (cursor.getCount() == 0) {
                    return null;
                }

                String company = cursor.getString(0);
                int contact_id = cursor.getInt(1);
                String email = cursor.getString(2);
                String is_active = cursor.getString(3);
                String name = cursor.getString(4);
                String phone = cursor.getString(5);
                String photo = cursor.getString(6);
                String sync_status = cursor.getString(7);
                String title = cursor.getString(8);
                String type = cursor.getString(9);
                String contactOwner = cursor.getString(10);
                Contact contact = new Contact(company , contact_id , email , is_active , name , phone , photo , sync_status ,
                        title  , type , contactOwner);
                return contact;
            } catch (Exception e) {
                Log.e("TAG","Error in cursor memory allocation: " + e.toString());
                return null;
            }
        } else {
            return null;
        }
    }

    public static List<Contact> getAll() {
        String selectQuery = "SELECT company , contact_id , email , is_active , name , phone , photo , sync_status , title , type , contactOwner FROM Contact";
        Log.d("SELECT",selectQuery);
        Cursor c = selectDataForQuery(selectQuery);

        ArrayList<Contact> contacts = new ArrayList<Contact>();
        Contact contact;
        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    contact = eventFromStatement(c);
                    contacts.add(contact);
                } while (c.moveToNext());
            }
            c.close();
        }
        return contacts;
    }

    private static List<Contact> findAllByCursor(Cursor c) {
        ArrayList<Contact> contacts = new ArrayList<Contact>();
        Contact contact;
        if (c != null) {

            if (c.moveToFirst()) {
                do {
                    contact = eventFromStatement(c);
                    contacts.add(contact);
                } while (c.moveToNext());
            }
            c.close();
        }
        return contacts;
    }

    public static Contact findContactById(int contactId) {
        String selectQuery = "SELECT company , contact_id , email , is_active , name , phone , photo , sync_status , title , type , contactOwner FROM Contact WHERE contact_id = " + contactId;
        Log.d("SELECT",selectQuery);
        Cursor c = selectDataForQuery(selectQuery);
        if (c != null) {
            if (c.getCount() > 0) {
                Contact contact = eventFromStatement(c);
                c.close();
                return contact;
            }
            c.close();
        }
        return null;
    }

    private static final String table = "Contact";

    public boolean saveToDb() {

        if (this.contact_id > 0) {
            Contact contact = this.findContactById(this.contact_id);
            if (contact == null) {
                ContentValues values = new ContentValues();
                values.put("company",this.company);
                values.put("contact_id",this.contact_id);
                values.put("email",this.email);
                values.put("is_active",this.is_active);
                values.put("name",this.name);
                values.put("phone",this.phone);
                values.put("photo",this.photo);
                values.put("sync_status",this.sync_status);
                values.put("title",this.title);
                values.put("type",this.type);
                values.put("contactOwner",this.contactOwner);

                return (insertDataForQuery(table,values) != -1);

            } else {
                if (!this.equals(contact)) {
                    String whereClause = "contact_id = " + this.contact_id;
                    ContentValues values = new ContentValues();
                    values.put("company",this.company);
                    values.put("contact_id",this.contact_id);
                    values.put("email",this.email);
                    values.put("is_active",this.is_active);
                    values.put("name",this.name);
                    values.put("phone",this.phone);
                    values.put("photo",this.photo);
                    values.put("sync_status",this.sync_status);
                    values.put("title",this.title);
                    values.put("type",this.type);
                    values.put("contactOwner",this.contactOwner);
                    return (updateDataForQuery(table, values, whereClause) != -1);
                } else {
                    return true;
                }
            }
        } else {
            ContentValues values = new ContentValues();
            values.put("company",this.company);
            values.put("contact_id",this.contact_id);
            values.put("email",this.email);
            values.put("is_active",this.is_active);
            values.put("name",this.name);
            values.put("phone",this.phone);
            values.put("photo",this.photo);
            values.put("sync_status",this.sync_status);
            values.put("title",this.title);
            values.put("type",this.type);
            values.put("contactOwner",this.contactOwner);

            return (insertDataForQuery(table,values) != -1);
        }



    }

    public static void destroy() {
        try{
            sqLiteOpenHelper.getWritableDatabase().execSQL(String.format("DELETE FROM Contact"));

        }
        catch (Exception e){
            e.printStackTrace();
        }

    }



}
