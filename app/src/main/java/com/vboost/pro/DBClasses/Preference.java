package com.vboost.pro.DBClasses;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import com.vboost.pro.VboostDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nabeel Hafeez on 9/2/2016.
 */
public class Preference extends VboostDatabase {

    private int campaignID;
    private int contactID;
    private String recepientEmail;
    private String recepientName;
    private String recepientPermission;
    private String recepientPhone;
    private String signature;
    private String syncStatus;
    private String terms;
    private String preferenceCampaign;
    private String preferenceUser;
    private String preferenceLabel;

    public int getCampaignID() {
        return campaignID;
    }

    public void setCampaignID(int campaignID) {
        this.campaignID = campaignID;
    }

    public int getContactID() {
        return contactID;
    }

    public void setContactID(int contactID) {
        this.contactID = contactID;
    }

    public String getRecepientEmail() {
        return recepientEmail;
    }

    public void setRecepientEmail(String recepientEmail) {
        this.recepientEmail = recepientEmail;
    }

    public String getRecepientName() {
        return recepientName;
    }

    public void setRecepientName(String recepientName) {
        this.recepientName = recepientName;
    }

    public String getRecepientPermission() {
        return recepientPermission;
    }

    public void setRecepientPermission(String recepientPermission) {
        this.recepientPermission = recepientPermission;
    }

    public String getRecepientPhone() {
        return recepientPhone;
    }

    public void setRecepientPhone(String recepientPhone) {
        this.recepientPhone = recepientPhone;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getSyncStatus() {
        return syncStatus;
    }

    public void setSyncStatus(String syncStatus) {
        this.syncStatus = syncStatus;
    }

    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    public String getPreferenceCampaign() {
        return preferenceCampaign;
    }

    public void setPreferenceCampaign(String preferenceCampaign) {
        this.preferenceCampaign = preferenceCampaign;
    }

    public String getPreferenceUser() {
        return preferenceUser;
    }

    public void setPreferenceUser(String preferenceUser) {
        this.preferenceUser = preferenceUser;
    }

    public String getPreferenceLabel() {
        return preferenceLabel;
    }

    public void setPreferenceLabel(String preferenceLabel) {
        this.preferenceLabel = preferenceLabel;
    }

    public Preference(int _campaignID,
                      int _contactID,
                      String _recepientEmail,
                      String _recepientName,
                      String _recepientPermission,
                      String _recepientPhone,
                      String _signature,
                      String _syncStatus,
                      String _terms,
                      String _preferenceCampaign,
                      String _preferenceUser,
                      String _preferenceLabel) {

        this.setCampaignID(_campaignID);
        this.setContactID(_contactID);
        this.setRecepientEmail(_recepientEmail);
        this.setRecepientName(_recepientName);
        this.setRecepientPermission(_recepientPermission);
        this.setRecepientPhone(_recepientPhone);
        this.setSignature(_signature);
        this.setSyncStatus(_syncStatus);
        this.setTerms(_terms);
        this.setPreferenceCampaign(_preferenceCampaign);
        this.setPreferenceUser(_preferenceUser);
        this.setPreferenceLabel(_preferenceLabel);

    }

    private static Preference eventFromStatement(Cursor cursor) {
        if (cursor != null) {
            try {
                if (cursor.getCount() == 0) {
                    return null;
                }


                int campaignID = cursor.getInt(0);
                int contactID = cursor.getInt(1);
                String recepientEmail = cursor.getString(2);
                String recepientName = cursor.getString(3);
                String recepientPermission = cursor.getString(4);
                String recepientPhone = cursor.getString(5);
                String signature = cursor.getString(6);
                String syncStatus = cursor.getString(7);
                String terms = cursor.getString(8);
                String preferenceCampaign = cursor.getString(9);
                String preferenceUser = cursor.getString(10);
                String preferenceLabel = cursor.getString(11);
                Preference preference = new Preference(campaignID,contactID,recepientEmail,recepientName,recepientPermission,recepientPhone,signature,syncStatus,terms,
                        preferenceCampaign,preferenceUser,preferenceLabel);
                return preference;
            } catch (Exception e) {
                Log.e("TAG","Error in cursor memory allocation: " + e.toString());
                return null;
            }
        } else {
            return null;
        }
    }

    public static List<Preference> getAll() {
        String selectQuery = "SELECT campaignID,contactID,recepientEmail,recepientName,recepientPermission,recepientPhone,signature,syncStatus,terms, preferenceCampaign,preferenceUser,preferenceLabel FROM Preference";
        Log.d("SELECT",selectQuery);
        Cursor c = selectDataForQuery(selectQuery);

        ArrayList<Preference> preferences = new ArrayList<Preference>();
        Preference preference;
        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    preference = eventFromStatement(c);
                    preferences.add(preference);
                } while (c.moveToNext());
            }
            c.close();
        }
        return preferences;
    }

    private static List<Preference> findAllByCursor(Cursor c) {
        ArrayList<Preference> preferences = new ArrayList<Preference>();
        Preference preference;
        if (c != null) {

            if (c.moveToFirst()) {
                do {
                    preference = eventFromStatement(c);
                    preferences.add(preference);
                } while (c.moveToNext());
            }
            c.close();
        }
        return preferences;
    }

    public static Preference findPreferenceById(int campaignId) {
        String selectQuery = "SELECT campaignID,contactID,recepientEmail,recepientName,recepientPermission,recepientPhone,signature,syncStatus,terms, preferenceCampaign,preferenceUser,preferenceLabel FROM Preference WHERE campaignID = " + campaignId;
        Log.d("SELECT",selectQuery);
        Cursor c = selectDataForQuery(selectQuery);
        if (c != null) {
            if (c.getCount() > 0) {
                Preference preference = eventFromStatement(c);
                c.close();
                return preference;
            }
            c.close();
        }
        return null;
    }

    private static final String table = "Preference";

    public boolean saveToDb() {

        ContentValues values = new ContentValues();
        values.put("campaignID",this.campaignID);
        values.put("contactID",this.contactID);
        values.put("recepientEmail",this.recepientEmail);
        values.put("recepientName",this.recepientName);
        values.put("recepientPermission",this.recepientPermission);
        values.put("recepientPhone",this.recepientPhone);
        values.put("signature",this.signature);
        values.put("syncStatus",this.syncStatus);
        values.put("terms",this.terms);
        values.put("preferenceCampaign",this.preferenceCampaign);
        values.put("preferenceUser",this.preferenceUser);
        values.put("preferenceLabel",this.preferenceLabel);

        return (insertDataForQuery(table,values) != -1);

    }

    public static void  destroy() {
        sqLiteOpenHelper.getWritableDatabase().execSQL(String.format("DELETE FROM Preference"));
    }



}
