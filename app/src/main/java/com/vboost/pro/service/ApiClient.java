package com.vboost.pro.service;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;

import com.vboost.pro.DBClasses.DataLabels;
import com.vboost.pro.DBClasses.Preference;
import com.vboost.pro.Util.Constant;
import com.vboost.pro.event.NetworkRequestFailedEvent;
import com.vboost.pro.event.PhotoUploadRequestEvent;
import com.vboost.pro.event.PhotoUploadResponseEvent;
import com.vboost.pro.model.PhotoAndName;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

/**
 * @author Samvel Abrahamyan
 */

public class ApiClient {

    private static ApiClient instance;

    public static final String MyPREFERENCES = "MyPrefs";
    public static final String Token = "tokenKey";
    SharedPreferences sharedpreferences;
    String userCredentials;
    private Bus eventBus;

    private Retrofit retrofit;
    private ImageService imageService;
    private Context context;

    private ApiClient(Context context) {

        sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        userCredentials = sharedpreferences.getString(Token, null);

        this.context = context;

        eventBus = new Bus("ApiClientBus");
        retrofit = new Retrofit.Builder()
                .baseUrl(Constant.SERVER_URL)
                .client(createHttpClient(context))
                .build();
        eventBus.register(this);
        imageService = retrofit.create(ImageService.class);

    }

    private OkHttpClient createHttpClient(final Context context) {
        //Add auth header interceptor
        return new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request.Builder ongoing = chain.request().newBuilder();
                        ongoing.addHeader("Authorization", "Token "+userCredentials);
                        ongoing.addHeader("User-Agent", "Vboost Pro, 20160624, Android "+android.os.Build.VERSION.SDK_INT);
                        return chain.proceed(ongoing.build());
                    }
                })
                .build();
    }

    public synchronized static ApiClient getInstance(Context context) {
        if (instance == null) {
            instance = new ApiClient(context);
        }
        return instance;
    }

    public Bus getBus() {
        return eventBus;
    }

    @Subscribe
    public void onRequestPhotoUpload(final PhotoUploadRequestEvent request) {
        Call<ResponseBody> apiCall = imageService.uploadPhoto(

                request.getCampaignID(),
                request.getContactID(),
                request.getRecipientName(),
                request.getRecipientEmail(),
                request.getPermission(),
                request.getRecipientPhone(),
                request.getRecipientSignature(),
                embedInJson(request.getPhotoAndName())
        );

        apiCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                try {
                    String json = response.body() == null ? response.errorBody().string() : response.body().string();
                    String indentedJson = new JSONObject(json).toString();
                    eventBus.post(new PhotoUploadResponseEvent(response.isSuccessful(), indentedJson));
                    if(response.isSuccessful()){
                        ArrayList<Uri> browsedUris = new ArrayList<>();
                        List<DataLabels> imagesAndLabels;
                        imagesAndLabels = DataLabels.getAll();

                        if (imagesAndLabels != null) {

                            if (imagesAndLabels.size() != 0) {

                                for (DataLabels dataLabels : imagesAndLabels) {

                                    Uri myUriImagePath = Uri.parse(dataLabels.getImage());
                                    browsedUris.add(myUriImagePath);
                                }

                                for (Uri uri : browsedUris) {
                                    deleteImage(uri);
                                }
                                DataLabels.destroy();
                                Preference.destroy();
                                System.out.println("Uploaded successfully");
                                File sdCard = Environment.getExternalStorageDirectory();
                                File dir = new File(sdCard.getAbsolutePath() + "/Vboost");
                                dir.delete();
                            }
                        }
                    }
                } catch (Exception e) {
                    throw new IllegalStateException("Failed parsing body",e);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                eventBus.post(new NetworkRequestFailedEvent(t));
            }

        });
    }

    private String embedInJson(ArrayList<PhotoAndName> photoAndNameArrayList) {
        JSONArray imageArray = new JSONArray();
        /*"Image_" + UUID.randomUUID().toString() + ".png"*/
        try {
            for (PhotoAndName photo : photoAndNameArrayList){
                imageArray.put(
                        new JSONObject()
                        .put("name", photo.getName())
                        .put("image", Base64.encodeToString(photo.getPhotos(), Base64.DEFAULT))
                );
            }

            return imageArray.toString();
        } catch (JSONException e) {
            throw new IllegalStateException("Error creating image array json", e);
        }
    }

    public void deleteImage(Uri uri) {
        //String file_dj_path = Environment.getExternalStorageDirectory() + "/ECP_Screenshots/abc";
        File fdelete = new File(uri.getPath());
        if (fdelete.exists()) {
            if (fdelete.delete()) {

                if(fdelete.exists()){
                    try {
                        fdelete.getCanonicalFile().delete();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if(fdelete.exists()){
                        context.deleteFile(fdelete.getName());
                    }
                }
                Log.e("-->", "file Deleted :" + uri.getPath());
                callBroadCast(uri.getPath(), true);
            } else {
                Log.e("-->", "file not Deleted :" + uri.getPath());
            }
        }
    }

    public void callBroadCast(String path, final boolean isDelete) {
        if (Build.VERSION.SDK_INT >= 14) {
            Log.e("-->", " >= 14");
            MediaScannerConnection.scanFile(context, new String[]{path}, null, new MediaScannerConnection.OnScanCompletedListener() {
                /*
                 *   (non-Javadoc)
                 * @see android.media.MediaScannerConnection.OnScanCompletedListener#onScanCompleted(java.lang.String, android.net.Uri)
                 */
                public void onScanCompleted(String path, Uri uri) {
                    if (isDelete) {
                        if (uri != null) {
                            context.getContentResolver().delete(uri,
                                    null, null);
                        }
                    }
                    Log.e("ExternalStorage", "Scanned " + path + ":");
                    Log.e("ExternalStorage", "-> uri=" + uri);
                }
            });
        } else {
            Log.e("-->", " < 14");
            context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED,
                    Uri.parse("file://" + Environment.getExternalStorageDirectory())));
        }
    }

}
