package com.vboost.pro.Network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.vboost.pro.VboostDatabase;
import com.vboost.pro.model.PhotoAndName;
import com.vboost.pro.service.ApiClient;
import com.vboost.pro.DBClasses.DataLabels;
import com.vboost.pro.DBClasses.Preference;
import com.vboost.pro.DBClasses.User;
import com.vboost.pro.event.NetworkRequestFailedEvent;
import com.vboost.pro.event.PhotoUploadRequestEvent;
import com.vboost.pro.event.PhotoUploadResponseEvent;

import com.squareup.otto.Subscribe;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nabeel Hafeez on 9/9/2016.
 */

public class NetworkChangeReceiver extends BroadcastReceiver {

    Context context;
    List<DataLabels> imagesAndLabels = null;
    List<Preference> preferenceData = null;

    List<User> userData;
    String campaignID;
    String contactID;
    String name;
    String phone;
    String email;
    String permission;
    String signature;
    String userCredentials;

    ApiClient client;
    ArrayList<Uri> browsedUris = new ArrayList<>();
    ArrayList<PhotoAndName> photoAndNameArrayList  = new ArrayList<>();


    @Override
    public void onReceive(Context _context, final Intent intent) {
        this.context = _context;
        int status = NetworkUtil.getConnectivityStatusString(context);

        String action = intent.getAction();
        VboostDatabase dbHelper = new VboostDatabase();
        try {
            dbHelper.createDataBase(context);
        } catch (IOException e) {
            e.printStackTrace();
        }

        client = ApiClient.getInstance(context);


        Log.i("Receiver", "Broadcast received: " + action);

        if (action.equals("my.action.string") || action.equals("android.net.conn.CONNECTIVITY_CHANGE") || action.equals("android.net.wifi.WIFI_STATE_CHANGED")) {

            if (status == 1 || status == 2) {
            imagesAndLabels = DataLabels.getAll();
            preferenceData = Preference.getAll();

            if (imagesAndLabels != null && preferenceData != null) {

                if (imagesAndLabels.size() != 0 && preferenceData.size() != 0) {
                    for (int i = 0; i < preferenceData.size(); i++) {

                        campaignID = String.valueOf(preferenceData.get(i).getCampaignID());
                        contactID = String.valueOf(preferenceData.get(i).getContactID());
                        name = preferenceData.get(i).getRecepientName();
                        phone = preferenceData.get(i).getRecepientPhone();
                        email = preferenceData.get(i).getRecepientEmail();
                        permission = preferenceData.get(i).getRecepientPermission();
                        signature = preferenceData.get(i).getSignature();
                    }

//                    Toast.makeText(context, "Service Place", Toast.LENGTH_SHORT);
                    for(DataLabels dataLabels : imagesAndLabels){

//                        Uri myUriImagePath = Uri.parse(dataLabels.getImage());
//                        browsedUris.add(myUriImagePath);
//                        String imageTitle = dataLabels.getTitle();
//                        try {
//                            InputStream imageStream = null;
//                            try {
//                                imageStream = context.getContentResolver().openInputStream(myUriImagePath);
//                            } catch (FileNotFoundException e) {
//                                e.printStackTrace();
//                            }
//
//                            Bitmap compressBitmap = BitmapFactory.decodeStream(imageStream);
//
//                            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
//                            compressBitmap.compress(Bitmap.CompressFormat.JPEG, 10, outStream);
//                            PhotoAndName photoAndName = new PhotoAndName();
//                            photoAndName.setPhotos(outStream.toByteArray());
//                            photoAndName.setName(imageTitle);
//                            photoAndNameArrayList.add(photoAndName);
//                        } catch (Exception e) {
//                            throw new IllegalStateException("Could not read file", e);
//                        }

                        String imageTitle = dataLabels.getTitle();

                        Bitmap bitmap = BitmapFactory.decodeFile(dataLabels.getImage());
                        bitmap = rotatedBitmap(bitmap, dataLabels.getImage());

                        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
//            compressBitmap.compress(Bitmap.CompressFormat.JPEG, 10, outStream);
                        // todo consideration
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, outStream);
                        PhotoAndName photoAndName = new PhotoAndName();
                        photoAndName.setPhotos(outStream.toByteArray());
                        photoAndName.setName(imageTitle);
                        photoAndNameArrayList.add(photoAndName);

                    }
                    /*for (int i = 0; i < imagesAndLabels.size(); i++) {
                        DataLabels dataLabels = imagesAndLabels.get(i);
                        Uri myUriImagePath = Uri.parse(dataLabels.getImage());
                        imagesTitle.add(dataLabels.getTitle());
                        browsedUris.add(myUriImagePath);
                    }*/



                        /*List<byte[]> fileList = new ArrayList<>();
                        try {
                            for (Uri uri : browsedUris) {
                                fileList.add(IOUtils.toByteArray(context.getContentResolver().openInputStream(uri)));
                            }
                        } catch (Exception e) {
                            throw new IllegalStateException("Could not read file", e);
                        }*/

                        if (signature == null) {
                            signature = "";
                        }
                        client.getBus().post(new PhotoUploadRequestEvent(photoAndNameArrayList, Integer.parseInt(campaignID), Integer.parseInt(contactID), name, email, Integer.parseInt(permission), phone, signature));
                    }
                }
            }
        }

        //Toast.makeText(context, status, Toast.LENGTH_LONG).show();

    }

    public static Bitmap rotatedBitmap(Bitmap bmp, String filePath) {
        try {
            ExifInterface ei = new ExifInterface(filePath);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
            Log.d("orientation", orientation + "");
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    Log.d("orientation bmp", 270 + "");
                    bmp = rotateImage(bmp, 270);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    Log.d("orientation bmp", 90 + "");
                    bmp = rotateImage(bmp, 90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    Log.d("orientation bmp", 180 + "");
                    bmp = rotateImage(bmp, 180);
                    break;
                case ExifInterface.ORIENTATION_NORMAL:
                    Log.d("orientation bmp", 0 + "");
                    bmp = rotateImage(bmp, 0);
                    // etc.
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return bmp;
    }

    public static Bitmap rotateImage(Bitmap bmp, int mRotation) {
        Matrix matrix = new Matrix();
        matrix.postRotate(mRotation);
        return Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(),
                matrix, true);
    }

    @Subscribe
    public void onPhotoUploadFinished(PhotoUploadResponseEvent event) {

        if (event.isSuccess()) {
            if(!browsedUris.isEmpty()) {
                for (Uri uri : browsedUris) {
                    deleteImage(uri);
                }
            }
            DataLabels.destroy();
            Preference.destroy();
            System.out.println("Uploaded successfully");
            File sdCard = Environment.getExternalStorageDirectory();
            File dir = new File(sdCard.getAbsolutePath() + "/Vboost");
            dir.delete();
            Toast.makeText(context, "Uploaded successfully", Toast.LENGTH_SHORT).show();
        } else

            System.out.println("Failed to upload");
        Toast.makeText(context, "Failed to upload", Toast.LENGTH_SHORT).show();

        if (event.getMessage() != null)
            System.out.println(event.getMessage());//logText.setText(event.getMessage());
    }

    @Subscribe
    public void onNetworkRequestFailed(NetworkRequestFailedEvent event) {

            Intent intent = new Intent("my.action.string");
            intent.putExtra("userCredentials", userCredentials);
            context.sendBroadcast(intent);

    }

    public void deleteImage(Uri uri) {
        //String file_dj_path = Environment.getExternalStorageDirectory() + "/ECP_Screenshots/abc";
        File fdelete = new File(uri.getPath());
        if (fdelete.exists()) {
            if (fdelete.delete()) {

                if(fdelete.exists()){
                    try {
                        fdelete.getCanonicalFile().delete();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if(fdelete.exists()){
                        context.deleteFile(fdelete.getName());
                    }
                }
                Log.e("-->", "file Deleted :" + uri.getPath());
                callBroadCast(uri.getPath(), true);
            } else {
                Log.e("-->", "file not Deleted :" + uri.getPath());
            }
        }
    }

    public void callBroadCast(String path, final boolean isDelete) {
        if (Build.VERSION.SDK_INT >= 14) {
            Log.e("-->", " >= 14");
            MediaScannerConnection.scanFile(context, new String[]{path}, null, new MediaScannerConnection.OnScanCompletedListener() {
                /*
                 *   (non-Javadoc)
                 * @see android.media.MediaScannerConnection.OnScanCompletedListener#onScanCompleted(java.lang.String, android.net.Uri)
                 */
                public void onScanCompleted(String path, Uri uri) {
                    if (isDelete) {
                        if (uri != null) {
                            context.getContentResolver().delete(uri,
                                    null, null);
                        }
                    }
                    Log.e("ExternalStorage", "Scanned " + path + ":");
                    Log.e("ExternalStorage", "-> uri=" + uri);
                }
            });
        } else {
            Log.e("-->", " < 14");
            context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED,
                    Uri.parse("file://" + Environment.getExternalStorageDirectory())));
        }
    }
}
