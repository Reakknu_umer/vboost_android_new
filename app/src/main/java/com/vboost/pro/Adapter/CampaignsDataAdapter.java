package com.vboost.pro.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.vboost.pro.DBClasses.DataLabels;
import com.vboost.pro.Network.NetworkChangeReceiver;
import com.vboost.pro.R;
import com.vboost.pro.Util.Constant;
import com.vboost.pro.model.Labels;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class CampaignsDataAdapter extends BaseAdapter implements OnClickListener {

    private static final int TYPE_ONE_COLUMN = 0;
    private static final int TYPE_TWO_COLUMNS = 1;
    private static final int TYPE_MAX_COUNT = TYPE_TWO_COLUMNS + 1;

    private CallbackInterface mCallback;

    private Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<Labels> labalsArrayList;
    String[] images;
    private int minCount;
    private int maxCount;
    private int defaultCount;
    boolean db;
    List<DataLabels> dBLabels;

    private boolean mIsLayoutOnTop;

    public void updateArray(String[] data) {
        this.images = data;
        notifyDataSetChanged();
        setClickPreference(mContext, true);
    }

    public interface CallbackInterface {

        /**
         * Callback invoked when clicked
         *
         * @param position - the position
         * @param text     - the text to pass back
         */
        void onHandleSelection(int position, String text);
    }
    public static final String MyPREFERENCES = "MyPrefs";
    public boolean getClickPreference(Context context){
        SharedPreferences sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        boolean isEnable = sharedpreferences.getBoolean("isClickEnable", true);
        return isEnable;
    }

    public void setClickPreference(Context context, boolean value){
        SharedPreferences sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = context.getSharedPreferences(Constant.MyPREFERENCES, context.MODE_PRIVATE).edit();
        editor.putBoolean("isClickEnable", value);
        editor.commit();
    }

    public CampaignsDataAdapter(Context context, String[] _images, int _minCount, int _maxCount, int _defaultCount,
                                ArrayList<Labels> _labalsArrayList, boolean _db, boolean isLayoutOnTop) {
        mContext = context;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.images = _images;
        this.labalsArrayList = _labalsArrayList;
        this.minCount = _minCount;
        this.maxCount = _maxCount;
        this.defaultCount = _defaultCount;
        this.db = _db;
        mIsLayoutOnTop = isLayoutOnTop;
        setClickPreference(mContext, true);
        try {
            mCallback = (CallbackInterface) context;
        } catch (ClassCastException ex) {
            //.. should log the error or throw and exception
            Log.e("MyAdapter", "Must implement the CallbackInterface in the Activity", ex);
        }


    }

    @Override
    public int getItemViewType(int position) {
        if ((position == defaultCount / 2)
                && (defaultCount % 2 == 1)) {
            return TYPE_ONE_COLUMN;
        } else {
            return TYPE_TWO_COLUMNS;
        }
    }

    @Override
    public int getViewTypeCount() {
        return TYPE_MAX_COUNT;
    }

    @Override
    public int getCount() {
        return (defaultCount / 2) + (defaultCount % 2);
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder.OneColumnViewHolder oneColumnViewHolder;
        final ViewHolder.TwoColumnsViewHolder twoColumnsViewHolder;
        int type = getItemViewType(position);

        DataLabels dataLabels = null;
        if (type == TYPE_ONE_COLUMN) {

            convertView = mInflater.inflate(R.layout.list_item_categories_one_column, parent, false);
            oneColumnViewHolder = new ViewHolder.OneColumnViewHolder();
            oneColumnViewHolder.image1 = (ImageView) convertView.findViewById(R.id.list_item_image_1);
            oneColumnViewHolder.title1 = (TextView) convertView.findViewById(R.id.list_item_title_1);
            oneColumnViewHolder.layoutTopBottom1 = (ViewGroup) convertView.findViewById(R.id.layout_top_bottom_1);
            oneColumnViewHolder.image1.setOnClickListener(this);
            convertView.setTag(oneColumnViewHolder);

			/*if(db){
                dBLabels = DataLabels.getAll();
				if (dBLabels.size() != 0) {
					for(int i = 0; i<dBLabels.size(); i++){
						DataLabels row_positionOne = dBLabels.get(i);
						oneColumnViewHolder.title1.setText(row_positionOne.getTitle());
					}
				}
				else{
					Toast.makeText(mContext, "No data in local database.", Toast.LENGTH_LONG).show();
				}

			}else {*/
            Labels row_positionOne = labalsArrayList.get(position * 2);
            oneColumnViewHolder.title1.setText(row_positionOne.getTitle());
            //}

            if (images != null) {
                String imagePath = images[position * 2];
                if (imagePath != null && !imagePath.toString().equals("")) {

                    new setImageTask(imagePath, oneColumnViewHolder.image1).execute();


                } else {
                    oneColumnViewHolder.image1.setImageResource(R.drawable.camera_image);
                }
            } else {
                oneColumnViewHolder.image1.setImageResource(R.drawable.camera_image);
            }

            oneColumnViewHolder.image1.setTag(position * 2);
            LayoutParams lp1 = (LayoutParams) oneColumnViewHolder.layoutTopBottom1.getLayoutParams();
            if (!mIsLayoutOnTop) {
                lp1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
            } else {
                lp1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, 0);
            }
        } else if (type == TYPE_TWO_COLUMNS) {
            convertView = mInflater.inflate(R.layout.list_item_categories_two_columns, parent, false);
            twoColumnsViewHolder = new ViewHolder.TwoColumnsViewHolder();
            twoColumnsViewHolder.progress_1 = (ContentLoadingProgressBar) convertView.findViewById(R.id.progress_1);
            twoColumnsViewHolder.image1 = (ImageView) convertView.findViewById(R.id.list_item_image_1);
            twoColumnsViewHolder.title1 = (TextView) convertView.findViewById(R.id.list_item_title_1);
            twoColumnsViewHolder.layoutTopBottom1 = (ViewGroup) convertView.findViewById(R.id.layout_top_bottom_1);
            twoColumnsViewHolder.progress_2 = (ContentLoadingProgressBar) convertView.findViewById(R.id.progress_2);
            twoColumnsViewHolder.image2 = (ImageView) convertView.findViewById(R.id.list_item_image_2);
            twoColumnsViewHolder.title2 = (TextView) convertView.findViewById(R.id.list_item_title_2);
            twoColumnsViewHolder.layoutTopBottom2 = (ViewGroup) convertView.findViewById(R.id.layout_top_bottom_2);
            twoColumnsViewHolder.image1.setOnClickListener(this);
            twoColumnsViewHolder.image2.setOnClickListener(this);
            twoColumnsViewHolder.progress_1.show();
            twoColumnsViewHolder.progress_2.show();

            convertView.setTag(twoColumnsViewHolder);

            Labels row_positionOne = labalsArrayList.get(position * 2);
            twoColumnsViewHolder.title1.setText(row_positionOne.getTitle());

            if (images != null) {
                String imagePath;

                if (images.length == (position * 2) || images.length < (position * 2)) {
                    imagePath = null;
                } else {
                    imagePath = images[position * 2];
                }

                if (imagePath != null && !imagePath.toString().equals("")) {
                    new setImageTask(imagePath, twoColumnsViewHolder.image1).execute();

//                    Bitmap bitmapImage = null;
//                    bitmapImage = BitmapFactory.decodeFile(imagePath.getEncodedPath());
//                    bitmapImage = NetworkChangeReceiver.rotatedBitmap(bitmapImage, imagePath.getPath());
//                    twoColumnsViewHolder.image1.setImageBitmap(bitmapImage);
                } else {
                    twoColumnsViewHolder.image1.setImageResource(R.drawable.camera_image);
                }
            } else {
                twoColumnsViewHolder.image1.setImageResource(R.drawable.camera_image);
            }

            Labels row_positionTwo = labalsArrayList.get(position * 2 + 1);
            twoColumnsViewHolder.title2.setText(row_positionTwo.getTitle());

            if (images != null) {
                String imagePath;
                if (images.length == (position * 2 + 1) || images.length < (position * 2 + 1)) {
                    imagePath = null;
                } else {
                    imagePath = images[position * 2 + 1];
                }
                if (imagePath != null && !imagePath.toString().equals("")) {
                    new setImageTask(imagePath, twoColumnsViewHolder.image2).execute();

//                    Bitmap bitmapImage = null;
//                    bitmapImage = BitmapFactory.decodeFile(imagePath.getEncodedPath());
//                    bitmapImage = NetworkChangeReceiver.rotatedBitmap(bitmapImage, imagePath.getPath());
//					twoColumnsViewHolder.image2.setImageBitmap(bitmapImage);
                } else {
                    twoColumnsViewHolder.image2.setImageResource(R.drawable.camera_image);
                }
            } else {
                twoColumnsViewHolder.image2.setImageResource(R.drawable.camera_image);
            }


            twoColumnsViewHolder.image1.setOnClickListener(this);
            twoColumnsViewHolder.image2.setOnClickListener(this);
            twoColumnsViewHolder.image1.setTag(position * 2);
            twoColumnsViewHolder.image2.setTag(position * 2 + 1);
            LayoutParams lp1 = (LayoutParams) twoColumnsViewHolder.layoutTopBottom1.getLayoutParams();
            LayoutParams lp2 = (LayoutParams) twoColumnsViewHolder.layoutTopBottom2.getLayoutParams();
            if (!mIsLayoutOnTop) {
                lp1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
                lp2.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
            } else {
                lp1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, 0);
                lp2.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, 0);
            }


        }
        return convertView;
    }

    private String getImagePathFromUriPath(Uri imageUri) {

        String filename = imageUri.getPath().substring(imageUri.getPath().lastIndexOf("/") + 1);
        File file = new File(Environment.getExternalStorageDirectory() + "/" + Constant.FOLDER_NAME, filename);
        String imagePath = file.getAbsolutePath();
        return imagePath;

    }

    private class setImageTask extends AsyncTask<String, Void, Bitmap> {

        String imagePath;
        ImageView imageView;

        public setImageTask(String imagePath, ImageView imageView) {
            this.imagePath = imagePath;
            this.imageView = imageView;
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Bitmap doInBackground(String... urls) {

            try {
                Bitmap bitmapImage = decodeFile(mContext, imagePath, 100);
                bitmapImage = NetworkChangeReceiver.rotatedBitmap(bitmapImage, imagePath);

                return bitmapImage;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap resultBitmap) {
            if (resultBitmap != null)
                imageView.setImageBitmap(resultBitmap);
            else
                imageView.setImageResource(R.drawable.camera_image);
        }
    }

    private static class ViewHolder {

        public static class OneColumnViewHolder {
            public ImageView image1;
            public TextView title1;
            public ViewGroup layoutTopBottom1;
        }

        private static class TwoColumnsViewHolder {
            public ContentLoadingProgressBar progress_1;
            public ImageView image1;
            public TextView title1;
            public ViewGroup layoutTopBottom1;

            public ContentLoadingProgressBar progress_2;
            public ImageView image2;
            public TextView title2;
            public ViewGroup layoutTopBottom2;
        }
    }

    public void setClicked(boolean clicked) {

        setClickPreference(mContext, clicked);
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        int position = (int) v.getTag();

        if (id == R.id.list_item_image_1 || id == R.id.list_item_image_2) {

            if (mCallback != null) {
                /*if (db) {
                    mCallback.onHandleSelection(position, dBLabels.get(position).getTitle());

				} else {*/
                if(getClickPreference(mContext)) {
//                  v.setOnClickListener(null);
                    setClickPreference(mContext, false);
                    mCallback.onHandleSelection(position, labalsArrayList.get(position).getTitle());

                }

                //}
            }

			/*Log.e("Click Position", ""+position);
			Intent intent = new Intent(mContext, CameraActivity.class);
			String cameraTitle = labalsArrayList.get(position).getTitle();
			intent.putExtra("phototitle", cameraTitle);
			mContext.startActivity(intent);*/

        }
    }

    public String imageStringFormat(String path) {

        Bitmap bitmapOrg = BitmapFactory.decodeFile(path);
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        bitmapOrg.compress(Bitmap.CompressFormat.JPEG, 100, bao);
        byte[] ba = bao.toByteArray();
        String image = Base64.encodeToString(ba, Base64.DEFAULT);

        return image;
    }

    public Bitmap stringToImage(String encodedImage) {


        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 4;
        byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length, options);

        return decodedByte;
    }

    public static Bitmap decodeUri(Context c, Uri uri, final int requiredSize)
            throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(c.getContentResolver().openInputStream(uri), null, o);

        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;

        while (true) {
            if (width_tmp / 2 < requiredSize || height_tmp / 2 < requiredSize)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(c.getContentResolver().openInputStream(uri), null, o2);
    }

    public static Bitmap decodeFile(Context c, String filePath, final int requiredSize)
            throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, o);

        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;

        while (true) {
            if (width_tmp / 2 < requiredSize || height_tmp / 2 < requiredSize)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeFile(filePath, o2);
    }
}
