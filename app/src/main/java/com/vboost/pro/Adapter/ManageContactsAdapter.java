package com.vboost.pro.Adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.bigkoo.alertview.AlertView;
import com.bigkoo.alertview.OnItemClickListener;

import com.vboost.pro.*;
import com.vboost.pro.Util.Constant;
import com.vboost.pro.model.ManageContacts;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Nabeel Hafeez on 7/27/2016.
 */
public class ManageContactsAdapter extends BaseAdapter {
    ArrayList<ManageContacts> manageContacts;
    Context context;
    private ProgressDialog progress;
    Activity activity;
    String userCredentials;
    String [] contactsName;
    boolean network;


    private static LayoutInflater inflater = null;

    public ManageContactsAdapter(Activity activity, ArrayList<ManageContacts> _manageContacts, String _userCredentials, String [] _contactsName, boolean _network) {

        // TODO Auto-generated constructor stub
        this.manageContacts = _manageContacts;
        this.context = activity;
        this.activity = activity;
        this.userCredentials = _userCredentials;
        this.contactsName = _contactsName;
        this.network = _network;

        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return manageContacts.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder {
        TextView titleContac;
        Button editContact;
        Button deleteContact;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Holder holder = new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.activity_manage_contact_list_item, null);
        holder.titleContac = (TextView) rowView.findViewById(R.id.contatc_title_text_view);
        holder.titleContac.setAllCaps(false);
        holder.editContact = (Button) rowView.findViewById(R.id.contact_edit_button);
        holder.editContact.setAllCaps(false);
        holder.deleteContact = (Button) rowView.findViewById(R.id.contact_delete_button);
        holder.deleteContact.setAllCaps(false);
        notifyDataSetChanged();
        holder.titleContac.setText(manageContacts.get(position).getName());

        holder.editContact.setTag(position);
        holder.deleteContact.setTag(position);

     /*   if(network) {
            ManageContacts mc = manageContacts.get(position);
            Contact contact = new Contact(String.valueOf(mc.getCompany()),mc.getId(),mc.getEmail(),mc.getIs_active(),mc.getName(),mc.getPhone(),mc.getPhoto(),null,mc.getTitle(),mc.getType(),null);
            contact.saveToDb();
        }*/

        holder.editContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int id =  manageContacts.get((Integer) view.getTag()).getId();
                String name = manageContacts.get((Integer) view.getTag()).getName();
                String email  =manageContacts.get((Integer) view.getTag()).getEmail();
                String phone = manageContacts.get((Integer) view.getTag()).getPhone();
                Intent  editIntent = new Intent(context, EditContact.class);
                editIntent.putExtra("contactId", id);
                editIntent.putExtra("contactname", name);
                editIntent.putExtra("contactemail", email);
                editIntent.putExtra("contactphone", phone);
                editIntent.putExtra("contactsName", contactsName);
                editIntent.putExtra("contacts", manageContacts);
                context.startActivity(editIntent);

            }
        });

        holder.deleteContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick( View view) {

                final String name = manageContacts.get((Integer) view.getTag()).getName();
                final int id =  manageContacts.get((Integer) view.getTag()).getId();

                Log.e("Delete Contact Name", name);

                 new AlertView(null, "\nMarK "+name+" inactive", "No", null, new String[]{"Yes"}, activity, AlertView.Style.Alert, new OnItemClickListener() {
                    @Override
                    public void onItemClick(Object o, int position) {
                        Log.e("Delete Contact id", ""+id);
                        if(position != AlertView.CANCELPOSITION) {
                            new DeleteContact(activity, id, name).execute();
                        }
                    }
                }).show();


            }
        });


        return rowView;
    }

    private class DeleteContact extends AsyncTask<String, Void, Void> {

        private final Context context;
        private int id;
        private String name;

        public DeleteContact(Context c, int id, String name) {
            this.context = c;
            this.id = id;
            this.name = name;
        }

        protected void onPreExecute() {
            progress = new ProgressDialog(this.context);
            progress.setMessage("Loading");
            progress.show();
        }

        @Override
        protected Void doInBackground(String... params) {

            try {

                URL url = new URL(Constant.SERVER_URL+"contacts/" + id +"/?format=json");
                HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();

               // userCredentials = "96ea502e088bc46dd3e863f1c82db3e160a4a56a";
                String tokenAuth = "Token " + userCredentials;
                httpCon.setRequestProperty("Authorization", tokenAuth);
                String urlParameters = "name="+name+"&is_active="+"false";


                httpCon.setDoOutput(false);
                httpCon.setRequestMethod("PUT");
                OutputStreamWriter out = new OutputStreamWriter(
                        httpCon.getOutputStream());
                out.write( urlParameters);
                out.close();
                httpCon.getInputStream();

                final int responseCode = httpCon.getResponseCode();


                BufferedReader in = new BufferedReader(
                        new InputStreamReader(httpCon.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                //print result
                System.out.println(response.toString());

                activity.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {

                        if(responseCode == 200){

                            Intent splashIntent = new Intent(context, ManageContatcsAvtivity.class);
                            context.startActivity(splashIntent);
                            ((Activity)context).finish();

                        }else{


                        }

                        progress.dismiss();


                    }
                });


            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                progress.dismiss();
                Log.e("Mal", ""+ e);

                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                progress.dismiss();
                Log.e("IO", ""+ e);
                e.printStackTrace();
            }
            return null;
        }


    }

}
