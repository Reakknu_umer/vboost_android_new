package com.vboost.pro.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.vboost.pro.CampaignsDataActivity;
import com.vboost.pro.DBClasses.Campaign;
import com.vboost.pro.model.ManageContacts;
import com.vboost.pro.R;
import com.vboost.pro.model.Campaigns;

import java.util.ArrayList;

import static com.vboost.pro.CampaignsActivity.liUserPicker;

/**
 * Created by Nabeel Hafeez on 7/27/2016.
 */

public class CampaignsAdapter extends BaseAdapter{

    ArrayList<ManageContacts> contacts;
    ArrayList<Campaigns> campaigns;
    String[] contactsName;
    Context context;
    boolean network;

    // ---------------------------------------------------------------------------------------------
    boolean judgeProspect;
    String[] prospectNames;
    ArrayList<Campaigns> prospectCampaings;


    private static LayoutInflater inflater = null;

    public CampaignsAdapter(Activity activity,
                            ArrayList<Campaigns> _campaigns,
                            ArrayList<ManageContacts> _contacts,
                            String[] _contactsName,
                            boolean _network,
                            boolean judgeProspect,
                            String[] prospectNames,
                            ArrayList<Campaigns> prospectCampaings) {
        // TODO Auto-generated constructor stub
        this.contacts = _contacts;
        this.campaigns = _campaigns;
        this.contactsName = _contactsName;
        this.network = _network;
        this.context = activity;

        this.judgeProspect = judgeProspect;
        this.prospectNames = prospectNames;
        this.prospectCampaings = prospectCampaings;

        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return campaigns.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder {

        Button selectCampaigns;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        // TODO Auto-generated method stub
        Holder holder = new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.list_item_campaigns,null);

        final Campaigns singleRow = campaigns.get(position);

        holder.selectCampaigns = (Button) rowView.findViewById(R.id.campaigns_list_item_button);
        holder.selectCampaigns.setAllCaps(false);

        if(judgeProspect) {
            if(position == 0) {
                holder.selectCampaigns.setText(singleRow.getName());
                holder.selectCampaigns.getBackground().setColorFilter(new LightingColorFilter(Color.parseColor("#" + singleRow.getColor()),0x00000000));
                if (network) {
                    Campaign campaign = new Campaign(singleRow.getColor(),singleRow.getDetails(),
                            singleRow.getId(),singleRow.getCompany(),singleRow.getImage().getDefault_count(),
                            singleRow.getKey(),singleRow.getImage(), singleRow.getLogo(),singleRow.getImage().getMax_count(),
                            singleRow.getImage().getMin_count(),singleRow.getName(),null,null,null);
                    campaign.saveToDb();
                }
            } else {
                holder.selectCampaigns.setText("Prospect");
                holder.selectCampaigns.getBackground().setColorFilter(new LightingColorFilter(Color.parseColor("#" + singleRow.getColor()),0x00000000));

                int len = prospectCampaings.size();

                if (network) {
                    for(int i = 1; i < len; i++) {
                        Campaigns singleRow1 = prospectCampaings.get(i);
                        Campaign campaign = new Campaign(singleRow1.getColor(), singleRow1.getDetails(),
                                singleRow1.getId(), singleRow1.getCompany(), singleRow1.getImage().getDefault_count(),
                                singleRow1.getKey(),singleRow.getImage(), singleRow1.getLogo(), singleRow1.getImage().getMax_count(),
                                singleRow1.getImage().getMin_count(), singleRow1.getName(),null,null,null);
                        campaign.saveToDb();
                    }
                }
            }
        } else {
            holder.selectCampaigns.setText(singleRow.getName());
            holder.selectCampaigns.getBackground().setColorFilter(new LightingColorFilter(Color.parseColor("#" + singleRow.getColor()),0x00000000));
            if (network) {
                Campaign campaign = new Campaign(singleRow.getColor(),singleRow.getDetails(),singleRow.getId(),singleRow.getCompany(),singleRow.getImage().getDefault_count(),singleRow.getKey(),singleRow.getImage(),singleRow.getLogo(),singleRow.getImage().getMax_count(),singleRow.getImage().getMin_count(),singleRow.getName(),null,null,null);
                campaign.saveToDb();
            }
        }



        holder.selectCampaigns.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(judgeProspect) {
                    if(position == 0) {
                        Intent campaignsDataIntent = new Intent(context,CampaignsDataActivity.class);
                        campaignsDataIntent.putExtra("contactsName",contactsName);
                        campaignsDataIntent.putExtra("campaignrow",singleRow);
                        campaignsDataIntent.putExtra("contacts",contacts);
                        campaignsDataIntent.putExtra("campaignArray", campaigns);

                        context.startActivity(campaignsDataIntent);
                    } else {
                        liUserPicker.setVisibility(View.VISIBLE);
                    }
                } else {
                    Intent campaignsDataIntent = new Intent(context,CampaignsDataActivity.class);
                    campaignsDataIntent.putExtra("contactsName",contactsName);
                    campaignsDataIntent.putExtra("campaignrow",singleRow);
                    campaignsDataIntent.putExtra("contacts",contacts);
                    campaignsDataIntent.putExtra("campaignArray", campaigns);
//                campaignsDataIntent.putExtra("position", position);
                    context.startActivity(campaignsDataIntent);
                }


            }
        });
        return rowView;
    }

}
